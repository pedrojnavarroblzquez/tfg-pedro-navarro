﻿using Windows.UI.Xaml.Controls;

namespace TFGPedroUWPApp
{
    public class dataFunction
    {
        public string name;
        public string type;
        public string det;
        public string ret;
        public applicationFur appFur;
        public Grid dFGrid;
        public Button dfBt;
        public Button BtEI;
        public Button BtEO;
        public Button BtEQ;

        public dataFunction(string n, string t, string d, string r, applicationFur appF, Grid g, Button dfBtt, Button EI, Button EO, Button EQ)
        {
            name = n;
            type = t;
            det = d;
            ret = r;
            appFur = appF;
            dFGrid = g;
            dfBt = dfBtt;
            BtEI = EI;
            BtEO = EO;
            BtEQ = EQ;
        }

        public string DFType
        {
            get { return type; }
            set { type = value; }
        }
        public string DFName
        {
            get { return name; }
            set { name = value; }
        }
        public string DFDet
        {
            get { return det; }
            set { det = value; }
        }
        public string DFRet
        {
            get { return ret; }
            set { ret = value; }
        }
        public applicationFur DFApplicationFUR
        {
            get { return appFur; }
            set { appFur = value; }
        }

        public Grid DFGrid
        {
            get { return dFGrid; }
            set { dFGrid = value; }
        }

        public Button DFButton
        {
            get { return dfBt; }
            set { dfBt = value; }
        }

        public Button EIButton
        {
            get { return BtEI; }
            set { BtEI = value; }
        }

        public Button EOButton
        {
            get { return BtEO; }
            set { BtEO = value; }
        }
        public Button EQButton
        {
            get { return BtEQ; }
            set { BtEQ = value; }
        }
    }
}
