﻿using Windows.UI.Xaml.Controls;

namespace TFGPedroUWPApp
{
    public class DFItems
    {
        public GridView dFGridView;
        public Grid dFGrid;

        public DFItems(GridView gv, Grid g)
        {
            dFGridView = gv;
            dFGrid = g;
        }

        public GridView DFGridView
        {
            get { return dFGridView; }
            set { dFGridView = value; }
        }

        public Grid DFGrid
        {
            get { return dFGrid; }
            set { dFGrid = value; }
        }

    }
}
