﻿using Windows.UI.Xaml.Controls;

namespace TFGPedroUWPApp
{
    public class dataTransaction
    {
        public string name;
        public string type;
        public applicationFur appFur;
        public dataFunction dFunction;
        public ListBox dataFunctionSelected = new ListBox();
        public ListBox detSelected = new ListBox();

        public dataTransaction(string n, string t, applicationFur appF, dataFunction dF, ListBox selected, ListBox detS)
        {
            name = n;
            type = t;
            appFur = appF;
            dFunction = dF;
            dataFunctionSelected = selected;
            detSelected = detS;
        }
        public string DTName
        {
            get { return name; }
            set { name = value; }
        }
        public string DTType
        {
            get { return type; }
            set { type = value; }
        }
        public applicationFur DFApplicationFUR
        {
            get { return appFur; }
            set { appFur = value; }
        }
        public dataFunction DataFunction
        {
            get { return dFunction; }
            set { dFunction = value; }
        }
        public ListBox SelectedDF
        {
            get { return dataFunctionSelected; }
            set { dataFunctionSelected = value; }
        }
        public ListBox SelectedDETs
        {
            get { return detSelected; }
            set { detSelected = value; }
        }
    }
}
