﻿using System;
using Windows.UI.Xaml.Controls;

namespace TFGPedroUWPApp
{
    public class applicationFur
    {
        string appName;
        Boolean appMain;
        Fur furRef;
        GridView gridApplication;

        public applicationFur(string application, Boolean main, Fur fur, GridView gApplication)
        {
            appName = application;
            appMain = main;
            furRef = fur;
            gridApplication = gApplication;

        }
        public string AppName
        {
            get { return appName; }
            set { appName = value; }
        }

        public Boolean AppBoolean
        {
            get { return appMain; }
            set { appMain = value; }
        }

        public Fur FurRef
        {
            get { return furRef; }
            set { furRef = value; }
        }

        public GridView GridApplication
        {
            get { return gridApplication; }
            set { gridApplication = value; }
        }

    }
}
