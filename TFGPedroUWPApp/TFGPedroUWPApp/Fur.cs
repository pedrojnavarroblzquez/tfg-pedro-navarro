﻿namespace TFGPedroUWPApp
{
    public class Fur
    {
        string furName, furDescription, furLongDescription;
        public Fur(string name, string description, string longDescription)
        {
            furName = name;
            furDescription = description;
            furLongDescription = longDescription;


        }
        public string FurName
        {
            get { return furName; }
            set { furName = value; }
        }

        public string FurDescription
        {
            get { return furDescription; }
            set { furDescription = value; }
        }
        public string FurLongDescription
        {
            get { return furLongDescription; }
            set { furLongDescription = value; }
        }

    }
}
