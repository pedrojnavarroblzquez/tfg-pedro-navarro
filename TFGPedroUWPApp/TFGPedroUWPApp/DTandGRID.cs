﻿using Windows.UI.Xaml.Controls;

namespace TFGPedroUWPApp
{

    public class DTandGRID
    {
        public dataTransaction dT;
        public Grid grid;

        public DTandGRID(dataTransaction d, Grid g)
        {
            dT = d;
            grid = g;
        }

        public dataTransaction DT
        {
            get { return dT; }
            set { dT = value; }
        }

        public Grid GRID
        {
            get { return grid; }
            set { grid = value; }
        }
    }

}
