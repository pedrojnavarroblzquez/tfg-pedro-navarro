﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Provider;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;


// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0xc0a

namespace TFGPedroUWPApp
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //VARIABLES
        public Library library = new Library();
        public IList<Fur> furList = new List<Fur>();
        public IList<applicationFur> applicationList = new List<applicationFur>();
        public IList<dataFunction> dataFunctionList = new List<dataFunction>();
        public IList<dataTransaction> dataTransactionList = new List<dataTransaction>();

        public IList<GridView> appGridViewListItems = new List<GridView>();

        public IList<DTandGRID> DTGridList = new List<DTandGRID>();


        public static string dataFunction;
        public static string typeDataFunction;
        public static string shortDescriptionOfFUR;

        public int num = 0;
        public int applicationCounter = 0;

        public int applicationGridView = 0;

        public MainPage()
        {
            this.InitializeComponent();
            EditButton.IsEnabled = false;
            DeleteButton.IsEnabled = false;
            okButton.IsEnabled = true;
        }

        private void dataFunctionSave_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private void EITransaction_Click(object sender, RoutedEventArgs e)
        {
            sizeFPTransaction.Text = "";
            transactionType.SelectedIndex = 0;
            loadTransaction.Visibility = Visibility.Collapsed;
            saveTransaction.Visibility = Visibility.Collapsed;
            okTransaction.Visibility = Visibility.Visible;
            for (int i = 0; i < appGridViewListItems.Count; i++)
            {
                if (appGridViewListItems[i].SelectedItem != null)
                {
                    for (int j = 0; j < dataFunctionList.Count; j++)
                    {
                        if (appGridViewListItems[i].SelectedItem.Equals(dataFunctionList[j].DFGrid))
                        {
                            applicationName.Text = dataFunctionList[j].appFur.AppName;
                        }
                    }
                }
            }
        }

        private void EQTransaction_Click(object sender, RoutedEventArgs e)
        {
            sizeFPTransaction.Text = "";
            transactionType.SelectedIndex = 1;
            loadTransaction.Visibility = Visibility.Collapsed;
            saveTransaction.Visibility = Visibility.Collapsed;
            okTransaction.Visibility = Visibility.Visible;
            for (int i = 0; i < appGridViewListItems.Count; i++)
            {
                if (appGridViewListItems[i].SelectedItem != null)
                {
                    for (int j = 0; j < dataFunctionList.Count; j++)
                    {
                        if (appGridViewListItems[i].SelectedItem.Equals(dataFunctionList[j].DFGrid))
                        {
                            applicationName.Text = dataFunctionList[j].appFur.AppName;
                        }
                    }
                }
            }
        }


        private void EOTransaction_Click(object sender, RoutedEventArgs e)
        {
            sizeFPTransaction.Text = "";
            transactionType.SelectedIndex = 2;
            loadTransaction.Visibility = Visibility.Collapsed;
            saveTransaction.Visibility = Visibility.Collapsed;
            okTransaction.Visibility = Visibility.Visible;
            for (int i = 0; i < appGridViewListItems.Count; i++)
            {
                if (appGridViewListItems[i].SelectedItem != null)
                {
                    for (int j = 0; j < dataFunctionList.Count; j++)
                    {
                        if (appGridViewListItems[i].SelectedItem.Equals(dataFunctionList[j].DFGrid))
                        {
                            applicationName.Text = dataFunctionList[j].appFur.AppName;
                        }
                    }
                }
            }
        }

        private void okTransaction_Click(object sender, RoutedEventArgs e)
        {
            ListBox auxList = new ListBox();
            ListBox auxDetList = new ListBox();

            //Check the fur that we are on.   
            for (int x = 0; x < furList.Count; x++)
            {
                if (furList[x].FurDescription.Equals(comboFUR.Description))
                {
                    applicationCounter = x;
                }
            }

            for (int i = 0; i < appGridViewListItems.Count; i++)
            {
                if (appGridViewListItems[i].SelectedItem != null)
                {
                    for (int j = 0; j < dataFunctionList.Count; j++)
                    {
                        if (furList[applicationCounter] != null)
                        {
                            if ((appGridViewListItems[i].SelectedItem.Equals(dataFunctionList[j].DFGrid)) && (dataFunctionList[i].DFApplicationFUR.FurRef.Equals(furList[applicationCounter])))
                            {
                                string nT = nameTransaction.Text;
                                applicationFur auxAppFur = dataFunctionList[j].DFApplicationFUR;
                                string aT = applicationName.Text;

                                string type;
                                if (transactionType.SelectedIndex == 0)
                                {
                                    type = "EI";
                                    dataFunctionList[j].BtEI.Visibility = Visibility.Visible;
                                }
                                else if (transactionType.SelectedIndex == 1)
                                {
                                    type = "EQ";
                                    dataFunctionList[j].BtEQ.Visibility = Visibility.Visible;
                                }
                                else
                                {
                                    type = "EO";
                                    dataFunctionList[j].BtEO.Visibility = Visibility.Visible;
                                }

                                foreach (var item in addDFtoFTR.SelectedItems)
                                {
                                    auxList.Items.Add(item.ToString());
                                }

                                foreach (var it in detList.SelectedItems)
                                {
                                    auxDetList.Items.Add(it.ToString());
                                }

                                dataTransaction auxTr = new dataTransaction(nT, type, auxAppFur, dataFunctionList[j], auxList, auxDetList);
                                dataTransactionList.Add(auxTr);
                                flyTransactionFunction.Hide();
                                sizeFPTransaction.Text = "";
                            }
                        }
                    }
                }
            }
            globalCalculationFur();
        }

        int flyDF = -1;

        private void dfButtonClick(object sender, RoutedEventArgs e)
        {
            Button b = new Button();
            b = (Button)sender;

            for (int i = 0; i < dataFunctionList.Count; i++)
            {
                if (b.Equals(dataFunctionList[i].DFButton))
                {
                    flyDF = i;
                    flyDataFunction.ShowAt(dataFunctionList[i].DFButton);

                    if (dataFunctionList[i].type.Equals("EIF"))
                    {
                        TypeDF.SelectedIndex = 1;
                    }
                    else if (dataFunctionList[i].type.Equals("ILF"))
                    {
                        TypeDF.SelectedIndex = 0;
                    }

                    TextDF.Text = dataFunctionList[i].DFName;
                    DetOfDF.Text = dataFunctionList[i].DFDet;
                    comboBoxRets.SelectedItem = dataFunctionList[i].DFRet;

                    EditButton.IsEnabled = true;
                    DeleteButton.IsEnabled = true;
                    okButton.IsEnabled = false;
                    DetOfDF_SelectionChanged(null, null);
                }
            }
        }

        private void NoDF_Click(object sender, RoutedEventArgs e)
        {
            deleteDataFunction.Hide();
        }

        private void Edit_Click_DataFunction(object sender, RoutedEventArgs e)
        {
            IList<int> auxTransaction = new List<int>();
            for (int k = 0; k < applicationList.Count; k++)
            {
                if ((flyDF >= 0) && (applicationList[k].Equals(dataFunctionList[flyDF].DFApplicationFUR)))
                {
                    applicationList[k].GridApplication.Items.Remove(dataFunctionList[flyDF]);
                    appGridViewListItems[k].Items.Remove(dataFunctionList[flyDF].dFGrid);
                    for (int j = 0; j < dataTransactionList.Count; j++)
                    {
                        if (dataTransactionList[j].dFunction.Equals(dataFunctionList[flyDF]))
                        {
                            auxTransaction.Add(j);
                        }
                    }

                    dataFunctionList.RemoveAt(flyDF);



                    Image img = new Image();
                    img.Source = new BitmapImage(new Uri("ms-appx:///Assets/DataBase Black.png"));
                    //img.Margin = new Thickness(5);

                    ItemsPanelTemplate itemPanel = new ItemsPanelTemplate();
                    //gridView1.ItemsPanel = itemPanel;

                    ItemsControl itemsControl = new ItemsControl();
                    itemsControl.ItemsPanel = itemPanel;

                    ItemsWrapGrid itemsWrap = new ItemsWrapGrid();
                    itemsWrap.MaximumRowsOrColumns = 3;

                    //Check the fur that we are on.  
                    int fCounter = 0;
                    for (int x = 0; x < furList.Count; x++)
                    {
                        if (furList[x].FurDescription.Equals(comboFUR.Description))
                        {
                            fCounter = x;
                        }
                    }

                    //comparar maingrid.item con el grid de applicationList
                    int cApp = 0;
                    for (int i = 0; i < applicationList.Count; i++)
                    {
                        if (applicationList[i].GridApplication.Equals(mainGrid.SelectedItem))
                        {
                            cApp = i;
                        }
                    }

                    double w = applicationList[cApp].GridApplication.ActualWidth;
                    double auxW = (w) / 2.4;

                    double h = applicationList[cApp].GridApplication.ActualHeight;
                    double auxH = (h) / 3;


                    // Create the Grid
                    Grid myGrid = new Grid();
                    myGrid.Width = auxW;
                    myGrid.Height = auxH;


                    // Define the Columns
                    ColumnDefinition colDef1 = new ColumnDefinition();
                    ColumnDefinition colDef2 = new ColumnDefinition();

                    myGrid.ColumnDefinitions.Add(colDef1);
                    myGrid.ColumnDefinitions.Add(colDef2);

                    // Define the Rows
                    RowDefinition rowDef1 = new RowDefinition();
                    RowDefinition rowDef2 = new RowDefinition();
                    RowDefinition rowDef3 = new RowDefinition();
                    RowDefinition rowDef4 = new RowDefinition();
                    myGrid.RowDefinitions.Add(rowDef1);
                    myGrid.RowDefinitions.Add(rowDef2);
                    myGrid.RowDefinitions.Add(rowDef3);
                    myGrid.RowDefinitions.Add(rowDef4);

                    //DetOfDF
                    //comboBoxRets
                    int aux = DetOfDF.Text.Split(',').Length;

                    int retIndex = comboBoxRets.SelectedIndex;
                    ComboBoxItem auxRet = (ComboBoxItem)comboBoxRets.Items.ElementAt(retIndex);

                    int selectedIndex = TypeDF.SelectedIndex;
                    if (selectedIndex >= 0)
                    {
                        ComboBoxItem auxC = (ComboBoxItem)TypeDF.Items.ElementAt(selectedIndex);

                        // Add the second text cell to the Grid
                        TextBlock name = new TextBlock();
                        name.Text = TextDF.Text;
                        name.FontSize = 12;
                        name.HorizontalAlignment = HorizontalAlignment.Center;
                        Grid.SetRow(name, 3);
                        Grid.SetColumn(name, 0);

                        // Add the second text cell to the Grid
                        TextBlock ret = new TextBlock();
                        ret.Text = auxRet.Content.ToString();
                        ret.FontSize = 15;
                        ret.HorizontalAlignment = HorizontalAlignment.Left;
                        Grid.SetRow(ret, 1);
                        Grid.SetColumn(ret, 1);

                        // Add the third text cell to the Grid
                        TextBlock det = new TextBlock();
                        det.Text = aux + "-Dets";
                        det.FontSize = 15;
                        det.HorizontalAlignment = HorizontalAlignment.Left;
                        Grid.SetRow(det, 2);
                        Grid.SetColumn(det, 1);

                        // Add the second text cell to the Grid
                        TextBlock type = new TextBlock();
                        type.Text = (string)auxC.Content;
                        type.HorizontalAlignment = HorizontalAlignment.Center;
                        type.FontSize = 12;
                        Grid.SetRow(type, 0);
                        Grid.SetColumn(type, 0);

                        Button b = new Button();
                        b.Content = img;
                        b.HorizontalAlignment = HorizontalAlignment.Center;
                        b.HorizontalContentAlignment = HorizontalAlignment.Center;
                        b.Click += dfButtonClick;

                        Grid trGrid = new Grid();
                        ColumnDefinition colDef11 = new ColumnDefinition();
                        ColumnDefinition colDef12 = new ColumnDefinition();
                        ColumnDefinition colDef13 = new ColumnDefinition();

                        trGrid.ColumnDefinitions.Add(colDef11);
                        trGrid.ColumnDefinitions.Add(colDef12);
                        trGrid.ColumnDefinitions.Add(colDef13);

                        Button EIButton = new Button();
                        EIButton.Content = "EI";
                        EIButton.FontSize = 7;
                        EIButton.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                        EIButton.BorderThickness = new Thickness(1);
                        EIButton.Background = new SolidColorBrush(Windows.UI.Colors.LightCoral);
                        EIButton.HorizontalAlignment = HorizontalAlignment.Center;
                        EIButton.Visibility = Visibility.Collapsed;
                        EIButton.Click += EIButtonClick;

                        Button EOButton = new Button();
                        EOButton.Content = "EO";
                        EOButton.FontSize = 7;
                        EOButton.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                        EOButton.BorderThickness = new Thickness(1);
                        EOButton.Background = new SolidColorBrush(Windows.UI.Colors.LightCoral);
                        EOButton.HorizontalAlignment = HorizontalAlignment.Center;
                        EOButton.Visibility = Visibility.Collapsed;
                        EOButton.Click += EOButtonClick;

                        Button EQButton = new Button();
                        EQButton.Content = "EQ";
                        EQButton.FontSize = 7;
                        EQButton.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                        EQButton.BorderThickness = new Thickness(1);
                        EQButton.Background = new SolidColorBrush(Windows.UI.Colors.LightCoral);
                        EQButton.HorizontalAlignment = HorizontalAlignment.Center;
                        EQButton.Visibility = Visibility.Collapsed;
                        EQButton.Click += EQButtonClick;

                        Grid.SetColumn(EIButton, 0);
                        Grid.SetColumn(EOButton, 1);
                        Grid.SetColumn(EQButton, 2);

                        trGrid.Children.Add(EQButton);
                        trGrid.Children.Add(EIButton);
                        trGrid.Children.Add(EOButton);

                        Grid.SetRow(b, 1);
                        Grid.SetRowSpan(b, 2);

                        Grid.SetRow(trGrid, 3);
                        Grid.SetColumn(trGrid, 1);

                        myGrid.Children.Add(b);
                        myGrid.Children.Add(name);
                        myGrid.Children.Add(det);
                        myGrid.Children.Add(ret);
                        myGrid.Children.Add(type);
                        myGrid.Children.Add(trGrid);

                        dataFunction auxDF = new dataFunction(name.Text, type.Text, DetOfDF.Text, ret.Text, applicationList[cApp], myGrid, b, EIButton, EOButton, EQButton);

                        dataFunctionList.Add(auxDF);
                        applicationList[cApp].GridApplication.Items.Add(myGrid);

                        for (int l = 0; l < auxTransaction.Count; l++)
                        {
                            dataTransactionList[auxTransaction[l]].DataFunction = auxDF;

                            if (dataTransactionList[auxTransaction[l]].type.Equals("EQ"))
                            {
                                dataTransactionList[auxTransaction[l]].DataFunction.EQButton.Visibility = Visibility.Visible;
                            }
                            else if (dataTransactionList[auxTransaction[l]].type.Equals("EO"))
                            {
                                dataTransactionList[auxTransaction[l]].DataFunction.EOButton.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                dataTransactionList[auxTransaction[l]].DataFunction.EIButton.Visibility = Visibility.Visible;
                            }
                        }

                        deleteDataFunction.Hide();
                        flyDataFunction.Hide();
                        TypeDF.SelectedIndex = -1;
                        TextDF.Text = "";
                    }
                }
            }
            globalCalculationFur();

        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            deleteDataFunction.ShowAt(DeleteButton);
        }

        private void YesDF_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < applicationList.Count; i++)
            {
                if (flyDF >= 0)
                {

                    if (applicationList[i].Equals(dataFunctionList[flyDF].DFApplicationFUR))
                    {
                        for (int t = 0; t < dataTransactionList.Count; t++)
                        {
                            if (dataFunctionList[flyDF].Equals(dataTransactionList[t].DataFunction))
                            {
                                dataTransactionList.RemoveAt(t);
                            }

                        }

                        applicationList[i].GridApplication.Items.Remove(dataFunctionList[flyDF]);
                        appGridViewListItems[i].Items.Remove(dataFunctionList[flyDF].dFGrid);
                        dataFunctionList.RemoveAt(flyDF);

                        flyDF--;
                        deleteDataFunction.Hide();
                    }
                }

            }
            globalCalculationFur();


        }
        private void showDataFunction_Click(object sender, RoutedEventArgs e)
        {
            listB.Items.Clear();
            //CreateFunctionButton.Flyout = flyDataFunction;

            for (int i = 0; i < dataFunctionList.Count; i++)
            {
                Grid myGrid = new Grid();
                myGrid.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                myGrid.BorderThickness = new Thickness(1);
                // Define the Columns
                ColumnDefinition colDef1 = new ColumnDefinition();
                ColumnDefinition colDef2 = new ColumnDefinition();
                ColumnDefinition colDef3 = new ColumnDefinition();
                ColumnDefinition colDef4 = new ColumnDefinition();
                colDef1.Width = new GridLength(205);
                colDef2.Width = new GridLength(110);
                colDef3.Width = new GridLength(100);
                colDef4.Width = new GridLength(150);

                myGrid.ColumnDefinitions.Add(colDef1);
                myGrid.ColumnDefinitions.Add(colDef2);
                myGrid.ColumnDefinitions.Add(colDef3);
                myGrid.ColumnDefinitions.Add(colDef4);
                TextBlock t1 = new TextBlock();
                t1.HorizontalAlignment = HorizontalAlignment.Center;
                t1.TextWrapping = TextWrapping.Wrap;

                TextBlock t2 = new TextBlock();
                t2.HorizontalAlignment = HorizontalAlignment.Center;

                TextBlock t3 = new TextBlock();
                t3.HorizontalAlignment = HorizontalAlignment.Center;
                t3.MaxWidth = 102;
                t3.TextWrapping = TextWrapping.Wrap;

                TextBlock t4 = new TextBlock();
                t4.HorizontalAlignment = HorizontalAlignment.Center;
                t4.MaxWidth = 102;
                t4.TextWrapping = TextWrapping.Wrap;

                Grid.SetColumn(t1, 0);
                myGrid.Children.Add(t1);

                Grid.SetColumn(t2, 1);
                myGrid.Children.Add(t2);

                Grid.SetColumn(t3, 2);
                myGrid.Children.Add(t3);

                Grid.SetColumn(t4, 3);
                myGrid.Children.Add(t4);

                t1.Text = dataFunctionList[i].DFName;
                t2.Text = dataFunctionList[i].DFType;
                //t3.Text = oneDTCalculation(dataTransactionList[i]);
                //t3.Text = "asd";
                t3.Text = oneDFCalculation(dataFunctionList[i]);
                t4.Text = dataFunctionList[i].DFApplicationFUR.AppName;

                listB.Items.Add(myGrid);
            }
        }
        private void showTransactionFunction_Click(object sender, RoutedEventArgs e)
        {
            ListTF.Items.Clear();
            //CreateFunctionButton.Flyout = flyDataFunction;

            for (int i = 0; i < dataTransactionList.Count; i++)
            {
                Grid myGrid = new Grid();
                myGrid.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                myGrid.BorderThickness = new Thickness(1);
                // Define the Columns
                ColumnDefinition colDef1 = new ColumnDefinition();
                ColumnDefinition colDef2 = new ColumnDefinition();
                ColumnDefinition colDef3 = new ColumnDefinition();
                ColumnDefinition colDef4 = new ColumnDefinition();
                colDef1.Width = new GridLength(205);
                colDef2.Width = new GridLength(110);
                colDef3.Width = new GridLength(100);
                colDef4.Width = new GridLength(150);

                myGrid.ColumnDefinitions.Add(colDef1);
                myGrid.ColumnDefinitions.Add(colDef2);
                myGrid.ColumnDefinitions.Add(colDef3);
                myGrid.ColumnDefinitions.Add(colDef4);
                TextBlock t1 = new TextBlock();
                t1.HorizontalAlignment = HorizontalAlignment.Center;
                t1.TextWrapping = TextWrapping.Wrap;

                TextBlock t2 = new TextBlock();
                t2.HorizontalAlignment = HorizontalAlignment.Center;

                TextBlock t3 = new TextBlock();
                t3.HorizontalAlignment = HorizontalAlignment.Center;
                t3.MaxWidth = 102;
                t3.TextWrapping = TextWrapping.Wrap;

                TextBlock t4 = new TextBlock();
                t4.HorizontalAlignment = HorizontalAlignment.Center;
                t4.MaxWidth = 102;
                t4.TextWrapping = TextWrapping.Wrap;

                Grid.SetColumn(t1, 0);
                myGrid.Children.Add(t1);

                Grid.SetColumn(t2, 1);
                myGrid.Children.Add(t2);

                Grid.SetColumn(t3, 2);
                myGrid.Children.Add(t3);

                Grid.SetColumn(t4, 3);
                myGrid.Children.Add(t4);

                t1.Text = dataTransactionList[i].DTName;
                t2.Text = dataTransactionList[i].DTType;
                //t3.Text = oneDTCalculation(dataTransactionList[i]);
                //t3.Text = "asd";
                t3.Text = oneDTCalculation(dataTransactionList[i]);
                t4.Text = dataTransactionList[i].DFApplicationFUR.AppName;

                ListTF.Items.Add(myGrid);
            }
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItem typeItem = (ComboBoxItem)TypeDF.SelectedItem;
            Button auxButton = new Button();
            CheckBox checkBox = new CheckBox();
            checkBox.Content = "";

            dataFunction = TextDF.Text;
            typeDataFunction = typeItem.Content.ToString();
            if (dataFunction != null)
            {
                //CheckFTR.Items.Add(checkBox);
                addDFtoFTR.Items.Add(dataFunction);
            }
        }

        public int furCounter = 0;
        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            shortFUR.Text = "";
            LongFUR.Text = "";
        }

        private async void okFUR_Click(object sender, RoutedEventArgs e)
        {
            if ((shortFUR.Text != null) && (LongFUR.Text != null))
            {
                Fur aux = new Fur("Fur" + furCounter, shortFUR.Text, LongFUR.Text);

                //Check if that Fur already exists.
                bool alreadyExists = furList.Any(x => x.FurDescription == aux.FurDescription || x.FurLongDescription == aux.FurLongDescription);

                if (!alreadyExists)
                {
                    furList.Add(aux);
                    comboFUR.Items.Add(furList[furCounter].FurDescription);
                    comboFUR.SelectedValue = (furList[furCounter].FurDescription);
                    furCounter++;
                }
                else
                {
                    string message = "Use another Fur name this one is already used.";
                    MessageDialog msgdialog = new MessageDialog(message, "Warning!!");
                    await msgdialog.ShowAsync();
                }
            }
            flyFurCombo.Hide();

        }

        private async void appNameButton_Click(object sender, RoutedEventArgs e)
        {
            string auxName = appNameCreateTextBox.Text;

            if (auxName == null)
            {
                string message = "Please enter an application name.";
                MessageDialog msgdialog = new MessageDialog(message, "Warning!!");
                await msgdialog.ShowAsync();
            }
            else
            {
                double w = mainGrid.ActualWidth;
                double auxW = (w) / 2.4;

                double h = mainGrid.ActualHeight;
                double auxH = (h) / 2.4;

                Boolean appMainCheck;

                GridView gridView = new GridView();
                gridView.CornerRadius = new CornerRadius(5);
                gridView.Margin = new Thickness(35);
                gridView.Header = auxName;
                gridView.VerticalAlignment = VerticalAlignment.Stretch;
                gridView.HorizontalAlignment = HorizontalAlignment.Stretch;
                gridView.CanReorderItems = true;
                gridView.AllowDrop = true;
                gridView.BorderThickness = new Thickness(2);
                gridView.Width = auxW;
                gridView.Height = auxH;
                gridView.Name = "applicationGridView" + applicationGridView;
                gridView.LostFocus += GridView_LostFocus;

                //Check if the checkBox is selected to be the main application.
                if (checkApplication.IsChecked == true)
                {
                    appMainCheck = true;
                    gridView.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Red);
                }
                else
                {
                    appMainCheck = false;
                    gridView.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Green);
                }

                //Check the fur that we are on.   
                for (int x = 0; x < furList.Count; x++)
                {
                    if (furList[x].FurDescription.Equals(comboFUR.Description))
                    {
                        applicationCounter = x;
                    }
                }
                //Check the list
                Boolean oneMainApplication = false;
                for (int y = 0; y < applicationList.Count; y++)
                {
                    if ((applicationList[y].AppBoolean.Equals(true)) && (appMainCheck == true) && (applicationList[y].FurRef.FurDescription.Equals(comboFUR.SelectedItem)))
                    {
                        oneMainApplication = true;
                        string message = "You already have a main application.";
                        MessageDialog msgdialog = new MessageDialog(message, "Warning!!");
                        await msgdialog.ShowAsync();
                    }
                    else
                    {

                    }
                    if (applicationList[y].AppName.Equals(appNameCreateTextBox.Text) && (applicationList[y].FurRef.FurDescription.Equals(comboFUR.SelectedItem)))
                    {
                        oneMainApplication = true;
                        string message = "You already have an application with this name." +
                            "Please Trype a different name.";
                        MessageDialog msgdialog = new MessageDialog(message, "Warning!!");
                        await msgdialog.ShowAsync();
                    }
                }
                if (!oneMainApplication == true && applicationCounter >= 0)
                {
                    applicationFur appFur = new applicationFur(auxName, appMainCheck, furList[applicationCounter], gridView);
                    applicationList.Add(appFur);

                    //   if(mainGrid.Items.Equals(appFur.GridApplication))
                    // {
                    if (appFur.GridApplication.Equals(mainGrid.Items))
                    {

                    }
                    mainGrid.Items.Add(gridView);
                    applicationGridView++;

                    //Add gridview to List to be able to have access
                    appGridViewListItems.Add(gridView);

                    appNameCreateTextBox.Text = "";
                    checkApplication.IsChecked = false;
                }
            }
            appNameCreation.Hide();
        }

        private void mainGrid_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {

            //comparar maingrid.item con el grid de applicationList
            int cApp = -1;

            for (int i = 0; i < applicationList.Count; i++)
            {
                if (applicationList[i].GridApplication.Equals(mainGrid.SelectedItem))
                {
                    cApp = i;
                }
            }

            if (cApp >= 0)
            {
                if (e.Key == Windows.System.VirtualKey.Delete)
                {
                    deleteElement.ShowAt(applicationList[cApp].GridApplication);
                }
            }
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {

            for (int i = 0; i < applicationList.Count; i++)
            {
                if (applicationList[i].GridApplication.Equals(mainGrid.SelectedItem))
                {
                    for (int j = 0; j < dataFunctionList.Count; j++)
                    {
                        if (dataFunctionList[j].DFApplicationFUR.Equals(applicationList[i]))
                        {
                            applicationList[i].GridApplication.Items.Remove(dataFunctionList[j]);
                            appGridViewListItems[i].Items.Remove(dataFunctionList[j].dFGrid);
                            dataFunctionList.RemoveAt(j);

                            deleteDataFunction.Hide();
                        }
                    }
                    mainGrid.Items.Remove(applicationList[i].GridApplication);


                    for (int j = 0; j < dataTransactionList.Count; j++)
                    {
                        if (dataTransactionList[j].appFur.Equals(applicationList[i]))
                        {
                            dataTransactionList.RemoveAt(j);
                        }
                    }
                    for (int k = 0; k < dataFunctionList.Count; k++)
                    {
                        if (dataFunctionList[k].appFur.Equals(applicationList[i]))
                        {
                            dataFunctionList.RemoveAt(k);
                        }
                    }
                    applicationList.RemoveAt(i);
                    //applicationCounter--;
                }

            }
            globalCalculationFur();
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            deleteElement.Hide();
        }

        private void comboFUR_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            for (int i = 0; i < furList.Count; i++)
            {
                if (comboFUR.SelectedItem.Equals(furList[i].FurDescription))
                {
                    LongDescriptionTextBox.Text = furList[i].FurLongDescription;

                    applicationCounter = i;

                    mainGrid.Items.Clear();

                    for (int j = 0; j < applicationList.Count; j++)
                    {
                        if (applicationList[j].FurRef.Equals(furList[i]))
                        {
                            mainGrid.Items.Add(applicationList[j].GridApplication);
                        }
                        else
                        {
                            mainGrid.Items.Remove(applicationList[j].GridApplication);
                        }
                    }
                }
            }
            globalCalculationFur();

        }


        //Make the EIF and EIF Enable and disable depending on if we selected a main application or a secondary application.
        private async void flyDataFunction_Opened(object sender, object e)
        {

            int cApp = 0;
            for (int i = 0; i < applicationList.Count; i++)
            {
                if (applicationList[i].GridApplication.Equals(mainGrid.SelectedItem))
                {
                    cApp = i;
                }
            }

            if (applicationList.Count == 0)
            {
                string message = "There are not applicattions created for the data base to be added to " +
                       "Please create an application before adding data functions.";
                MessageDialog msgdialog = new MessageDialog(message, "Warning!!");
                await msgdialog.ShowAsync();
            }
            else
            {
                if ((flyDataFunction.IsOpen == true) && (applicationList[cApp].AppBoolean == true))
                {
                    ComboILF.IsEnabled = true;
                    ComboEIF.IsEnabled = false;
                }
                else if ((flyDataFunction.IsOpen == true) && (applicationList[cApp].AppBoolean == false))
                {
                    ComboILF.IsEnabled = false;
                    ComboEIF.IsEnabled = true;
                }
            }
        }

        private void Ok_Click_DataFunction(object sender, RoutedEventArgs e)
        {
            Image img = new Image();
            img.Source = new BitmapImage(new Uri("ms-appx:///Assets/DataBase Black.png"));
            //img.Margin = new Thickness(5);

            ItemsPanelTemplate itemPanel = new ItemsPanelTemplate();
            //gridView1.ItemsPanel = itemPanel;

            ItemsControl itemsControl = new ItemsControl();
            itemsControl.ItemsPanel = itemPanel;

            ItemsWrapGrid itemsWrap = new ItemsWrapGrid();
            itemsWrap.MaximumRowsOrColumns = 3;

            //Check the fur that we are on.  
            int fCounter = 0;
            for (int x = 0; x < furList.Count; x++)
            {
                if (furList[x].FurDescription.Equals(comboFUR.Description))
                {
                    fCounter = x;
                }
            }

            //comparar maingrid.item con el grid de applicationList
            int cApp = 0;
            for (int i = 0; i < applicationList.Count; i++)
            {
                if (applicationList[i].GridApplication.Equals(mainGrid.SelectedItem))
                {
                    cApp = i;
                }
            }

            double w = applicationList[cApp].GridApplication.ActualWidth;
            double auxW = (w) / 2.4;

            double h = applicationList[cApp].GridApplication.ActualHeight;
            double auxH = (h) / 3;

            // Create the Grid
            Grid myGrid = new Grid();
            myGrid.Width = auxW;
            myGrid.Height = auxH;

            // Define the Columns
            ColumnDefinition colDef1 = new ColumnDefinition();
            ColumnDefinition colDef2 = new ColumnDefinition();

            myGrid.ColumnDefinitions.Add(colDef1);
            myGrid.ColumnDefinitions.Add(colDef2);

            // Define the Rows
            RowDefinition rowDef1 = new RowDefinition();
            RowDefinition rowDef2 = new RowDefinition();
            RowDefinition rowDef3 = new RowDefinition();
            RowDefinition rowDef4 = new RowDefinition();
            myGrid.RowDefinitions.Add(rowDef1);
            myGrid.RowDefinitions.Add(rowDef2);
            myGrid.RowDefinitions.Add(rowDef3);
            myGrid.RowDefinitions.Add(rowDef4);
            //DetOfDF
            //comboBoxRets
            int aux = DetOfDF.Text.Split(',').Length;

            int retIndex = comboBoxRets.SelectedIndex;
            ComboBoxItem auxRet = (ComboBoxItem)comboBoxRets.Items.ElementAt(retIndex);

            int selectedIndex = TypeDF.SelectedIndex;
            if (selectedIndex >= 0)
            {
                ComboBoxItem auxC = (ComboBoxItem)TypeDF.Items.ElementAt(selectedIndex);

                // Add the second text cell to the Grid
                TextBlock name = new TextBlock();
                name.Text = TextDF.Text;
                name.FontSize = 12;
                name.HorizontalAlignment = HorizontalAlignment.Center;
                Grid.SetRow(name, 3);
                Grid.SetColumn(name, 0);

                // Add the second text cell to the Grid
                TextBlock ret = new TextBlock();
                ret.Text = auxRet.Content.ToString();
                ret.FontSize = 15;
                ret.HorizontalAlignment = HorizontalAlignment.Left;
                Grid.SetRow(ret, 1);
                Grid.SetColumn(ret, 1);

                // Add the third text cell to the Grid
                TextBlock det = new TextBlock();
                det.Text = aux + "-Dets";
                det.FontSize = 15;
                det.HorizontalAlignment = HorizontalAlignment.Left;
                Grid.SetRow(det, 2);
                Grid.SetColumn(det, 1);

                // Add the second text cell to the Grid
                TextBlock type = new TextBlock();
                type.Text = (string)auxC.Content;
                type.HorizontalAlignment = HorizontalAlignment.Center;
                type.FontSize = 12;
                Grid.SetRow(type, 0);
                Grid.SetColumn(type, 0);

                Button b = new Button();
                b.Content = img;
                b.HorizontalAlignment = HorizontalAlignment.Center;
                b.HorizontalContentAlignment = HorizontalAlignment.Center;
                b.Click += dfButtonClick;

                Grid trGrid = new Grid();
                ColumnDefinition colDef11 = new ColumnDefinition();
                ColumnDefinition colDef12 = new ColumnDefinition();
                ColumnDefinition colDef13 = new ColumnDefinition();

                trGrid.ColumnDefinitions.Add(colDef11);
                trGrid.ColumnDefinitions.Add(colDef12);
                trGrid.ColumnDefinitions.Add(colDef13);

                Button EIButton = new Button();
                EIButton.Content = "EI";
                EIButton.FontSize = 7;
                EIButton.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                EIButton.BorderThickness = new Thickness(1);
                EIButton.Background = new SolidColorBrush(Windows.UI.Colors.LightCoral);
                EIButton.HorizontalAlignment = HorizontalAlignment.Center;
                EIButton.Visibility = Visibility.Collapsed;
                EIButton.Click += EIButtonClick;

                Button EOButton = new Button();
                EOButton.Content = "EO";
                EOButton.FontSize = 7;
                EOButton.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                EOButton.BorderThickness = new Thickness(1);
                EOButton.Background = new SolidColorBrush(Windows.UI.Colors.LightCoral);
                EOButton.HorizontalAlignment = HorizontalAlignment.Center;
                EOButton.Visibility = Visibility.Collapsed;
                EOButton.Click += EOButtonClick;

                Button EQButton = new Button();
                EQButton.Content = "EQ";
                EQButton.FontSize = 7;
                EQButton.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                EQButton.BorderThickness = new Thickness(1);
                EQButton.Background = new SolidColorBrush(Windows.UI.Colors.LightCoral);
                EQButton.HorizontalAlignment = HorizontalAlignment.Center;
                EQButton.Visibility = Visibility.Collapsed;
                EQButton.Click += EQButtonClick;

                Grid.SetColumn(EIButton, 0);
                Grid.SetColumn(EOButton, 1);
                Grid.SetColumn(EQButton, 2);

                trGrid.Children.Add(EQButton);
                trGrid.Children.Add(EIButton);
                trGrid.Children.Add(EOButton);

                Grid.SetRow(b, 1);
                Grid.SetRowSpan(b, 2);

                Grid.SetRow(trGrid, 3);
                Grid.SetColumn(trGrid, 1);

                myGrid.Children.Add(b);
                myGrid.Children.Add(name);
                myGrid.Children.Add(det);
                myGrid.Children.Add(ret);
                myGrid.Children.Add(type);
                myGrid.Children.Add(trGrid);


                dataFunction auxDF = new dataFunction(name.Text, type.Text, DetOfDF.Text, ret.Text, applicationList[cApp], myGrid, b, EIButton, EOButton, EQButton);
                dataFunctionList.Add(auxDF);
                applicationList[cApp].GridApplication.Items.Add(myGrid);

                flyDataFunction.Hide();
                TypeDF.SelectedIndex = -1;
                TextDF.Text = "";
                comboBoxRets.SelectedIndex = 0;
                DetOfDF.Text = "";
            }

            globalCalculationFur();
        }

        private void EIButtonClick(object sender, RoutedEventArgs e)
        {
            Button b = new Button();
            b = (Button)sender;

            listDT.Items.Clear();

            for (int i = 0; i < dataTransactionList.Count; i++)
            {
                if ((dataTransactionList[i].DataFunction.EIButton.Equals(b)) && (dataTransactionList[i].type.Equals("EI")))
                {
                    Grid myGrid = new Grid();
                    myGrid.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                    myGrid.BorderThickness = new Thickness(1);
                    // Define the Columns
                    ColumnDefinition colDef1 = new ColumnDefinition();
                    ColumnDefinition colDef2 = new ColumnDefinition();
                    ColumnDefinition colDef3 = new ColumnDefinition();
                    ColumnDefinition colDef4 = new ColumnDefinition();
                    colDef1.Width = new GridLength(205);
                    colDef2.Width = new GridLength(110);
                    colDef3.Width = new GridLength(100);
                    colDef4.Width = new GridLength(150);

                    myGrid.ColumnDefinitions.Add(colDef1);
                    myGrid.ColumnDefinitions.Add(colDef2);
                    myGrid.ColumnDefinitions.Add(colDef3);
                    myGrid.ColumnDefinitions.Add(colDef4);
                    TextBlock t1 = new TextBlock();
                    t1.HorizontalAlignment = HorizontalAlignment.Center;
                    t1.TextWrapping = TextWrapping.Wrap;

                    TextBlock t2 = new TextBlock();
                    t2.HorizontalAlignment = HorizontalAlignment.Center;

                    TextBlock t3 = new TextBlock();
                    t3.HorizontalAlignment = HorizontalAlignment.Center;
                    t3.MaxWidth = 102;
                    t3.TextWrapping = TextWrapping.Wrap;

                    TextBlock t4 = new TextBlock();
                    t4.HorizontalAlignment = HorizontalAlignment.Center;
                    t4.MaxWidth = 102;
                    t4.TextWrapping = TextWrapping.Wrap;

                    Grid.SetColumn(t1, 0);
                    myGrid.Children.Add(t1);

                    Grid.SetColumn(t2, 1);
                    myGrid.Children.Add(t2);

                    Grid.SetColumn(t3, 2);
                    myGrid.Children.Add(t3);

                    Grid.SetColumn(t4, 3);
                    myGrid.Children.Add(t4);

                    t1.Text = dataTransactionList[i].DTName;
                    t2.Text = dataTransactionList[i].DTType;
                    t3.Text = oneDTCalculation(dataTransactionList[i]);
                    t4.Text = dataTransactionList[i].DFApplicationFUR.AppName;
                    listDT.SelectionMode = SelectionMode.Single;
                    listDT.Items.Add(myGrid);
                    DTandGRID aux = new DTandGRID(dataTransactionList[i], myGrid);
                    DTGridList.Add(aux);

                    flyShowTransaction.ShowAt(b);
                }
            }
        }

        private void EQButtonClick(object sender, RoutedEventArgs e)
        {
            Button b = new Button();
            b = (Button)sender;

            listDT.Items.Clear();

            for (int i = 0; i < dataTransactionList.Count; i++)
            {
                if ((dataTransactionList[i].DataFunction.EQButton.Equals(b)) && (dataTransactionList[i].type.Equals("EQ")))
                {
                    Grid myGrid = new Grid();
                    myGrid.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                    myGrid.BorderThickness = new Thickness(1);
                    // Define the Columns
                    ColumnDefinition colDef1 = new ColumnDefinition();
                    ColumnDefinition colDef2 = new ColumnDefinition();
                    ColumnDefinition colDef3 = new ColumnDefinition();
                    ColumnDefinition colDef4 = new ColumnDefinition();
                    colDef1.Width = new GridLength(205);
                    colDef2.Width = new GridLength(110);
                    colDef3.Width = new GridLength(100);
                    colDef4.Width = new GridLength(150);

                    myGrid.ColumnDefinitions.Add(colDef1);
                    myGrid.ColumnDefinitions.Add(colDef2);
                    myGrid.ColumnDefinitions.Add(colDef3);
                    myGrid.ColumnDefinitions.Add(colDef4);
                    TextBlock t1 = new TextBlock();
                    t1.HorizontalAlignment = HorizontalAlignment.Center;
                    t1.TextWrapping = TextWrapping.Wrap;

                    TextBlock t2 = new TextBlock();
                    t2.HorizontalAlignment = HorizontalAlignment.Center;

                    TextBlock t3 = new TextBlock();
                    t3.HorizontalAlignment = HorizontalAlignment.Center;
                    t3.MaxWidth = 102;
                    t3.TextWrapping = TextWrapping.Wrap;

                    TextBlock t4 = new TextBlock();
                    t4.HorizontalAlignment = HorizontalAlignment.Center;
                    t4.MaxWidth = 102;
                    t4.TextWrapping = TextWrapping.Wrap;

                    Grid.SetColumn(t1, 0);
                    myGrid.Children.Add(t1);

                    Grid.SetColumn(t2, 1);
                    myGrid.Children.Add(t2);

                    Grid.SetColumn(t3, 2);
                    myGrid.Children.Add(t3);

                    Grid.SetColumn(t4, 3);
                    myGrid.Children.Add(t4);

                    t1.Text = dataTransactionList[i].DTName;
                    t2.Text = dataTransactionList[i].DTType;
                    //t3.Text = oneDFCalculation(dataTransactionList[i].dFunction);
                    t3.Text = oneDTCalculation(dataTransactionList[i]);
                    t4.Text = dataTransactionList[i].DFApplicationFUR.AppName;
                    listDT.SelectionMode = SelectionMode.Single;
                    listDT.Items.Add(myGrid);
                    DTandGRID aux = new DTandGRID(dataTransactionList[i], myGrid);
                    DTGridList.Add(aux);

                    flyShowTransaction.ShowAt(b);
                }
            }
        }

        private void EOButtonClick(object sender, RoutedEventArgs e)
        {
            Button b = new Button();
            b = (Button)sender;

            listDT.Items.Clear();

            for (int i = 0; i < dataTransactionList.Count; i++)
            {
                if ((dataTransactionList[i].DataFunction.EOButton.Equals(b)) && (dataTransactionList[i].type.Equals("EO")))
                {
                    Grid myGrid = new Grid();
                    myGrid.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                    myGrid.BorderThickness = new Thickness(1);
                    // Define the Columns
                    ColumnDefinition colDef1 = new ColumnDefinition();
                    ColumnDefinition colDef2 = new ColumnDefinition();
                    ColumnDefinition colDef3 = new ColumnDefinition();
                    ColumnDefinition colDef4 = new ColumnDefinition();
                    colDef1.Width = new GridLength(205);
                    colDef2.Width = new GridLength(110);
                    colDef3.Width = new GridLength(100);
                    colDef4.Width = new GridLength(150);

                    myGrid.ColumnDefinitions.Add(colDef1);
                    myGrid.ColumnDefinitions.Add(colDef2);
                    myGrid.ColumnDefinitions.Add(colDef3);
                    myGrid.ColumnDefinitions.Add(colDef4);
                    TextBlock t1 = new TextBlock();
                    t1.HorizontalAlignment = HorizontalAlignment.Center;
                    t1.TextWrapping = TextWrapping.Wrap;

                    TextBlock t2 = new TextBlock();
                    t2.HorizontalAlignment = HorizontalAlignment.Center;

                    TextBlock t3 = new TextBlock();
                    t3.HorizontalAlignment = HorizontalAlignment.Center;
                    t3.MaxWidth = 102;
                    t3.TextWrapping = TextWrapping.Wrap;

                    TextBlock t4 = new TextBlock();
                    t4.HorizontalAlignment = HorizontalAlignment.Center;
                    t4.MaxWidth = 102;
                    t4.TextWrapping = TextWrapping.Wrap;

                    Grid.SetColumn(t1, 0);
                    myGrid.Children.Add(t1);

                    Grid.SetColumn(t2, 1);
                    myGrid.Children.Add(t2);

                    Grid.SetColumn(t3, 2);
                    myGrid.Children.Add(t3);

                    Grid.SetColumn(t4, 3);
                    myGrid.Children.Add(t4);

                    t1.Text = dataTransactionList[i].DTName;
                    t2.Text = dataTransactionList[i].DTType;
                    //t3.Text = oneDFCalculation(dataTransactionList[i].dFunction);
                    t3.Text = oneDTCalculation(dataTransactionList[i]);
                    t4.Text = dataTransactionList[i].DFApplicationFUR.AppName;
                    listDT.SelectionMode = SelectionMode.Single;
                    listDT.Items.Add(myGrid);
                    DTandGRID aux = new DTandGRID(dataTransactionList[i], myGrid);
                    DTGridList.Add(aux);

                    flyShowTransaction.ShowAt(b);
                }
            }
        }

        private int DFCalculation()
        {

            //Check the fur that we are on.   
            for (int x = 0; x < furList.Count; x++)
            {
                if (furList[x].FurDescription.Equals(comboFUR.Description))
                {
                    applicationCounter = x;
                }
            }

            int c = 0;
            for (int i = 0; i < dataFunctionList.Count; i++)
            {
                if (dataFunctionList[i].DFApplicationFUR.FurRef.Equals(furList[applicationCounter]))
                {
                    c += oneDFCalculationN(dataFunctionList[i]);
                }
            }

            return c;


        }

        private string oneDFCalculation(dataFunction df)
        {
            string complexity;
            int c = 0;
            if (df.type.Equals("EIF"))
            {

                int d = df.DFDet.Split(',').Length;

                int r = 0;
                switch (df.ret)
                {
                    case "1-RET":
                        r = 1;
                        break;
                    case "2-RETs":
                        r = 2;
                        break;
                    case "3-RETs":
                        r = 3;
                        break;
                    case "4-RETs":
                        r = 4;
                        break;
                    case "5-RETs":
                        r = 5;
                        break;
                    case "< 5-RETs":
                        r = 6;
                        break;
                }

                complexity = EIFandILFCalculation(d, r);

                switch (complexity)
                {
                    case "L":
                        c = 5;
                        break;

                    case "A":
                        c = 7;
                        break;

                    case "H":
                        c = 10;
                        break;
                }

            }
            else if (df.type.Equals("ILF"))
            {
                int d = df.DFDet.Split(',').Length;

                int r = 0;
                switch (df.ret)
                {
                    case "1-RET":
                        r = 1;
                        break;
                    case "2-RETs":
                        r = 2;
                        break;
                    case "3-RETs":
                        r = 3;
                        break;
                    case "4-RETs":
                        r = 4;
                        break;
                    case "5-RETs":
                        r = 5;
                        break;
                    case "< 5-RETs":
                        r = 6;
                        break;
                }
                complexity = EIFandILFCalculation(d, r);

                switch (complexity)
                {
                    case "L":
                        c = 7;
                        break;

                    case "A":
                        c = 10;
                        break;

                    case "H":
                        c = 15;
                        break;
                }
            }
            return c.ToString() + "FPs";
        }

        private int oneDFCalculationN(dataFunction df)
        {
            string complexity;
            int c = 0;
            if (df.type.Equals("EIF"))
            {

                int d = df.DFDet.Split(',').Length;

                int r = 0;
                switch (df.ret)
                {
                    case "1-RET":
                        r = 1;
                        break;
                    case "2-RETs":
                        r = 2;
                        break;
                    case "3-RETs":
                        r = 3;
                        break;
                    case "4-RETs":
                        r = 4;
                        break;
                    case "5-RETs":
                        r = 5;
                        break;
                    case "< 5-RETs":
                        r = 6;
                        break;
                }

                complexity = EIFandILFCalculation(d, r);

                switch (complexity)
                {
                    case "L":
                        c = 5;
                        break;

                    case "A":
                        c = 7;
                        break;

                    case "H":
                        c = 10;
                        break;
                }

            }
            else if (df.type.Equals("ILF"))
            {
                int d = df.DFDet.Split(',').Length;

                int r = 0;
                switch (df.ret)
                {
                    case "1-RET":
                        r = 1;
                        break;
                    case "2-RETs":
                        r = 2;
                        break;
                    case "3-RETs":
                        r = 3;
                        break;
                    case "4-RETs":
                        r = 4;
                        break;
                    case "5-RETs":
                        r = 5;
                        break;
                    case "< 5-RETs":
                        r = 6;
                        break;
                }
                complexity = EIFandILFCalculation(d, r);

                switch (complexity)
                {
                    case "L":
                        c = 7;
                        break;

                    case "A":
                        c = 10;
                        break;

                    case "H":
                        c = 15;
                        break;
                }
            }
            return c;
        }

        private string oneDFCalculationDetRet(int det, int ret, string type)
        {
            string complexity;
            int c = 0;
            if (type.Equals("EIF"))
            {

                complexity = EIFandILFCalculation(det, ret);

                switch (complexity)
                {
                    case "L":
                        c = 5;
                        break;

                    case "A":
                        c = 7;
                        break;

                    case "H":
                        c = 10;
                        break;
                }

            }
            else if (type.Equals("ILF"))
            {
                complexity = EIFandILFCalculation(det, ret);

                switch (complexity)
                {
                    case "L":
                        c = 7;
                        break;

                    case "A":
                        c = 10;
                        break;

                    case "H":
                        c = 15;
                        break;
                }
            }
            return c.ToString() + "FPs";
        }

        private string EIFandILFCalculation(int det, int ret)
        {
            String complexity = "L";
            if (ret == 1)
            {
                if (det < 20)
                {
                    complexity = "L";
                }
                else if ((det > 19) && (det < 51))
                {
                    complexity = "L";
                }
                else if (det > 50)
                {
                    complexity = "A";
                }


            }
            else if ((ret > 1) && (ret < 6))
            {
                if (det < 20)
                {
                    complexity = "L";
                }
                else if ((det > 19) && (det < 51))
                {
                    complexity = "A";
                }
                else if (det > 50)
                {
                    complexity = "H";
                }

            }
            else
            {
                if (det < 20)
                {
                    complexity = "A";
                }
                else if ((det > 19) && (det < 51))
                {
                    complexity = "H";
                }
                else if (det > 50)
                {
                    complexity = "H";
                }

            }
            return complexity;
        }

        private string oneDTCalculation(dataTransaction dt)
        {
            string complexity;
            int c = 0;
            if (dt.type.Equals("EI"))
            {
                int d = dt.detSelected.Items.Count;
                int f = dt.SelectedDF.Items.Count;

                complexity = EIcalculation(d, f);

                switch (complexity)
                {
                    case "L":
                        c = 3;
                        break;

                    case "A":
                        c = 4;
                        break;

                    case "H":
                        c = 6;
                        break;
                }

            }
            else if (dt.type.Equals("EQ"))
            {
                int d = dt.detSelected.Items.Count;
                int f = dt.SelectedDF.Items.Count;

                complexity = EOEQcalculation(d, f);

                switch (complexity)
                {
                    case "L":
                        c = 3;
                        break;

                    case "A":
                        c = 4;
                        break;

                    case "H":
                        c = 6;
                        break;
                }

            }
            else if (dt.type.Equals("EO"))
            {
                int d = dt.detSelected.Items.Count;
                int f = dt.SelectedDF.Items.Count;

                complexity = EOEQcalculation(d, f);

                switch (complexity)
                {
                    case "L":
                        c = 4;
                        break;

                    case "A":
                        c = 5;
                        break;

                    case "H":
                        c = 7;
                        break;
                }
            }
            return c.ToString() + "FPs";
        }

        private int oneDTCalculationNumber(dataTransaction dt)
        {
            int auxCheckFur = 0;
            //Check the fur that we are on.   
            for (int x = 0; x < furList.Count; x++)
            {
                if (furList[x].FurDescription.Equals(comboFUR.Description))
                {
                    auxCheckFur = x;
                }
            }

            string complexity;
            int c = 0;

            for (int i = 0; i < dataTransactionList.Count; i++)
            {

                if ((dt.type.Equals("EI")) && (dataTransactionList[i].DFApplicationFUR.FurRef.Equals(furList[auxCheckFur])))
                {
                    int d = dt.detSelected.Items.Count;
                    int f = dt.SelectedDF.Items.Count;

                    complexity = EIcalculation(d, f);

                    switch (complexity)
                    {
                        case "L":
                            c = 3;
                            break;

                        case "A":
                            c = 4;
                            break;

                        case "H":
                            c = 6;
                            break;
                    }

                }
                else if ((dt.type.Equals("EQ")) && (dataTransactionList[i].DFApplicationFUR.FurRef.Equals(furList[auxCheckFur])))
                {
                    int d = dt.detSelected.Items.Count;
                    int f = dt.SelectedDF.Items.Count;

                    complexity = EOEQcalculation(d, f);

                    switch (complexity)
                    {
                        case "L":
                            c = 3;
                            break;

                        case "A":
                            c = 4;
                            break;

                        case "H":
                            c = 6;
                            break;
                    }

                }
                else if ((dt.type.Equals("EO")) && (dataTransactionList[i].DFApplicationFUR.FurRef.Equals(furList[auxCheckFur])))
                {
                    int d = dt.detSelected.Items.Count;
                    int f = dt.SelectedDF.Items.Count;

                    complexity = EOEQcalculation(d, f);

                    switch (complexity)
                    {
                        case "L":
                            c = 4;
                            break;

                        case "A":
                            c = 5;
                            break;

                        case "H":
                            c = 7;
                            break;
                    }
                }
            }
            return c;
        }

        private string oneDTCalculation(int d, int f, string t)
        {
            string complexity;
            int c = 0;
            if (t.Equals("EI"))
            {
                complexity = EIcalculation(d, f);

                switch (complexity)
                {
                    case "L":
                        c = 3;
                        break;

                    case "A":
                        c = 4;
                        break;

                    case "H":
                        c = 6;
                        break;
                }

            }
            else if (t.Equals("EQ"))
            {
                complexity = EOEQcalculation(d, f);

                switch (complexity)
                {
                    case "L":
                        c = 3;
                        break;

                    case "A":
                        c = 4;
                        break;

                    case "H":
                        c = 6;
                        break;
                }

            }
            else if (t.Equals("EO"))
            {
                complexity = EOEQcalculation(d, f);

                switch (complexity)
                {
                    case "L":
                        c = 4;
                        break;

                    case "A":
                        c = 5;
                        break;

                    case "H":
                        c = 7;
                        break;
                }
            }
            return c.ToString() + "FPs";
        }


        private string EIcalculation(int det, int ftr)
        {
            String complexity = "";
            if (((ftr == 0) || (ftr == 1)) && (det > 0))
            {
                if ((det < 5) && (det > 0))
                {
                    complexity = "L";
                }
                else if ((det > 4) && (det < 16))
                {
                    complexity = "L";
                }
                else
                {
                    complexity = "A";
                }
            }
            else if ((ftr == 2) && (det > 0))
            {
                if ((det < 5) && (det > 0))
                {
                    complexity = "L";
                }
                else if ((det > 4) && (det < 16))
                {
                    complexity = "A";
                }
                else
                {
                    complexity = "H";
                }
            }
            else if ((ftr > 2) && (det > 0))
            {
                if ((det < 5) && (det > 0))
                {
                    complexity = "A";
                }
                else if ((det > 4) && (det < 16))
                {
                    complexity = "H";
                }
                else
                {
                    complexity = "H";
                }
            }
            return complexity;
        }

        private string EOEQcalculation(int det, int ftr)
        {
            String complexity = "";
            if (((ftr == 0) || (ftr == 1)) && (det > 0))
            {
                if ((det < 6) && (det > 0))
                {
                    complexity = "L";
                }
                else if ((det > 5) && (det < 20))
                {
                    complexity = "L";
                }
                else
                {
                    complexity = "A";
                }
            }
            else if (((ftr == 2) || (ftr == 3)) && (det > 0))
            {
                if ((det < 6) && (det > 0))
                {
                    complexity = "L";
                }
                else if ((det > 5) && (det < 20))
                {
                    complexity = "A";
                }
                else
                {
                    complexity = "H";
                }
            }
            else if ((ftr > 3) && (det > 0))
            {
                if ((det < 6) && (det > 0))
                {
                    complexity = "A";
                }
                else if ((det > 5) && (det < 20))
                {
                    complexity = "H";
                }
                else
                {
                    complexity = "H";
                }
            }
            return complexity;
        }

        private async void exportButton_Click(object sender, RoutedEventArgs e)
        {
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            //Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add("Plain Text", new List<String>() { ".xml" });
            // default file name if the user does not type one in or select a file to replace
            savePicker.SuggestedFileName = "New Document";
            IStorageFile file = await savePicker.PickSaveFileAsync();

            this.projectName.Text = file.Name;

            if (file != null)
            {
                //Prevent updates to the remote version of the file until we finish making changes and call CompleteUpdateAsync.
                CachedFileManager.DeferUpdates(file);

                XmlDocument xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("furs");
                xmlDoc.AppendChild(rootNode);

                for (int i = 0; i < furList.Count; i++)
                {
                    XmlNode furIdNode = xmlDoc.CreateElement("id");
                    furIdNode.InnerText = furList[i].FurName;
                    XmlNode furNameNode = xmlDoc.CreateElement("name");
                    furNameNode.InnerText = furList[i].FurDescription;
                    XmlNode furDescriptionNode = xmlDoc.CreateElement("description");
                    furDescriptionNode.InnerText = furList[i].FurLongDescription;

                    rootNode.AppendChild(furIdNode);
                    rootNode.AppendChild(furNameNode);
                    rootNode.AppendChild(furDescriptionNode);
                    for (int j = 0; j < applicationList.Count; j++)
                    {
                        if (applicationList[j].FurRef.FurDescription.Equals(furList[i].FurDescription))
                        {

                            XmlNode appName = xmlDoc.CreateElement("applicationName");
                            appName.InnerText = applicationList[j].AppName;
                            XmlNode appFur = xmlDoc.CreateElement("applicationFur");
                            appFur.InnerText = applicationList[j].FurRef.FurDescription;
                            XmlNode appMain = xmlDoc.CreateElement("applicationMain");
                            appMain.InnerText = applicationList[j].AppBoolean.ToString();

                            rootNode.AppendChild(appName);
                            rootNode.AppendChild(appFur);
                            rootNode.AppendChild(appMain);

                        }
                    }
                }

                await FileIO.WriteTextAsync(file, xmlDoc.InnerXml, Windows.Storage.Streams.UnicodeEncoding.Utf8);

                //Let Windows know that weéw finished changing the file so the other app can update the remote version of the file. 
                // Completing updates may require Windows to ask for user input.
                FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                if (status == FileUpdateStatus.Complete)
                {
                    Console.WriteLine("File " + file.Name + " was saved.");

                }
                else if (status == FileUpdateStatus.CompleteAndRenamed)
                {
                    Console.WriteLine("File " + file.Name + " was rename and saved.");
                }
                else
                {
                    Console.WriteLine("File " + file.Name + "couldn´t be saved.");
                }

            }
            else
            {
                Console.WriteLine("Operation cancelled.");
            }
        }


        private async void importButton_Click(object sender, RoutedEventArgs e)
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".xml");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            //String datas = await FileIO.ReadTextAsync(file, Windows.Storage.Streams.UnicodeEncoding.Utf8);

            string datas = await FileIO.ReadTextAsync(file, Windows.Storage.Streams.UnicodeEncoding.Utf8);
            if (file != null)
            {
                // Application now has read/write access to the picked file
                this.projectName.Text = file.Name;


                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(datas);

                XmlNodeList xmlnode = xmlDoc.SelectNodes("furs");
                int c = 0;

                foreach (XmlNode xn in xmlnode)
                {

                    string id = null;
                    string name = null;
                    string description = null;

                    for (int a = 0; a < xmlnode.Count; a++)
                    {
                        id = xn["id"].ChildNodes.Item(a).InnerText;
                        name = xn["name"].ChildNodes.Item(a).InnerText;
                        description = xn["description"].ChildNodes.Item(a).InnerText;
                    }

                    string appName = xn["applicationName"].InnerText;
                    string appFurDescription = xn["applicationFur"].InnerText;
                    string appMainCheck = xn["applicationMain"].InnerText;

                    for (int x = 0; x < furList.Count; x++)
                    {
                        if (furList[x].Equals(appFurDescription))
                        {
                            applicationCounter = x;
                        }
                    }

                    Fur aux = new Fur(id, name, description);
                    furList.Add(aux);
                    comboFUR.Items.Add(name);
                    LongDescriptionTextBox.Text = description;

                    double w = mainGrid.ActualWidth;
                    double auxW = (w) / 2.4;

                    double h = mainGrid.ActualHeight;
                    double auxH = (h) / 2.4;

                    GridView gridView = new GridView();
                    gridView.CornerRadius = new CornerRadius(5);
                    gridView.Margin = new Thickness(35);
                    gridView.Header = appName;
                    gridView.VerticalAlignment = VerticalAlignment.Stretch;
                    gridView.HorizontalAlignment = HorizontalAlignment.Stretch;
                    gridView.CanReorderItems = true;
                    gridView.AllowDrop = true;
                    gridView.BorderThickness = new Thickness(2);
                    gridView.Width = auxW;
                    gridView.Height = auxH;

                    Boolean mainBool;
                    if (appMainCheck == "true")
                    {
                        mainBool = true;
                        gridView.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Red);
                    }
                    else
                    {
                        mainBool = false;
                        gridView.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Green);
                    }

                    for (int x = 0; x < furList.Count; x++)
                    {
                        if (furList[x].Equals(aux))
                        {
                            applicationCounter = x;
                        }
                    }

                    applicationFur appFur = new applicationFur(appName, mainBool, furList[applicationCounter], gridView);
                    applicationList.Add(appFur);
                    appGridViewListItems.Add(gridView);
                    furCounter++;
                    c++;

                }
            }
            else
            {
                this.projectName.Text = "Operation cancelled.";
            }
        }

        private void NewDataFunction_Click(object sender, RoutedEventArgs e)
        {
            EditButton.IsEnabled = false;
            DeleteButton.IsEnabled = false;
            okButton.IsEnabled = true;

            comboBoxRets.SelectedIndex = 0;
            TypeDF.SelectedIndex = -1;
            TextDF.Text = "";
            DetOfDF.Text = "";

            FpsDF.Text = "0 FPs";
            updateDetTextBox.Text = "0-Dets";
            updateRET.Text = " 1 RET ";

        }

        private void flyTransactionFunction_Opened(object sender, object e)
        {
            addDFtoFTR.Items.Clear();
            int co = 0;
            //Check the fur that we are on.   
            for (int x = 0; x < furList.Count; x++)
            {
                if (furList[x].FurDescription.Equals(comboFUR.SelectedItem.ToString()))//(comboFUR.Description))
                {
                    co = x;
                }
            }

            for (int y = 0; y < dataFunctionList.Count; y++)
            {
                if (dataFunctionList[y].DFApplicationFUR.FurRef.Equals(furList[co]))
                {
                    addDFtoFTR.Items.Add(dataFunctionList[y].DFName);
                }
            }

        }

        private void addDFtoFTR_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            detList.Items.Clear();
            String[] l;
            //ListBox auxList = new ListBox();
            if (addDFtoFTR.SelectedItems.Count > 0)
            {
                foreach (var item in addDFtoFTR.SelectedItems)
                {
                    for (int i = 0; i < dataFunctionList.Count; i++)
                    {
                        if (dataFunctionList[i].DFName.Equals(item))
                        {
                            l = dataFunctionList[i].DFDet.Split(", ");
                            foreach (string it in l)
                            {
                                detList.Items.Add(it);

                            }
                            detList.SelectAll();
                        }
                    }

                }
                int c = detList.SelectedItems.Count;
                int f = addDFtoFTR.SelectedItems.Count;
                string t;
                if (transactionType.Equals("EO"))
                {
                    t = "EO";
                }
                else if (transactionType.Equals("EQ"))
                {
                    t = "EQ";
                }
                else
                {
                    t = "EI";
                }

                sizeFPTransaction.Text = oneDTCalculation(c, f, t);
            }
            else
            {
                sizeFPTransaction.Text = "0 FPs";
            }
        }

        private void detList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ListBox auxList = new ListBox();
            int c = detList.SelectedItems.Count;
            int f = addDFtoFTR.SelectedItems.Count;
            string t;
            if (transactionType.Equals("EO"))
            {
                t = "EO";
            }
            else if (transactionType.Equals("EQ"))
            {
                t = "EQ";
            }
            else
            {
                t = "EI";
            }

            sizeFPTransaction.Text = oneDTCalculation(c, f, t);
        }

        private void GridView_LostFocus(object sender, RoutedEventArgs e)
        {

            if ((newEO.IsPressed) || (newEI.IsPressed) || (newEQ.IsPressed))
            {

            }
            else
            {
                var gV = sender as GridView;
                gV.SelectedItem = null;
            }
        }

        private void DetOfDF_SelectionChanged(object sender, RoutedEventArgs e)
        {
            int aux = DetOfDF.Text.Split(',').Length;
            updateDetTextBox.Text = aux + "-Dets";

            //CALCULATE TYPE
            string t;
            if (TypeDF.SelectedIndex == 0)
            {
                t = "ILF";
            }
            else
            {
                t = "EIF";
            }

            //CALCULATE DET
            int d = DetOfDF.Text.Split(',').Length;

            //CALCULATE RET
            int r = RetCal(comboBoxRets.SelectedIndex);

            string final = oneDFCalculationDetRet(d, r, t);

            FpsDF.Text = final;
        }

        private void comboBoxRets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RetExchange(comboBoxRets.SelectedIndex);

            //CALCULATE TYPE
            string t;
            if (TypeDF.SelectedIndex == 0)
            {
                t = "ILF";
            }
            else
            {
                t = "EIF";
            }

            //CALCULATE DET
            int d = DetOfDF.Text.Split(',').Length;

            //CALCULATE RET
            int r = RetCal(comboBoxRets.SelectedIndex);

            string final = oneDFCalculationDetRet(d, r, t);

            FpsDF.Text = final;
        }

        private void RetExchange(int i)
        {
            if (i == 1)
            {
                updateRET.Text = "2-RETs";
            }
            else if (i == 2)
            {
                updateRET.Text = "3-RETs";
            }
            else if (i == 3)
            {
                updateRET.Text = "4-RETs";
            }
            else if (i == 4)
            {
                updateRET.Text = "5-RETs";
            }
            else if (i == 5)
            {
                updateRET.Text = "> 5-RETs";
            }
            else
            {
                updateRET.Text = "1-RET";
            }
        }

        private int RetCal(int i)
        {
            if (i == 1)
            {
                return 2;
            }
            else if (i == 2)
            {
                return 3;
            }
            else if (i == 3)
            {
                return 4;
            }
            else if (i == 4)
            {
                return 5;
            }
            else if (i == 5)
            {
                return 6;
            }
            else
            {
                return 1;
            }
        }

        private void DeleteTransactionButton_Click(object sender, RoutedEventArgs e)
        {
            Button b = new Button();
            b = (Button)sender;
            deleteDataTransaction.ShowAt(b);
        }

        private void EditTransactionButton_Click(object sender, RoutedEventArgs e)
        {
            Button b = new Button();
            b = (Button)sender;
            loadTransaction.Visibility = Visibility.Visible;
            saveTransaction.Visibility = Visibility.Visible;

            if (listDT.SelectedItem != null)
            {
                flyTransactionFunction.ShowAt(b);
                for (int i = 0; i < DTGridList.Count; i++)
                {
                    if (listDT.SelectedItem.Equals(DTGridList[i].grid))
                    {

                        nameTransaction.Text = DTGridList[i].DT.DTName;
                        applicationName.Text = DTGridList[i].DT.DFApplicationFUR.AppName;

                        if (DTGridList[i].DT.type.Equals("EI"))
                        {
                            transactionType.SelectedIndex = 0;
                        }
                        else if (DTGridList[i].DT.type.Equals("EQ"))
                        {
                            transactionType.SelectedIndex = 1;
                        }
                        else
                        {
                            transactionType.SelectedIndex = 2;
                        }
                    }
                }
            }
        }

        private void loadTransaction_Click(object sender, RoutedEventArgs e)
        {
            addDFtoFTR.SelectAll();
            loadTransaction.Visibility = Visibility.Collapsed;
        }

        private void saveTransaction_Click(object sender, RoutedEventArgs e)
        {

            okTransaction.Visibility = Visibility.Collapsed;

            int aux = listDT.SelectedIndex;
            if (aux >= 0)
            {
                for (int i = 0; i < DTGridList.Count; i++)
                {
                    if (dataTransactionList[i] != null)
                    {



                        if (DTGridList[i].grid.Equals(listDT.SelectedItem))
                        {
                            for (int l = 0; l < dataTransactionList.Count; l++)
                            {
                                if (dataTransactionList[l].Equals(DTGridList[i].DT))
                                {
                                    dataTransactionList.RemoveAt(l);

                                }
                            }

                            string type;
                            if (transactionType.SelectedIndex == 0)
                            {
                                type = "EI";

                            }
                            else if (transactionType.SelectedIndex == 1)
                            {
                                type = "EQ";
                            }
                            else
                            {
                                type = "EO";
                            }

                            dataTransaction aux1 = new dataTransaction(nameTransaction.Text, type, DTGridList[i].DT.DFApplicationFUR, DTGridList[i].DT.DataFunction, DTGridList[i].DT.SelectedDF, DTGridList[i].DT.SelectedDETs);

                            dataTransactionList.Add(aux1);

                            for (int j = 0; j < dataFunctionList.Count; j++)
                            {
                                if ((DTGridList[i].dT.dFunction.Equals(dataFunctionList[j])) && (listDT.Items.Count <= 1))
                                {
                                    if (DTGridList[i].DT.type.Equals("EI"))
                                    {
                                        dataFunctionList[j].EIButton.Visibility = Visibility.Collapsed;
                                    }
                                    else if (DTGridList[i].DT.type.Equals("EQ"))
                                    {
                                        dataFunctionList[j].EQButton.Visibility = Visibility.Collapsed;
                                    }
                                    else if (DTGridList[i].DT.type.Equals("EO"))
                                    {
                                        dataFunctionList[j].EOButton.Visibility = Visibility.Collapsed;
                                    }
                                }
                            }

                            listDT.Items.RemoveAt(aux);
                            DTGridList.RemoveAt(i);


                            Grid myGrid = new Grid();
                            myGrid.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Black);
                            myGrid.BorderThickness = new Thickness(1);
                            // Define the Columns
                            ColumnDefinition colDef1 = new ColumnDefinition();
                            ColumnDefinition colDef2 = new ColumnDefinition();
                            ColumnDefinition colDef3 = new ColumnDefinition();
                            ColumnDefinition colDef4 = new ColumnDefinition();
                            colDef1.Width = new GridLength(205);
                            colDef2.Width = new GridLength(110);
                            colDef3.Width = new GridLength(100);
                            colDef4.Width = new GridLength(150);

                            myGrid.ColumnDefinitions.Add(colDef1);
                            myGrid.ColumnDefinitions.Add(colDef2);
                            myGrid.ColumnDefinitions.Add(colDef3);
                            myGrid.ColumnDefinitions.Add(colDef4);
                            TextBlock t1 = new TextBlock();
                            t1.HorizontalAlignment = HorizontalAlignment.Center;
                            t1.TextWrapping = TextWrapping.Wrap;

                            TextBlock t2 = new TextBlock();
                            t2.HorizontalAlignment = HorizontalAlignment.Center;

                            TextBlock t3 = new TextBlock();
                            t3.HorizontalAlignment = HorizontalAlignment.Center;
                            t3.MaxWidth = 102;
                            t3.TextWrapping = TextWrapping.Wrap;

                            TextBlock t4 = new TextBlock();
                            t4.HorizontalAlignment = HorizontalAlignment.Center;
                            t4.MaxWidth = 102;
                            t4.TextWrapping = TextWrapping.Wrap;

                            Grid.SetColumn(t1, 0);
                            myGrid.Children.Add(t1);

                            Grid.SetColumn(t2, 1);
                            myGrid.Children.Add(t2);

                            Grid.SetColumn(t3, 2);
                            myGrid.Children.Add(t3);

                            Grid.SetColumn(t4, 3);
                            myGrid.Children.Add(t4);

                            int b = -1;
                            for (int a = 0; a < dataTransactionList.Count; a++)
                            {
                                if (dataTransactionList[a].Equals(aux1))
                                {
                                    b = a;
                                }

                            }

                            t1.Text = nameTransaction.Text;//dataTransactionList[i].DTName;
                            t2.Text = dataTransactionList[i].DTType;
                            //t3.Text = oneDFCalculation(dataTransactionList[i].dFunction);
                            t3.Text = oneDTCalculation(dataTransactionList[i]);
                            t4.Text = dataTransactionList[i].DFApplicationFUR.AppName;
                            listDT.SelectionMode = SelectionMode.Single;
                            listDT.Items.Add(myGrid);
                            DTandGRID aux2 = new DTandGRID(aux1, myGrid);
                            DTGridList.Add(aux2);



                            for (int j = 0; j < dataFunctionList.Count; j++)
                            {
                                if (aux2.dT.dFunction.Equals(dataFunctionList[j]))
                                {
                                    if (DTGridList[i].DT.type.Equals("EI"))
                                    {
                                        dataFunctionList[j].EIButton.Visibility = Visibility.Visible;
                                    }
                                    else if (DTGridList[i].DT.type.Equals("EQ"))
                                    {
                                        dataFunctionList[j].EQButton.Visibility = Visibility.Visible;
                                    }
                                    else if (DTGridList[i].DT.type.Equals("EO"))
                                    {
                                        dataFunctionList[j].EOButton.Visibility = Visibility.Visible;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            deleteDataTransaction.Hide();
            int dfCalc = DFCalculation();
            int dtCalc = 0;
            for (int i = 0; i < dataTransactionList.Count; i++)
            {
                dtCalc += oneDTCalculationNumber(dataTransactionList[i]);
            }
            int total = dfCalc + dtCalc;

            TextFPs.Text = total + "FPs";


            globalCalculationFur();

        }

        private void NoDT_Click(object sender, RoutedEventArgs e)
        {
            deleteDataTransaction.Hide();

        }

        private void YesDT_Click(object sender, RoutedEventArgs e)
        {
            int aux = listDT.SelectedIndex;
            if (aux >= 0)
            {
                for (int i = 0; i < DTGridList.Count; i++)
                {
                    if (DTGridList[i].grid.Equals(listDT.SelectedItem))
                    {
                        for (int l = 0; l < dataTransactionList.Count; l++)
                        {
                            if (dataTransactionList[l].Equals(DTGridList[i].DT))
                            {
                                dataTransactionList.RemoveAt(l);
                            }
                        }

                        for (int j = 0; j < dataFunctionList.Count; j++)
                        {
                            if ((DTGridList[i].dT.dFunction.Equals(dataFunctionList[j])) && (listDT.Items.Count <= 1))
                            {
                                if (DTGridList[i].DT.type.Equals("EI"))
                                {
                                    dataFunctionList[j].EIButton.Visibility = Visibility.Collapsed;
                                }
                                else if (DTGridList[i].DT.type.Equals("EQ"))
                                {
                                    dataFunctionList[j].EQButton.Visibility = Visibility.Collapsed;
                                }
                                else if (DTGridList[i].DT.type.Equals("EO"))
                                {
                                    dataFunctionList[j].EOButton.Visibility = Visibility.Collapsed;
                                }
                            }
                        }

                        listDT.Items.RemoveAt(aux);
                        DTGridList.RemoveAt(i);
                    }
                }
            }
            deleteDataTransaction.Hide();
            int dfCalc = DFCalculation();
            int dtCalc = 0;
            for (int i = 0; i < dataTransactionList.Count; i++)
            {
                dtCalc += oneDTCalculationNumber(dataTransactionList[i]);
            }
            int total = dfCalc + dtCalc;

            TextFPs.Text = total + "FPs";
        }

        private void globalCalculationFur()
        {
            int dfCalc = DFCalculation();
            int dtCalc = 0;

            //Check the fur that we are on.   
            for (int x = 0; x < furList.Count; x++)
            {
                if (furList[x].FurDescription.Equals(comboFUR.Description))
                {
                    applicationCounter = x;
                }
            }

            for (int i = 0; i < dataTransactionList.Count; i++)
            {
                if (dataTransactionList[i].DFApplicationFUR.FurRef.Equals(furList[applicationCounter]))
                {
                    dtCalc += oneDTCalculationNumber(dataTransactionList[i]);
                }
            }
            int total = dfCalc + dtCalc;

            TextFPs.Text = total + "FPs";
        }


    }
}