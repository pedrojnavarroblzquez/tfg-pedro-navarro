extern alias System_Runtime_Extensions;
namespace System.Runtime.CompilerServices {
    internal class __BlockReflectionAttribute : Attribute { }
}

namespace Microsoft.Xml.Serialization.GeneratedAssembly {


    [System.Runtime.CompilerServices.__BlockReflection]
    public class XmlSerializationWriter1 : System.Xml.Serialization.XmlSerializationWriter {

        public void Write44_ClusterManifest(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"http://schemas.microsoft.com/2011/01/fabric";
            WriteStartDocument();
            if (o == null) {
                WriteEmptyTag(@"ClusterManifest", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace1 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write39_ClusterManifestType(@"ClusterManifest", namespace1, ((global::System.Fabric.Management.ServiceModel.ClusterManifestType)o), false, false, namespace1, @"http://schemas.microsoft.com/2011/01/fabric");
        }

        public void Write45_InfrastructureInformation(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"http://schemas.microsoft.com/2011/01/fabric";
            WriteStartDocument();
            if (o == null) {
                WriteEmptyTag(@"InfrastructureInformation", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace2 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write41_InfrastructureInformationType(@"InfrastructureInformation", namespace2, ((global::System.Fabric.Management.ServiceModel.InfrastructureInformationType)o), false, false, namespace2, @"http://schemas.microsoft.com/2011/01/fabric");
        }

        public void Write46_TargetInformation(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"http://schemas.microsoft.com/2011/01/fabric";
            WriteStartDocument();
            if (o == null) {
                WriteEmptyTag(@"TargetInformation", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace3 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write43_TargetInformationType(@"TargetInformation", namespace3, ((global::System.Fabric.Management.ServiceModel.TargetInformationType)o), false, false, namespace3, @"http://schemas.microsoft.com/2011/01/fabric");
        }

        void Write43_TargetInformationType(string n, string ns, global::System.Fabric.Management.ServiceModel.TargetInformationType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.TargetInformationType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"TargetInformationType", defaultNamespace);
            string namespace4 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write42_Item(@"CurrentInstallation", namespace4, ((global::System.Fabric.Management.ServiceModel.WindowsFabricDeploymentInformation)o.@CurrentInstallation), false, false, namespace4, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace5 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write42_Item(@"TargetInstallation", namespace5, ((global::System.Fabric.Management.ServiceModel.WindowsFabricDeploymentInformation)o.@TargetInstallation), false, false, namespace5, @"http://schemas.microsoft.com/2011/01/fabric");
            WriteEndElement(o);
        }

        void Write42_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.WindowsFabricDeploymentInformation o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.WindowsFabricDeploymentInformation)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"WindowsFabricDeploymentInformation", defaultNamespace);
            WriteAttribute(@"InstanceId", @"", ((global::System.String)o.@InstanceId));
            WriteAttribute(@"MSILocation", @"", ((global::System.String)o.@MSILocation));
            WriteAttribute(@"ClusterManifestLocation", @"", ((global::System.String)o.@ClusterManifestLocation));
            WriteAttribute(@"InfrastructureManifestLocation", @"", ((global::System.String)o.@InfrastructureManifestLocation));
            WriteAttribute(@"TargetVersion", @"", ((global::System.String)o.@TargetVersion));
            WriteAttribute(@"NodeName", @"", ((global::System.String)o.@NodeName));
            if (((global::System.Boolean)o.@RemoveNodeState) != false) {
                WriteAttribute(@"RemoveNodeState", @"", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)o.@RemoveNodeState)));
            }
            WriteAttribute(@"UpgradeEntryPointExe", @"", ((global::System.String)o.@UpgradeEntryPointExe));
            WriteAttribute(@"UpgradeEntryPointExeParameters", @"", ((global::System.String)o.@UpgradeEntryPointExeParameters));
            WriteAttribute(@"UndoUpgradeEntryPointExe", @"", ((global::System.String)o.@UndoUpgradeEntryPointExe));
            WriteAttribute(@"UndoUpgradeEntryPointExeParameters", @"", ((global::System.String)o.@UndoUpgradeEntryPointExeParameters));
            WriteEndElement(o);
        }

        void Write41_InfrastructureInformationType(string n, string ns, global::System.Fabric.Management.ServiceModel.InfrastructureInformationType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.InfrastructureInformationType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"InfrastructureInformationType", defaultNamespace);
            string namespace6 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.InfrastructureNodeType[] a = (global::System.Fabric.Management.ServiceModel.InfrastructureNodeType[])((global::System.Fabric.Management.ServiceModel.InfrastructureNodeType[])o.@NodeList);
                if (a != null){
                    WriteStartElement(@"NodeList", namespace6, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace7 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write40_InfrastructureNodeType(@"Node", namespace7, ((global::System.Fabric.Management.ServiceModel.InfrastructureNodeType)a[ia]), false, false, namespace7, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write40_InfrastructureNodeType(string n, string ns, global::System.Fabric.Management.ServiceModel.InfrastructureNodeType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.InfrastructureNodeType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"InfrastructureNodeType", defaultNamespace);
            WriteAttribute(@"NodeName", @"", ((global::System.String)o.@NodeName));
            WriteAttribute(@"IPAddressOrFQDN", @"", ((global::System.String)o.@IPAddressOrFQDN));
            WriteAttribute(@"RoleOrTierName", @"", ((global::System.String)o.@RoleOrTierName));
            WriteAttribute(@"NodeTypeRef", @"", ((global::System.String)o.@NodeTypeRef));
            if (((global::System.Boolean)o.@IsSeedNode) != false) {
                WriteAttribute(@"IsSeedNode", @"", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)o.@IsSeedNode)));
            }
            WriteAttribute(@"FaultDomain", @"", ((global::System.String)o.@FaultDomain));
            WriteAttribute(@"UpgradeDomain", @"", ((global::System.String)o.@UpgradeDomain));
            WriteAttribute(@"InfrastructurePlacementID", @"", ((global::System.String)o.@InfrastructurePlacementID));
            string namespace8 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write8_FabricEndpointsType(@"Endpoints", namespace8, ((global::System.Fabric.Management.ServiceModel.FabricEndpointsType)o.@Endpoints), false, false, namespace8, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace9 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write17_CertificatesType(@"Certificates", namespace9, ((global::System.Fabric.Management.ServiceModel.CertificatesType)o.@Certificates), false, false, namespace9, @"http://schemas.microsoft.com/2011/01/fabric");
            WriteEndElement(o);
        }

        void Write17_CertificatesType(string n, string ns, global::System.Fabric.Management.ServiceModel.CertificatesType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.CertificatesType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"CertificatesType", defaultNamespace);
            string namespace10 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write16_FabricCertificateType(@"ClusterCertificate", namespace10, ((global::System.Fabric.Management.ServiceModel.FabricCertificateType)o.@ClusterCertificate), false, false, namespace10, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace11 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write16_FabricCertificateType(@"ServerCertificate", namespace11, ((global::System.Fabric.Management.ServiceModel.FabricCertificateType)o.@ServerCertificate), false, false, namespace11, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace12 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write16_FabricCertificateType(@"ClientCertificate", namespace12, ((global::System.Fabric.Management.ServiceModel.FabricCertificateType)o.@ClientCertificate), false, false, namespace12, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace13 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write16_FabricCertificateType(@"UserRoleClientCertificate", namespace13, ((global::System.Fabric.Management.ServiceModel.FabricCertificateType)o.@UserRoleClientCertificate), false, false, namespace13, @"http://schemas.microsoft.com/2011/01/fabric");
            WriteEndElement(o);
        }

        void Write16_FabricCertificateType(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricCertificateType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricCertificateType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"FabricCertificateType", defaultNamespace);
            if (((global::System.String)o.@X509StoreName) != @"My") {
                WriteAttribute(@"X509StoreName", @"", ((global::System.String)o.@X509StoreName));
            }
            if (((global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType)o.@X509FindType) != global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType.@FindByThumbprint) {
                WriteAttribute(@"X509FindType", @"", Write15_Item(((global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType)o.@X509FindType)));
            }
            WriteAttribute(@"X509FindValue", @"", ((global::System.String)o.@X509FindValue));
            if ((((global::System.String)o.@X509FindValueSecondary) != null) && (((global::System.String)o.@X509FindValueSecondary).Length != 0)) {
                WriteAttribute(@"X509FindValueSecondary", @"", ((global::System.String)o.@X509FindValueSecondary));
            }
            WriteAttribute(@"Name", @"", ((global::System.String)o.@Name));
            WriteEndElement(o);
        }

        string Write15_Item(global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType v) {
            string parentRuntimeNs = null;
            string parentCompileTimeNs = null;
            string s = null;
            switch (v) {
                case global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType.@FindByThumbprint: s = @"FindByThumbprint"; break;
                case global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType.@FindBySubjectName: s = @"FindBySubjectName"; break;
                case global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType.@FindByExtension: s = @"FindByExtension"; break;
                default: throw CreateInvalidEnumValueException(((System.Int64)v).ToString(System.Globalization.CultureInfo.InvariantCulture), @"System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType");
            }
            return s;
        }

        void Write8_FabricEndpointsType(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricEndpointsType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricEndpointsType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"FabricEndpointsType", defaultNamespace);
            string namespace14 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write3_InputEndpointType(@"ClientConnectionEndpoint", namespace14, ((global::System.Fabric.Management.ServiceModel.InputEndpointType)o.@ClientConnectionEndpoint), false, false, namespace14, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace15 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"LeaseDriverEndpoint", namespace15, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@LeaseDriverEndpoint), false, false, namespace15, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace16 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"NotificationEndpoint", namespace16, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@NotificationEndpoint), false, false, namespace16, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace17 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"ClusterConnectionEndpoint", namespace17, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@ClusterConnectionEndpoint), false, false, namespace17, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace18 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write3_InputEndpointType(@"HttpGatewayEndpoint", namespace18, ((global::System.Fabric.Management.ServiceModel.InputEndpointType)o.@HttpGatewayEndpoint), false, false, namespace18, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace19 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write3_InputEndpointType(@"HttpApplicationGatewayEndpoint", namespace19, ((global::System.Fabric.Management.ServiceModel.InputEndpointType)o.@HttpApplicationGatewayEndpoint), false, false, namespace19, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace20 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"ServiceConnectionEndpoint", namespace20, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@ServiceConnectionEndpoint), false, false, namespace20, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace21 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"ClusterManagerReplicatorEndpoint", namespace21, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@ClusterManagerReplicatorEndpoint), false, false, namespace21, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace22 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"RepairManagerReplicatorEndpoint", namespace22, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@RepairManagerReplicatorEndpoint), false, false, namespace22, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace23 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"NamingReplicatorEndpoint", namespace23, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@NamingReplicatorEndpoint), false, false, namespace23, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace24 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"FailoverManagerReplicatorEndpoint", namespace24, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@FailoverManagerReplicatorEndpoint), false, false, namespace24, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace25 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"ImageStoreServiceReplicatorEndpoint", namespace25, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@ImageStoreServiceReplicatorEndpoint), false, false, namespace25, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace26 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"UpgradeServiceReplicatorEndpoint", namespace26, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@UpgradeServiceReplicatorEndpoint), false, false, namespace26, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace27 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"FaultAnalysisServiceReplicatorEndpoint", namespace27, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@FaultAnalysisServiceReplicatorEndpoint), false, false, namespace27, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace28 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"BackupRestoreServiceReplicatorEndpoint", namespace28, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@BackupRestoreServiceReplicatorEndpoint), false, false, namespace28, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace29 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"UpgradeOrchestrationServiceReplicatorEndpoint", namespace29, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@UpgradeOrchestrationServiceReplicatorEndpoint), false, false, namespace29, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace30 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"CentralSecretServiceReplicatorEndpoint", namespace30, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@CentralSecretServiceReplicatorEndpoint), false, false, namespace30, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace31 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"EventStoreServiceReplicatorEndpoint", namespace31, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@EventStoreServiceReplicatorEndpoint), false, false, namespace31, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace32 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"GatewayResourceManagerReplicatorEndpoint", namespace32, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@GatewayResourceManagerReplicatorEndpoint), false, false, namespace32, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace33 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write5_InternalEndpointType(@"DefaultReplicatorEndpoint", namespace33, ((global::System.Fabric.Management.ServiceModel.InternalEndpointType)o.@DefaultReplicatorEndpoint), false, false, namespace33, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace34 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write3_InputEndpointType(@"TokenServiceEndpoint", namespace34, ((global::System.Fabric.Management.ServiceModel.InputEndpointType)o.@TokenServiceEndpoint), false, false, namespace34, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace35 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write6_Item(@"ApplicationEndpoints", namespace35, ((global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeApplicationEndpoints)o.@ApplicationEndpoints), false, false, namespace35, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace36 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write7_Item(@"EphemeralEndpoints", namespace36, ((global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeEphemeralEndpoints)o.@EphemeralEndpoints), false, false, namespace36, @"http://schemas.microsoft.com/2011/01/fabric");
            WriteEndElement(o);
        }

        void Write7_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeEphemeralEndpoints o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeEphemeralEndpoints)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            WriteAttribute(@"StartPort", @"", System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o.@StartPort)));
            WriteAttribute(@"EndPort", @"", System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o.@EndPort)));
            WriteEndElement(o);
        }

        void Write6_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeApplicationEndpoints o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeApplicationEndpoints)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            WriteAttribute(@"StartPort", @"", System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o.@StartPort)));
            WriteAttribute(@"EndPort", @"", System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o.@EndPort)));
            WriteEndElement(o);
        }

        void Write3_InputEndpointType(string n, string ns, global::System.Fabric.Management.ServiceModel.InputEndpointType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.InputEndpointType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"InputEndpointType", defaultNamespace);
            WriteAttribute(@"Port", @"", ((global::System.String)o.@Port));
            if (((global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol)o.@Protocol) != global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol.@tcp) {
                WriteAttribute(@"Protocol", @"", Write1_InputEndpointTypeProtocol(((global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol)o.@Protocol)));
            }
            WriteEndElement(o);
        }

        string Write1_InputEndpointTypeProtocol(global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol v) {
            string parentRuntimeNs = null;
            string parentCompileTimeNs = null;
            string s = null;
            switch (v) {
                case global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol.@http: s = @"http"; break;
                case global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol.@https: s = @"https"; break;
                case global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol.@tcp: s = @"tcp"; break;
                default: throw CreateInvalidEnumValueException(((System.Int64)v).ToString(System.Globalization.CultureInfo.InvariantCulture), @"System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol");
            }
            return s;
        }

        void Write5_InternalEndpointType(string n, string ns, global::System.Fabric.Management.ServiceModel.InternalEndpointType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.InternalEndpointType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"InternalEndpointType", defaultNamespace);
            WriteAttribute(@"Port", @"", ((global::System.String)o.@Port));
            if (((global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol)o.@Protocol) != global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol.@tcp) {
                WriteAttribute(@"Protocol", @"", Write4_InternalEndpointTypeProtocol(((global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol)o.@Protocol)));
            }
            WriteEndElement(o);
        }

        string Write4_InternalEndpointTypeProtocol(global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol v) {
            string parentRuntimeNs = null;
            string parentCompileTimeNs = null;
            string s = null;
            switch (v) {
                case global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol.@http: s = @"http"; break;
                case global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol.@https: s = @"https"; break;
                case global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol.@tcp: s = @"tcp"; break;
                default: throw CreateInvalidEnumValueException(((System.Int64)v).ToString(System.Globalization.CultureInfo.InvariantCulture), @"System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol");
            }
            return s;
        }

        void Write39_ClusterManifestType(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"ClusterManifestType", defaultNamespace);
            WriteAttribute(@"Name", @"", ((global::System.String)o.@Name));
            WriteAttribute(@"Version", @"", ((global::System.String)o.@Version));
            WriteAttribute(@"Description", @"", ((global::System.String)o.@Description));
            string namespace37 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType[] a = (global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType[])((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType[])o.@NodeTypes);
                if (a != null){
                    WriteStartElement(@"NodeTypes", namespace37, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace38 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write19_ClusterManifestTypeNodeType(@"NodeType", namespace38, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType)a[ia]), false, false, namespace38, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            string namespace39 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write35_Item(@"Infrastructure", namespace39, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructure)o.@Infrastructure), false, false, namespace39, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace40 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection[] a = (global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection[])((global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection[])o.@FabricSettings);
                if (a != null){
                    WriteStartElement(@"FabricSettings", namespace40, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace41 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write37_SettingsOverridesTypeSection(@"Section", namespace41, ((global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection)a[ia]), false, false, namespace41, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            string namespace42 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write38_Item(@"Certificates", namespace42, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeCertificates)o.@Certificates), false, false, namespace42, @"http://schemas.microsoft.com/2011/01/fabric");
            WriteEndElement(o);
        }

        void Write38_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeCertificates o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeCertificates)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            string namespace43 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write16_FabricCertificateType(@"SecretsCertificate", namespace43, ((global::System.Fabric.Management.ServiceModel.FabricCertificateType)o.@SecretsCertificate), false, false, namespace43, @"http://schemas.microsoft.com/2011/01/fabric");
            WriteEndElement(o);
        }

        void Write37_SettingsOverridesTypeSection(string n, string ns, global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            WriteAttribute(@"Name", @"", ((global::System.String)o.@Name));
            {
                global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter[] a = (global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter[])o.@Parameter;
                if (a != null) {
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace44 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write36_Item(@"Parameter", namespace44, ((global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter)a[ia]), false, false, namespace44, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                }
            }
            WriteEndElement(o);
        }

        void Write36_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            WriteAttribute(@"Name", @"", ((global::System.String)o.@Name));
            WriteAttribute(@"Value", @"", ((global::System.String)o.@Value));
            if (((global::System.Boolean)o.@IsEncrypted) != false) {
                WriteAttribute(@"IsEncrypted", @"", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)o.@IsEncrypted)));
            }
            WriteAttribute(@"Type", @"", ((global::System.String)o.@Type));
            WriteEndElement(o);
        }

        void Write35_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructure o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructure)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            if ((object)(o.@Item) != null){
                if (o.@Item is global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureLinux) {
                    string namespace45 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                    Write26_Item(@"Linux", namespace45, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureLinux)o.@Item), false, false, namespace45, @"http://schemas.microsoft.com/2011/01/fabric");
                }
                else if (o.@Item is global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzureStaticTopology) {
                    string namespace46 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                    Write33_Item(@"WindowsAzureStaticTopology", namespace46, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzureStaticTopology)o.@Item), false, false, namespace46, @"http://schemas.microsoft.com/2011/01/fabric");
                }
                else if (o.@Item is global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsServer) {
                    string namespace47 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                    Write34_Item(@"WindowsServer", namespace47, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsServer)o.@Item), false, false, namespace47, @"http://schemas.microsoft.com/2011/01/fabric");
                }
                else if (o.@Item is global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureBlackbird) {
                    string namespace48 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                    Write21_Item(@"Blackbird", namespace48, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureBlackbird)o.@Item), false, false, namespace48, @"http://schemas.microsoft.com/2011/01/fabric");
                }
                else if (o.@Item is global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzure) {
                    string namespace49 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                    Write31_Item(@"WindowsAzure", namespace49, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzure)o.@Item), false, false, namespace49, @"http://schemas.microsoft.com/2011/01/fabric");
                }
                else if (o.@Item is global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureFlexibleDynamicTopology) {
                    string namespace50 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                    Write23_Item(@"FlexibleDynamicTopology", namespace50, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureFlexibleDynamicTopology)o.@Item), false, false, namespace50, @"http://schemas.microsoft.com/2011/01/fabric");
                }
                else if (o.@Item is global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructurePaaS) {
                    string namespace51 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                    Write29_Item(@"PaaS", namespace51, ((global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructurePaaS)o.@Item), false, false, namespace51, @"http://schemas.microsoft.com/2011/01/fabric");
                }
                else  if ((object)(o.@Item) != null){
                    throw CreateUnknownTypeException(o.@Item);
                }
            }
            WriteEndElement(o);
        }

        void Write29_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructurePaaS o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructurePaaS)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            string namespace52 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.PaaSRoleType[] a = (global::System.Fabric.Management.ServiceModel.PaaSRoleType[])((global::System.Fabric.Management.ServiceModel.PaaSRoleType[])o.@Roles);
                if (a != null){
                    WriteStartElement(@"Roles", namespace52, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace53 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write27_PaaSRoleType(@"Role", namespace53, ((global::System.Fabric.Management.ServiceModel.PaaSRoleType)a[ia]), false, false, namespace53, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            string namespace54 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.PaaSVoteType[] a = (global::System.Fabric.Management.ServiceModel.PaaSVoteType[])((global::System.Fabric.Management.ServiceModel.PaaSVoteType[])o.@Votes);
                if (a != null){
                    WriteStartElement(@"Votes", namespace54, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace55 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write28_PaaSVoteType(@"Vote", namespace55, ((global::System.Fabric.Management.ServiceModel.PaaSVoteType)a[ia]), false, false, namespace55, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write28_PaaSVoteType(string n, string ns, global::System.Fabric.Management.ServiceModel.PaaSVoteType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.PaaSVoteType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"PaaSVoteType", defaultNamespace);
            WriteAttribute(@"NodeName", @"", ((global::System.String)o.@NodeName));
            WriteAttribute(@"IPAddressOrFQDN", @"", ((global::System.String)o.@IPAddressOrFQDN));
            WriteAttribute(@"Port", @"", System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o.@Port)));
            WriteEndElement(o);
        }

        void Write27_PaaSRoleType(string n, string ns, global::System.Fabric.Management.ServiceModel.PaaSRoleType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.PaaSRoleType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"PaaSRoleType", defaultNamespace);
            WriteAttribute(@"RoleName", @"", ((global::System.String)o.@RoleName));
            WriteAttribute(@"NodeTypeRef", @"", ((global::System.String)o.@NodeTypeRef));
            WriteAttribute(@"RoleNodeCount", @"", System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o.@RoleNodeCount)));
            WriteEndElement(o);
        }

        void Write23_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureFlexibleDynamicTopology o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureFlexibleDynamicTopology)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            string namespace56 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.StaticVoteType[] a = (global::System.Fabric.Management.ServiceModel.StaticVoteType[])((global::System.Fabric.Management.ServiceModel.StaticVoteType[])o.@StaticVotes);
                if (a != null){
                    WriteStartElement(@"StaticVotes", namespace56, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace57 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write22_StaticVoteType(@"StaticVote", namespace57, ((global::System.Fabric.Management.ServiceModel.StaticVoteType)a[ia]), false, false, namespace57, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write22_StaticVoteType(string n, string ns, global::System.Fabric.Management.ServiceModel.StaticVoteType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.StaticVoteType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"StaticVoteType", defaultNamespace);
            WriteAttribute(@"NodeName", @"", ((global::System.String)o.@NodeName));
            WriteAttribute(@"IPAddressOrFQDN", @"", ((global::System.String)o.@IPAddressOrFQDN));
            WriteAttribute(@"NodeTypeRef", @"", ((global::System.String)o.@NodeTypeRef));
            WriteEndElement(o);
        }

        void Write31_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzure o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzure)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            string namespace58 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.AzureRoleType[] a = (global::System.Fabric.Management.ServiceModel.AzureRoleType[])((global::System.Fabric.Management.ServiceModel.AzureRoleType[])o.@Roles);
                if (a != null){
                    WriteStartElement(@"Roles", namespace58, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace59 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write30_AzureRoleType(@"Role", namespace59, ((global::System.Fabric.Management.ServiceModel.AzureRoleType)a[ia]), false, false, namespace59, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write30_AzureRoleType(string n, string ns, global::System.Fabric.Management.ServiceModel.AzureRoleType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.AzureRoleType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"AzureRoleType", defaultNamespace);
            WriteAttribute(@"RoleName", @"", ((global::System.String)o.@RoleName));
            WriteAttribute(@"NodeTypeRef", @"", ((global::System.String)o.@NodeTypeRef));
            if (((global::System.Int32)o.@SeedNodeCount) != 0) {
                WriteAttribute(@"SeedNodeCount", @"", System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o.@SeedNodeCount)));
            }
            WriteEndElement(o);
        }

        void Write21_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureBlackbird o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureBlackbird)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            string namespace60 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.BlackbirdRoleType[] a = (global::System.Fabric.Management.ServiceModel.BlackbirdRoleType[])((global::System.Fabric.Management.ServiceModel.BlackbirdRoleType[])o.@Roles);
                if (a != null){
                    WriteStartElement(@"Roles", namespace60, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace61 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write20_BlackbirdRoleType(@"Role", namespace61, ((global::System.Fabric.Management.ServiceModel.BlackbirdRoleType)a[ia]), false, false, namespace61, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write20_BlackbirdRoleType(string n, string ns, global::System.Fabric.Management.ServiceModel.BlackbirdRoleType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.BlackbirdRoleType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"BlackbirdRoleType", defaultNamespace);
            WriteAttribute(@"EnvironmentName", @"", ((global::System.String)o.@EnvironmentName));
            WriteAttribute(@"RoleName", @"", ((global::System.String)o.@RoleName));
            WriteAttribute(@"NodeTypeRef", @"", ((global::System.String)o.@NodeTypeRef));
            if (((global::System.Boolean)o.@IsSeedNode) != false) {
                WriteAttribute(@"IsSeedNode", @"", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)o.@IsSeedNode)));
            }
            WriteEndElement(o);
        }

        void Write34_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsServer o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsServer)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            if (((global::System.Boolean)o.@IsScaleMin) != false) {
                WriteAttribute(@"IsScaleMin", @"", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)o.@IsScaleMin)));
            }
            string namespace62 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.FabricNodeType[] a = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])((global::System.Fabric.Management.ServiceModel.FabricNodeType[])o.@NodeList);
                if (a != null){
                    WriteStartElement(@"NodeList", namespace62, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace63 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write24_FabricNodeType(@"Node", namespace63, ((global::System.Fabric.Management.ServiceModel.FabricNodeType)a[ia]), false, false, namespace63, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write24_FabricNodeType(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricNodeType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricNodeType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"FabricNodeType", defaultNamespace);
            WriteAttribute(@"NodeName", @"", ((global::System.String)o.@NodeName));
            WriteAttribute(@"IPAddressOrFQDN", @"", ((global::System.String)o.@IPAddressOrFQDN));
            if (((global::System.Boolean)o.@IsSeedNode) != false) {
                WriteAttribute(@"IsSeedNode", @"", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)o.@IsSeedNode)));
            }
            WriteAttribute(@"NodeTypeRef", @"", ((global::System.String)o.@NodeTypeRef));
            WriteAttribute(@"FaultDomain", @"", ((global::System.String)o.@FaultDomain));
            WriteAttribute(@"UpgradeDomain", @"", ((global::System.String)o.@UpgradeDomain));
            WriteAttribute(@"InfrastructurePlacementID", @"", ((global::System.String)o.@InfrastructurePlacementID));
            WriteEndElement(o);
        }

        void Write33_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzureStaticTopology o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzureStaticTopology)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            string namespace64 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.FabricNodeType[] a = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])((global::System.Fabric.Management.ServiceModel.FabricNodeType[])o.@NodeList);
                if (a != null){
                    WriteStartElement(@"NodeList", namespace64, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace65 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write24_FabricNodeType(@"Node", namespace65, ((global::System.Fabric.Management.ServiceModel.FabricNodeType)a[ia]), false, false, namespace65, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write26_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureLinux o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureLinux)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            if (((global::System.Boolean)o.@IsScaleMin) != false) {
                WriteAttribute(@"IsScaleMin", @"", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)o.@IsScaleMin)));
            }
            string namespace66 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.FabricNodeType[] a = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])((global::System.Fabric.Management.ServiceModel.FabricNodeType[])o.@NodeList);
                if (a != null){
                    WriteStartElement(@"NodeList", namespace66, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace67 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write24_FabricNodeType(@"Node", namespace67, ((global::System.Fabric.Management.ServiceModel.FabricNodeType)a[ia]), false, false, namespace67, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write19_ClusterManifestTypeNodeType(string n, string ns, global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            WriteAttribute(@"Name", @"", ((global::System.String)o.@Name));
            string namespace68 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            if (((global::System.Boolean)o.@AllowStatefulWorkloads) != true) {
                WriteElementStringRaw(@"AllowStatefulWorkloads", namespace68, System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)o.@AllowStatefulWorkloads)));
            }
            string namespace69 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write8_FabricEndpointsType(@"Endpoints", namespace69, ((global::System.Fabric.Management.ServiceModel.FabricEndpointsType)o.@Endpoints), false, false, namespace69, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace70 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write12_FabricKtlLoggerSettingsType(@"KtlLoggerSettings", namespace70, ((global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsType)o.@KtlLoggerSettings), false, false, namespace70, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace71 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.LogicalDirectoryType[] a = (global::System.Fabric.Management.ServiceModel.LogicalDirectoryType[])((global::System.Fabric.Management.ServiceModel.LogicalDirectoryType[])o.@LogicalDirectories);
                if (a != null){
                    WriteStartElement(@"LogicalDirectories", namespace71, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace72 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write14_LogicalDirectoryType(@"LogicalDirectory", namespace72, ((global::System.Fabric.Management.ServiceModel.LogicalDirectoryType)a[ia]), false, false, namespace72, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            string namespace73 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write17_CertificatesType(@"Certificates", namespace73, ((global::System.Fabric.Management.ServiceModel.CertificatesType)o.@Certificates), false, false, namespace73, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace74 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])((global::System.Fabric.Management.ServiceModel.KeyValuePairType[])o.@PlacementProperties);
                if (a != null){
                    WriteStartElement(@"PlacementProperties", namespace74, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace75 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write18_KeyValuePairType(@"Property", namespace75, ((global::System.Fabric.Management.ServiceModel.KeyValuePairType)a[ia]), false, false, namespace75, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            string namespace76 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])((global::System.Fabric.Management.ServiceModel.KeyValuePairType[])o.@Capacities);
                if (a != null){
                    WriteStartElement(@"Capacities", namespace76, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace77 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write18_KeyValuePairType(@"Capacity", namespace77, ((global::System.Fabric.Management.ServiceModel.KeyValuePairType)a[ia]), false, false, namespace77, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            string namespace78 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])((global::System.Fabric.Management.ServiceModel.KeyValuePairType[])o.@SfssRgPolicies);
                if (a != null){
                    WriteStartElement(@"SfssRgPolicies", namespace78, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace79 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write18_KeyValuePairType(@"SfssRgPolicy", namespace79, ((global::System.Fabric.Management.ServiceModel.KeyValuePairType)a[ia]), false, false, namespace79, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            string namespace80 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            {
                global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])((global::System.Fabric.Management.ServiceModel.KeyValuePairType[])o.@UserServiceMetricCapacities);
                if (a != null){
                    WriteStartElement(@"UserServiceMetricCapacities", namespace80, null, false);
                    for (int ia = 0; ia < a.Length; ia++) {
                        string namespace81 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
                        Write18_KeyValuePairType(@"Parameter", namespace81, ((global::System.Fabric.Management.ServiceModel.KeyValuePairType)a[ia]), false, false, namespace81, @"http://schemas.microsoft.com/2011/01/fabric");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write18_KeyValuePairType(string n, string ns, global::System.Fabric.Management.ServiceModel.KeyValuePairType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"KeyValuePairType", defaultNamespace);
            WriteAttribute(@"Name", @"", ((global::System.String)o.@Name));
            WriteAttribute(@"Value", @"", ((global::System.String)o.@Value));
            WriteEndElement(o);
        }

        void Write14_LogicalDirectoryType(string n, string ns, global::System.Fabric.Management.ServiceModel.LogicalDirectoryType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.LogicalDirectoryType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"LogicalDirectoryType", defaultNamespace);
            WriteAttribute(@"LogicalDirectoryName", @"", ((global::System.String)o.@LogicalDirectoryName));
            WriteAttribute(@"MappedTo", @"", ((global::System.String)o.@MappedTo));
            if (((global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext)o.@Context) != global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext.@application) {
                WriteAttribute(@"Context", @"", Write13_LogicalDirectoryTypeContext(((global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext)o.@Context)));
            }
            WriteEndElement(o);
        }

        string Write13_LogicalDirectoryTypeContext(global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext v) {
            string parentRuntimeNs = null;
            string parentCompileTimeNs = null;
            string s = null;
            switch (v) {
                case global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext.@application: s = @"application"; break;
                case global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext.@node: s = @"node"; break;
                default: throw CreateInvalidEnumValueException(((System.Int64)v).ToString(System.Globalization.CultureInfo.InvariantCulture), @"System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext");
            }
            return s;
        }

        void Write12_FabricKtlLoggerSettingsType(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsType o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsType)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"FabricKtlLoggerSettingsType", defaultNamespace);
            string namespace82 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write9_Item(@"SharedLogFilePath", namespace82, ((global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFilePath)o.@SharedLogFilePath), false, false, namespace82, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace83 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write10_Item(@"SharedLogFileId", namespace83, ((global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileId)o.@SharedLogFileId), false, false, namespace83, @"http://schemas.microsoft.com/2011/01/fabric");
            string namespace84 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/2011/01/fabric" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/2011/01/fabric";
            Write11_Item(@"SharedLogFileSizeInMB", namespace84, ((global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileSizeInMB)o.@SharedLogFileSizeInMB), false, false, namespace84, @"http://schemas.microsoft.com/2011/01/fabric");
            WriteEndElement(o);
        }

        void Write11_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileSizeInMB o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileSizeInMB)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            WriteAttribute(@"Value", @"", System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o.@Value)));
            WriteEndElement(o);
        }

        void Write10_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileId o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileId)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            WriteAttribute(@"Value", @"", ((global::System.String)o.@Value));
            WriteEndElement(o);
        }

        void Write9_Item(string n, string ns, global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFilePath o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFilePath)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(null, defaultNamespace);
            WriteAttribute(@"Value", @"", ((global::System.String)o.@Value));
            WriteEndElement(o);
        }

        protected override void InitCallbacks() {
        }
    }

    [System.Runtime.CompilerServices.__BlockReflection]
    public class XmlSerializationReader1 : System.Xml.Serialization.XmlSerializationReader {

        public object Read44_ClusterManifest(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id1_ClusterManifest && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    o = Read39_ClusterManifestType(false, true, defaultNamespace);
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":ClusterManifest");
            }
            return (object)o;
        }

        public object Read45_InfrastructureInformation(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id3_InfrastructureInformation && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    o = Read41_InfrastructureInformationType(false, true, defaultNamespace);
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":InfrastructureInformation");
            }
            return (object)o;
        }

        public object Read46_TargetInformation(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id4_TargetInformation && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    o = Read43_TargetInformationType(false, true, defaultNamespace);
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":TargetInformation");
            }
            return (object)o;
        }

        global::System.Fabric.Management.ServiceModel.TargetInformationType Read43_TargetInformationType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id5_TargetInformationType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.TargetInformationType o;
            o = new global::System.Fabric.Management.ServiceModel.TargetInformationType();
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations0 = 0;
            int readerCount0 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id6_CurrentInstallation && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@CurrentInstallation = Read42_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id7_TargetInstallation && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@TargetInstallation = Read42_Item(false, true, defaultNamespace);
                        paramsRead[1] = true;
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:CurrentInstallation, http://schemas.microsoft.com/2011/01/fabric:TargetInstallation");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:CurrentInstallation, http://schemas.microsoft.com/2011/01/fabric:TargetInstallation");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations0, ref readerCount0);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.WindowsFabricDeploymentInformation Read42_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id8_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.WindowsFabricDeploymentInformation o;
            o = new global::System.Fabric.Management.ServiceModel.WindowsFabricDeploymentInformation();
            bool[] paramsRead = new bool[11];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id9_InstanceId && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@InstanceId = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id11_MSILocation && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@MSILocation = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id12_ClusterManifestLocation && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@ClusterManifestLocation = Reader.Value;
                    paramsRead[2] = true;
                }
                else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id13_InfrastructureManifestLocation && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@InfrastructureManifestLocation = Reader.Value;
                    paramsRead[3] = true;
                }
                else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id14_TargetVersion && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@TargetVersion = Reader.Value;
                    paramsRead[4] = true;
                }
                else if (!paramsRead[5] && ((object) Reader.LocalName == (object)id15_NodeName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeName = Reader.Value;
                    paramsRead[5] = true;
                }
                else if (!paramsRead[6] && ((object) Reader.LocalName == (object)id16_RemoveNodeState && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@RemoveNodeState = System.Xml.XmlConvert.ToBoolean(Reader.Value);
                    paramsRead[6] = true;
                }
                else if (!paramsRead[7] && ((object) Reader.LocalName == (object)id17_UpgradeEntryPointExe && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@UpgradeEntryPointExe = Reader.Value;
                    paramsRead[7] = true;
                }
                else if (!paramsRead[8] && ((object) Reader.LocalName == (object)id18_UpgradeEntryPointExeParameters && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@UpgradeEntryPointExeParameters = Reader.Value;
                    paramsRead[8] = true;
                }
                else if (!paramsRead[9] && ((object) Reader.LocalName == (object)id19_UndoUpgradeEntryPointExe && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@UndoUpgradeEntryPointExe = Reader.Value;
                    paramsRead[9] = true;
                }
                else if (!paramsRead[10] && ((object) Reader.LocalName == (object)id20_Item && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@UndoUpgradeEntryPointExeParameters = Reader.Value;
                    paramsRead[10] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":InstanceId, :MSILocation, :ClusterManifestLocation, :InfrastructureManifestLocation, :TargetVersion, :NodeName, :RemoveNodeState, :UpgradeEntryPointExe, :UpgradeEntryPointExeParameters, :UndoUpgradeEntryPointExe, :UndoUpgradeEntryPointExeParameters");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations1 = 0;
            int readerCount1 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations1, ref readerCount1);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.InfrastructureInformationType Read41_InfrastructureInformationType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id21_InfrastructureInformationType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.InfrastructureInformationType o;
            o = new global::System.Fabric.Management.ServiceModel.InfrastructureInformationType();
            global::System.Fabric.Management.ServiceModel.InfrastructureNodeType[] a_0 = null;
            int ca_0 = 0;
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations2 = 0;
            int readerCount2 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id22_NodeList && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.InfrastructureNodeType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations3 = 0;
                                int readerCount3 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id23_Node && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.InfrastructureNodeType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.InfrastructureNodeType));a_0_0[ca_0_0++] = Read40_InfrastructureNodeType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Node");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Node");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations3, ref readerCount3);
                                }
                            ReadEndElement();
                            }
                            o.@NodeList = (global::System.Fabric.Management.ServiceModel.InfrastructureNodeType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.InfrastructureNodeType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeList");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeList");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations2, ref readerCount2);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.InfrastructureNodeType Read40_InfrastructureNodeType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id24_InfrastructureNodeType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.InfrastructureNodeType o;
            o = new global::System.Fabric.Management.ServiceModel.InfrastructureNodeType();
            bool[] paramsRead = new bool[10];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[2] && ((object) Reader.LocalName == (object)id15_NodeName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeName = Reader.Value;
                    paramsRead[2] = true;
                }
                else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id25_IPAddressOrFQDN && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IPAddressOrFQDN = Reader.Value;
                    paramsRead[3] = true;
                }
                else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id26_RoleOrTierName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@RoleOrTierName = Reader.Value;
                    paramsRead[4] = true;
                }
                else if (!paramsRead[5] && ((object) Reader.LocalName == (object)id27_NodeTypeRef && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeTypeRef = Reader.Value;
                    paramsRead[5] = true;
                }
                else if (!paramsRead[6] && ((object) Reader.LocalName == (object)id28_IsSeedNode && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IsSeedNode = System.Xml.XmlConvert.ToBoolean(Reader.Value);
                    paramsRead[6] = true;
                }
                else if (!paramsRead[7] && ((object) Reader.LocalName == (object)id29_FaultDomain && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@FaultDomain = CollapseWhitespace(Reader.Value);
                    paramsRead[7] = true;
                }
                else if (!paramsRead[8] && ((object) Reader.LocalName == (object)id30_UpgradeDomain && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@UpgradeDomain = CollapseWhitespace(Reader.Value);
                    paramsRead[8] = true;
                }
                else if (!paramsRead[9] && ((object) Reader.LocalName == (object)id31_InfrastructurePlacementID && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@InfrastructurePlacementID = Reader.Value;
                    paramsRead[9] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":NodeName, :IPAddressOrFQDN, :RoleOrTierName, :NodeTypeRef, :IsSeedNode, :FaultDomain, :UpgradeDomain, :InfrastructurePlacementID");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations4 = 0;
            int readerCount4 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id32_Endpoints && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Endpoints = Read8_FabricEndpointsType(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id33_Certificates && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Certificates = Read17_CertificatesType(false, true, defaultNamespace);
                        paramsRead[1] = true;
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Endpoints, http://schemas.microsoft.com/2011/01/fabric:Certificates");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Endpoints, http://schemas.microsoft.com/2011/01/fabric:Certificates");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations4, ref readerCount4);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.CertificatesType Read17_CertificatesType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id34_CertificatesType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.CertificatesType o;
            o = new global::System.Fabric.Management.ServiceModel.CertificatesType();
            bool[] paramsRead = new bool[4];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations5 = 0;
            int readerCount5 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id35_ClusterCertificate && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ClusterCertificate = Read16_FabricCertificateType(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id36_ServerCertificate && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ServerCertificate = Read16_FabricCertificateType(false, true, defaultNamespace);
                        paramsRead[1] = true;
                    }
                    else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id37_ClientCertificate && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ClientCertificate = Read16_FabricCertificateType(false, true, defaultNamespace);
                        paramsRead[2] = true;
                    }
                    else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id38_UserRoleClientCertificate && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@UserRoleClientCertificate = Read16_FabricCertificateType(false, true, defaultNamespace);
                        paramsRead[3] = true;
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:ClusterCertificate, http://schemas.microsoft.com/2011/01/fabric:ServerCertificate, http://schemas.microsoft.com/2011/01/fabric:ClientCertificate, http://schemas.microsoft.com/2011/01/fabric:UserRoleClientCertificate");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:ClusterCertificate, http://schemas.microsoft.com/2011/01/fabric:ServerCertificate, http://schemas.microsoft.com/2011/01/fabric:ClientCertificate, http://schemas.microsoft.com/2011/01/fabric:UserRoleClientCertificate");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations5, ref readerCount5);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.FabricCertificateType Read16_FabricCertificateType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id39_FabricCertificateType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricCertificateType o;
            o = new global::System.Fabric.Management.ServiceModel.FabricCertificateType();
            bool[] paramsRead = new bool[5];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id40_X509StoreName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@X509StoreName = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id41_X509FindType && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@X509FindType = Read15_Item(Reader.Value);
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id42_X509FindValue && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@X509FindValue = Reader.Value;
                    paramsRead[2] = true;
                }
                else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id43_X509FindValueSecondary && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@X509FindValueSecondary = Reader.Value;
                    paramsRead[3] = true;
                }
                else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id44_Name && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Name = Reader.Value;
                    paramsRead[4] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":X509StoreName, :X509FindType, :X509FindValue, :X509FindValueSecondary, :Name");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations6 = 0;
            int readerCount6 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations6, ref readerCount6);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType Read15_Item(string s) {
            switch (s) {
                case @"FindByThumbprint": return global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType.@FindByThumbprint;
                case @"FindBySubjectName": return global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType.@FindBySubjectName;
                case @"FindByExtension": return global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType.@FindByExtension;
                default: throw CreateUnknownConstantException(s, typeof(global::System.Fabric.Management.ServiceModel.FabricCertificateTypeX509FindType));
            }
        }

        global::System.Fabric.Management.ServiceModel.FabricEndpointsType Read8_FabricEndpointsType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id45_FabricEndpointsType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricEndpointsType o;
            o = new global::System.Fabric.Management.ServiceModel.FabricEndpointsType();
            bool[] paramsRead = new bool[23];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations7 = 0;
            int readerCount7 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id46_ClientConnectionEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ClientConnectionEndpoint = Read3_InputEndpointType(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id47_LeaseDriverEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@LeaseDriverEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[1] = true;
                    }
                    else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id48_NotificationEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@NotificationEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[2] = true;
                    }
                    else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id49_ClusterConnectionEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ClusterConnectionEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[3] = true;
                    }
                    else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id50_HttpGatewayEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@HttpGatewayEndpoint = Read3_InputEndpointType(false, true, defaultNamespace);
                        paramsRead[4] = true;
                    }
                    else if (!paramsRead[5] && ((object) Reader.LocalName == (object)id51_HttpApplicationGatewayEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@HttpApplicationGatewayEndpoint = Read3_InputEndpointType(false, true, defaultNamespace);
                        paramsRead[5] = true;
                    }
                    else if (!paramsRead[6] && ((object) Reader.LocalName == (object)id52_ServiceConnectionEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ServiceConnectionEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[6] = true;
                    }
                    else if (!paramsRead[7] && ((object) Reader.LocalName == (object)id53_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ClusterManagerReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[7] = true;
                    }
                    else if (!paramsRead[8] && ((object) Reader.LocalName == (object)id54_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@RepairManagerReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[8] = true;
                    }
                    else if (!paramsRead[9] && ((object) Reader.LocalName == (object)id55_NamingReplicatorEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@NamingReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[9] = true;
                    }
                    else if (!paramsRead[10] && ((object) Reader.LocalName == (object)id56_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@FailoverManagerReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[10] = true;
                    }
                    else if (!paramsRead[11] && ((object) Reader.LocalName == (object)id57_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ImageStoreServiceReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[11] = true;
                    }
                    else if (!paramsRead[12] && ((object) Reader.LocalName == (object)id58_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@UpgradeServiceReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[12] = true;
                    }
                    else if (!paramsRead[13] && ((object) Reader.LocalName == (object)id59_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@FaultAnalysisServiceReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[13] = true;
                    }
                    else if (!paramsRead[14] && ((object) Reader.LocalName == (object)id60_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@BackupRestoreServiceReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[14] = true;
                    }
                    else if (!paramsRead[15] && ((object) Reader.LocalName == (object)id61_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@UpgradeOrchestrationServiceReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[15] = true;
                    }
                    else if (!paramsRead[16] && ((object) Reader.LocalName == (object)id62_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@CentralSecretServiceReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[16] = true;
                    }
                    else if (!paramsRead[17] && ((object) Reader.LocalName == (object)id63_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@EventStoreServiceReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[17] = true;
                    }
                    else if (!paramsRead[18] && ((object) Reader.LocalName == (object)id64_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@GatewayResourceManagerReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[18] = true;
                    }
                    else if (!paramsRead[19] && ((object) Reader.LocalName == (object)id65_DefaultReplicatorEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@DefaultReplicatorEndpoint = Read5_InternalEndpointType(false, true, defaultNamespace);
                        paramsRead[19] = true;
                    }
                    else if (!paramsRead[20] && ((object) Reader.LocalName == (object)id66_TokenServiceEndpoint && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@TokenServiceEndpoint = Read3_InputEndpointType(false, true, defaultNamespace);
                        paramsRead[20] = true;
                    }
                    else if (!paramsRead[21] && ((object) Reader.LocalName == (object)id67_ApplicationEndpoints && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@ApplicationEndpoints = Read6_Item(false, true, defaultNamespace);
                        paramsRead[21] = true;
                    }
                    else if (!paramsRead[22] && ((object) Reader.LocalName == (object)id68_EphemeralEndpoints && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@EphemeralEndpoints = Read7_Item(false, true, defaultNamespace);
                        paramsRead[22] = true;
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:ClientConnectionEndpoint, http://schemas.microsoft.com/2011/01/fabric:LeaseDriverEndpoint, http://schemas.microsoft.com/2011/01/fabric:NotificationEndpoint, http://schemas.microsoft.com/2011/01/fabric:ClusterConnectionEndpoint, http://schemas.microsoft.com/2011/01/fabric:HttpGatewayEndpoint, http://schemas.microsoft.com/2011/01/fabric:HttpApplicationGatewayEndpoint, http://schemas.microsoft.com/2011/01/fabric:ServiceConnectionEndpoint, http://schemas.microsoft.com/2011/01/fabric:ClusterManagerReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:RepairManagerReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:NamingReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:FailoverManagerReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:ImageStoreServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:UpgradeServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:FaultAnalysisServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:BackupRestoreServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:UpgradeOrchestrationServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:CentralSecretServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:EventStoreServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:GatewayResourceManagerReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:DefaultReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:TokenServiceEndpoint, http://schemas.microsoft.com/2011/01/fabric:ApplicationEndpoints, http://schemas.microsoft.com/2011/01/fabric:EphemeralEndpoints");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:ClientConnectionEndpoint, http://schemas.microsoft.com/2011/01/fabric:LeaseDriverEndpoint, http://schemas.microsoft.com/2011/01/fabric:NotificationEndpoint, http://schemas.microsoft.com/2011/01/fabric:ClusterConnectionEndpoint, http://schemas.microsoft.com/2011/01/fabric:HttpGatewayEndpoint, http://schemas.microsoft.com/2011/01/fabric:HttpApplicationGatewayEndpoint, http://schemas.microsoft.com/2011/01/fabric:ServiceConnectionEndpoint, http://schemas.microsoft.com/2011/01/fabric:ClusterManagerReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:RepairManagerReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:NamingReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:FailoverManagerReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:ImageStoreServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:UpgradeServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:FaultAnalysisServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:BackupRestoreServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:UpgradeOrchestrationServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:CentralSecretServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:EventStoreServiceReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:GatewayResourceManagerReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:DefaultReplicatorEndpoint, http://schemas.microsoft.com/2011/01/fabric:TokenServiceEndpoint, http://schemas.microsoft.com/2011/01/fabric:ApplicationEndpoints, http://schemas.microsoft.com/2011/01/fabric:EphemeralEndpoints");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations7, ref readerCount7);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeEphemeralEndpoints Read7_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeEphemeralEndpoints o;
            o = new global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeEphemeralEndpoints();
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id69_StartPort && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@StartPort = System.Xml.XmlConvert.ToInt32(Reader.Value);
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id70_EndPort && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@EndPort = System.Xml.XmlConvert.ToInt32(Reader.Value);
                    paramsRead[1] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":StartPort, :EndPort");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations8 = 0;
            int readerCount8 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations8, ref readerCount8);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeApplicationEndpoints Read6_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeApplicationEndpoints o;
            o = new global::System.Fabric.Management.ServiceModel.FabricEndpointsTypeApplicationEndpoints();
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id69_StartPort && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@StartPort = System.Xml.XmlConvert.ToInt32(Reader.Value);
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id70_EndPort && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@EndPort = System.Xml.XmlConvert.ToInt32(Reader.Value);
                    paramsRead[1] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":StartPort, :EndPort");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations9 = 0;
            int readerCount9 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations9, ref readerCount9);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.InputEndpointType Read3_InputEndpointType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id71_InputEndpointType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.InputEndpointType o;
            o = new global::System.Fabric.Management.ServiceModel.InputEndpointType();
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id72_Port && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Port = CollapseWhitespace(Reader.Value);
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id73_Protocol && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Protocol = Read1_InputEndpointTypeProtocol(Reader.Value);
                    paramsRead[1] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Port, :Protocol");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations10 = 0;
            int readerCount10 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations10, ref readerCount10);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol Read1_InputEndpointTypeProtocol(string s) {
            switch (s) {
                case @"http": return global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol.@http;
                case @"https": return global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol.@https;
                case @"tcp": return global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol.@tcp;
                default: throw CreateUnknownConstantException(s, typeof(global::System.Fabric.Management.ServiceModel.InputEndpointTypeProtocol));
            }
        }

        global::System.Fabric.Management.ServiceModel.InternalEndpointType Read5_InternalEndpointType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id74_InternalEndpointType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.InternalEndpointType o;
            o = new global::System.Fabric.Management.ServiceModel.InternalEndpointType();
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id72_Port && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Port = CollapseWhitespace(Reader.Value);
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id73_Protocol && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Protocol = Read4_InternalEndpointTypeProtocol(Reader.Value);
                    paramsRead[1] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Port, :Protocol");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations11 = 0;
            int readerCount11 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations11, ref readerCount11);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol Read4_InternalEndpointTypeProtocol(string s) {
            switch (s) {
                case @"http": return global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol.@http;
                case @"https": return global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol.@https;
                case @"tcp": return global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol.@tcp;
                default: throw CreateUnknownConstantException(s, typeof(global::System.Fabric.Management.ServiceModel.InternalEndpointTypeProtocol));
            }
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestType Read39_ClusterManifestType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id75_ClusterManifestType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestType o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestType();
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType[] a_0 = null;
            int ca_0 = 0;
            global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection[] a_2 = null;
            int ca_2 = 0;
            bool[] paramsRead = new bool[7];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[4] && ((object) Reader.LocalName == (object)id44_Name && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Name = Reader.Value;
                    paramsRead[4] = true;
                }
                else if (!paramsRead[5] && ((object) Reader.LocalName == (object)id76_Version && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Version = Reader.Value;
                    paramsRead[5] = true;
                }
                else if (!paramsRead[6] && ((object) Reader.LocalName == (object)id77_Description && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Description = Reader.Value;
                    paramsRead[6] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Name, :Version, :Description");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations12 = 0;
            int readerCount12 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id78_NodeTypes && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations13 = 0;
                                int readerCount13 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id79_NodeType && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType));a_0_0[ca_0_0++] = Read19_ClusterManifestTypeNodeType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:NodeType");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:NodeType");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations13, ref readerCount13);
                                }
                            ReadEndElement();
                            }
                            o.@NodeTypes = (global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType), false);
                        }
                    }
                    else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id80_Infrastructure && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Infrastructure = Read35_Item(false, true, defaultNamespace);
                        paramsRead[1] = true;
                    }
                    else if (((object) Reader.LocalName == (object)id81_FabricSettings && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection[] a_2_0 = null;
                            int ca_2_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations14 = 0;
                                int readerCount14 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id82_Section && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_2_0 = (global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection[])EnsureArrayIndex(a_2_0, ca_2_0, typeof(global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection));a_2_0[ca_2_0++] = Read37_SettingsOverridesTypeSection(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Section");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Section");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations14, ref readerCount14);
                                }
                            ReadEndElement();
                            }
                            o.@FabricSettings = (global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection[])ShrinkArray(a_2_0, ca_2_0, typeof(global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection), false);
                        }
                    }
                    else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id33_Certificates && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Certificates = Read38_Item(false, true, defaultNamespace);
                        paramsRead[3] = true;
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeTypes, http://schemas.microsoft.com/2011/01/fabric:Infrastructure, http://schemas.microsoft.com/2011/01/fabric:FabricSettings, http://schemas.microsoft.com/2011/01/fabric:Certificates");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeTypes, http://schemas.microsoft.com/2011/01/fabric:Infrastructure, http://schemas.microsoft.com/2011/01/fabric:FabricSettings, http://schemas.microsoft.com/2011/01/fabric:Certificates");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations12, ref readerCount12);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeCertificates Read38_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeCertificates o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeCertificates();
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations15 = 0;
            int readerCount15 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id83_SecretsCertificate && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@SecretsCertificate = Read16_FabricCertificateType(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:SecretsCertificate");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:SecretsCertificate");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations15, ref readerCount15);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection Read37_SettingsOverridesTypeSection(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection o;
            o = new global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSection();
            global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter[] a_0 = null;
            int ca_0 = 0;
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[1] && ((object) Reader.LocalName == (object)id44_Name && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Name = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Name");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                o.@Parameter = (global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter[])ShrinkArray(a_0, ca_0, typeof(global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter), true);
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations16 = 0;
            int readerCount16 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id84_Parameter && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        a_0 = (global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter[])EnsureArrayIndex(a_0, ca_0, typeof(global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter));a_0[ca_0++] = Read36_Item(false, true, defaultNamespace);
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Parameter");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Parameter");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations16, ref readerCount16);
            }
            o.@Parameter = (global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter[])ShrinkArray(a_0, ca_0, typeof(global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter), true);
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter Read36_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter o;
            o = new global::System.Fabric.Management.ServiceModel.SettingsOverridesTypeSectionParameter();
            bool[] paramsRead = new bool[4];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id44_Name && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Name = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id85_Value && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Value = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id86_IsEncrypted && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IsEncrypted = System.Xml.XmlConvert.ToBoolean(Reader.Value);
                    paramsRead[2] = true;
                }
                else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id87_Type && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Type = Reader.Value;
                    paramsRead[3] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Name, :Value, :IsEncrypted, :Type");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations17 = 0;
            int readerCount17 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations17, ref readerCount17);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructure Read35_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructure o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructure();
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations18 = 0;
            int readerCount18 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id88_Blackbird && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Item = Read21_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[0] && ((object) Reader.LocalName == (object)id89_FlexibleDynamicTopology && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Item = Read23_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[0] && ((object) Reader.LocalName == (object)id90_Linux && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Item = Read26_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[0] && ((object) Reader.LocalName == (object)id91_PaaS && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Item = Read29_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[0] && ((object) Reader.LocalName == (object)id92_WindowsAzure && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Item = Read31_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[0] && ((object) Reader.LocalName == (object)id93_WindowsAzureStaticTopology && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Item = Read33_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[0] && ((object) Reader.LocalName == (object)id94_WindowsServer && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Item = Read34_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Blackbird, http://schemas.microsoft.com/2011/01/fabric:FlexibleDynamicTopology, http://schemas.microsoft.com/2011/01/fabric:Linux, http://schemas.microsoft.com/2011/01/fabric:PaaS, http://schemas.microsoft.com/2011/01/fabric:WindowsAzure, http://schemas.microsoft.com/2011/01/fabric:WindowsAzureStaticTopology, http://schemas.microsoft.com/2011/01/fabric:WindowsServer");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Blackbird, http://schemas.microsoft.com/2011/01/fabric:FlexibleDynamicTopology, http://schemas.microsoft.com/2011/01/fabric:Linux, http://schemas.microsoft.com/2011/01/fabric:PaaS, http://schemas.microsoft.com/2011/01/fabric:WindowsAzure, http://schemas.microsoft.com/2011/01/fabric:WindowsAzureStaticTopology, http://schemas.microsoft.com/2011/01/fabric:WindowsServer");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations18, ref readerCount18);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsServer Read34_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsServer o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsServer();
            global::System.Fabric.Management.ServiceModel.FabricNodeType[] a_0 = null;
            int ca_0 = 0;
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[1] && ((object) Reader.LocalName == (object)id95_IsScaleMin && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IsScaleMin = System.Xml.XmlConvert.ToBoolean(Reader.Value);
                    paramsRead[1] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":IsScaleMin");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations19 = 0;
            int readerCount19 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id22_NodeList && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.FabricNodeType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations20 = 0;
                                int readerCount20 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id23_Node && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.FabricNodeType));a_0_0[ca_0_0++] = Read24_FabricNodeType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Node");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Node");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations20, ref readerCount20);
                                }
                            ReadEndElement();
                            }
                            o.@NodeList = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.FabricNodeType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeList");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeList");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations19, ref readerCount19);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.FabricNodeType Read24_FabricNodeType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id96_FabricNodeType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricNodeType o;
            o = new global::System.Fabric.Management.ServiceModel.FabricNodeType();
            bool[] paramsRead = new bool[7];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id15_NodeName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeName = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id25_IPAddressOrFQDN && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IPAddressOrFQDN = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id28_IsSeedNode && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IsSeedNode = System.Xml.XmlConvert.ToBoolean(Reader.Value);
                    paramsRead[2] = true;
                }
                else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id27_NodeTypeRef && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeTypeRef = Reader.Value;
                    paramsRead[3] = true;
                }
                else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id29_FaultDomain && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@FaultDomain = CollapseWhitespace(Reader.Value);
                    paramsRead[4] = true;
                }
                else if (!paramsRead[5] && ((object) Reader.LocalName == (object)id30_UpgradeDomain && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@UpgradeDomain = CollapseWhitespace(Reader.Value);
                    paramsRead[5] = true;
                }
                else if (!paramsRead[6] && ((object) Reader.LocalName == (object)id31_InfrastructurePlacementID && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@InfrastructurePlacementID = Reader.Value;
                    paramsRead[6] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":NodeName, :IPAddressOrFQDN, :IsSeedNode, :NodeTypeRef, :FaultDomain, :UpgradeDomain, :InfrastructurePlacementID");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations21 = 0;
            int readerCount21 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations21, ref readerCount21);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzureStaticTopology Read33_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzureStaticTopology o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzureStaticTopology();
            global::System.Fabric.Management.ServiceModel.FabricNodeType[] a_0 = null;
            int ca_0 = 0;
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations22 = 0;
            int readerCount22 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id22_NodeList && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.FabricNodeType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations23 = 0;
                                int readerCount23 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id23_Node && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.FabricNodeType));a_0_0[ca_0_0++] = Read24_FabricNodeType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Node");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Node");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations23, ref readerCount23);
                                }
                            ReadEndElement();
                            }
                            o.@NodeList = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.FabricNodeType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeList");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeList");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations22, ref readerCount22);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzure Read31_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzure o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureWindowsAzure();
            global::System.Fabric.Management.ServiceModel.AzureRoleType[] a_0 = null;
            int ca_0 = 0;
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations24 = 0;
            int readerCount24 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id97_Roles && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.AzureRoleType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations25 = 0;
                                int readerCount25 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id98_Role && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.AzureRoleType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.AzureRoleType));a_0_0[ca_0_0++] = Read30_AzureRoleType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Role");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Role");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations25, ref readerCount25);
                                }
                            ReadEndElement();
                            }
                            o.@Roles = (global::System.Fabric.Management.ServiceModel.AzureRoleType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.AzureRoleType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Roles");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Roles");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations24, ref readerCount24);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.AzureRoleType Read30_AzureRoleType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id99_AzureRoleType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.AzureRoleType o;
            o = new global::System.Fabric.Management.ServiceModel.AzureRoleType();
            bool[] paramsRead = new bool[3];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id100_RoleName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@RoleName = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id27_NodeTypeRef && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeTypeRef = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id101_SeedNodeCount && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@SeedNodeCount = System.Xml.XmlConvert.ToInt32(Reader.Value);
                    paramsRead[2] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":RoleName, :NodeTypeRef, :SeedNodeCount");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations26 = 0;
            int readerCount26 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations26, ref readerCount26);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructurePaaS Read29_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructurePaaS o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructurePaaS();
            global::System.Fabric.Management.ServiceModel.PaaSRoleType[] a_0 = null;
            int ca_0 = 0;
            global::System.Fabric.Management.ServiceModel.PaaSVoteType[] a_1 = null;
            int ca_1 = 0;
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations27 = 0;
            int readerCount27 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id97_Roles && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.PaaSRoleType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations28 = 0;
                                int readerCount28 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id98_Role && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.PaaSRoleType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.PaaSRoleType));a_0_0[ca_0_0++] = Read27_PaaSRoleType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Role");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Role");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations28, ref readerCount28);
                                }
                            ReadEndElement();
                            }
                            o.@Roles = (global::System.Fabric.Management.ServiceModel.PaaSRoleType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.PaaSRoleType), false);
                        }
                    }
                    else if (((object) Reader.LocalName == (object)id102_Votes && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.PaaSVoteType[] a_1_0 = null;
                            int ca_1_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations29 = 0;
                                int readerCount29 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id103_Vote && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_1_0 = (global::System.Fabric.Management.ServiceModel.PaaSVoteType[])EnsureArrayIndex(a_1_0, ca_1_0, typeof(global::System.Fabric.Management.ServiceModel.PaaSVoteType));a_1_0[ca_1_0++] = Read28_PaaSVoteType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Vote");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Vote");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations29, ref readerCount29);
                                }
                            ReadEndElement();
                            }
                            o.@Votes = (global::System.Fabric.Management.ServiceModel.PaaSVoteType[])ShrinkArray(a_1_0, ca_1_0, typeof(global::System.Fabric.Management.ServiceModel.PaaSVoteType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Roles, http://schemas.microsoft.com/2011/01/fabric:Votes");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Roles, http://schemas.microsoft.com/2011/01/fabric:Votes");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations27, ref readerCount27);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.PaaSVoteType Read28_PaaSVoteType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id104_PaaSVoteType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.PaaSVoteType o;
            o = new global::System.Fabric.Management.ServiceModel.PaaSVoteType();
            bool[] paramsRead = new bool[3];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id15_NodeName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeName = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id25_IPAddressOrFQDN && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IPAddressOrFQDN = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id72_Port && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Port = System.Xml.XmlConvert.ToInt32(Reader.Value);
                    paramsRead[2] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":NodeName, :IPAddressOrFQDN, :Port");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations30 = 0;
            int readerCount30 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations30, ref readerCount30);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.PaaSRoleType Read27_PaaSRoleType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id105_PaaSRoleType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.PaaSRoleType o;
            o = new global::System.Fabric.Management.ServiceModel.PaaSRoleType();
            bool[] paramsRead = new bool[3];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id100_RoleName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@RoleName = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id27_NodeTypeRef && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeTypeRef = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id106_RoleNodeCount && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@RoleNodeCount = System.Xml.XmlConvert.ToInt32(Reader.Value);
                    paramsRead[2] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":RoleName, :NodeTypeRef, :RoleNodeCount");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations31 = 0;
            int readerCount31 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations31, ref readerCount31);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureLinux Read26_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureLinux o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureLinux();
            global::System.Fabric.Management.ServiceModel.FabricNodeType[] a_0 = null;
            int ca_0 = 0;
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[1] && ((object) Reader.LocalName == (object)id95_IsScaleMin && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IsScaleMin = System.Xml.XmlConvert.ToBoolean(Reader.Value);
                    paramsRead[1] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":IsScaleMin");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations32 = 0;
            int readerCount32 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id22_NodeList && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.FabricNodeType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations33 = 0;
                                int readerCount33 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id23_Node && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.FabricNodeType));a_0_0[ca_0_0++] = Read24_FabricNodeType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Node");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Node");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations33, ref readerCount33);
                                }
                            ReadEndElement();
                            }
                            o.@NodeList = (global::System.Fabric.Management.ServiceModel.FabricNodeType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.FabricNodeType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeList");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:NodeList");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations32, ref readerCount32);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureFlexibleDynamicTopology Read23_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureFlexibleDynamicTopology o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureFlexibleDynamicTopology();
            global::System.Fabric.Management.ServiceModel.StaticVoteType[] a_0 = null;
            int ca_0 = 0;
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations34 = 0;
            int readerCount34 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id107_StaticVotes && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.StaticVoteType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations35 = 0;
                                int readerCount35 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id108_StaticVote && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.StaticVoteType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.StaticVoteType));a_0_0[ca_0_0++] = Read22_StaticVoteType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:StaticVote");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:StaticVote");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations35, ref readerCount35);
                                }
                            ReadEndElement();
                            }
                            o.@StaticVotes = (global::System.Fabric.Management.ServiceModel.StaticVoteType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.StaticVoteType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:StaticVotes");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:StaticVotes");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations34, ref readerCount34);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.StaticVoteType Read22_StaticVoteType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id109_StaticVoteType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.StaticVoteType o;
            o = new global::System.Fabric.Management.ServiceModel.StaticVoteType();
            bool[] paramsRead = new bool[3];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id15_NodeName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeName = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id25_IPAddressOrFQDN && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IPAddressOrFQDN = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id27_NodeTypeRef && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeTypeRef = Reader.Value;
                    paramsRead[2] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":NodeName, :IPAddressOrFQDN, :NodeTypeRef");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations36 = 0;
            int readerCount36 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations36, ref readerCount36);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureBlackbird Read21_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureBlackbird o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeInfrastructureBlackbird();
            global::System.Fabric.Management.ServiceModel.BlackbirdRoleType[] a_0 = null;
            int ca_0 = 0;
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations37 = 0;
            int readerCount37 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (((object) Reader.LocalName == (object)id97_Roles && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.BlackbirdRoleType[] a_0_0 = null;
                            int ca_0_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations38 = 0;
                                int readerCount38 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id98_Role && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_0_0 = (global::System.Fabric.Management.ServiceModel.BlackbirdRoleType[])EnsureArrayIndex(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.BlackbirdRoleType));a_0_0[ca_0_0++] = Read20_BlackbirdRoleType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Role");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Role");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations38, ref readerCount38);
                                }
                            ReadEndElement();
                            }
                            o.@Roles = (global::System.Fabric.Management.ServiceModel.BlackbirdRoleType[])ShrinkArray(a_0_0, ca_0_0, typeof(global::System.Fabric.Management.ServiceModel.BlackbirdRoleType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Roles");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:Roles");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations37, ref readerCount37);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.BlackbirdRoleType Read20_BlackbirdRoleType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id110_BlackbirdRoleType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.BlackbirdRoleType o;
            o = new global::System.Fabric.Management.ServiceModel.BlackbirdRoleType();
            bool[] paramsRead = new bool[4];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id111_EnvironmentName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@EnvironmentName = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id100_RoleName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@RoleName = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id27_NodeTypeRef && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@NodeTypeRef = Reader.Value;
                    paramsRead[2] = true;
                }
                else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id28_IsSeedNode && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@IsSeedNode = System.Xml.XmlConvert.ToBoolean(Reader.Value);
                    paramsRead[3] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":EnvironmentName, :RoleName, :NodeTypeRef, :IsSeedNode");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations39 = 0;
            int readerCount39 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations39, ref readerCount39);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType Read19_ClusterManifestTypeNodeType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType o;
            o = new global::System.Fabric.Management.ServiceModel.ClusterManifestTypeNodeType();
            global::System.Fabric.Management.ServiceModel.LogicalDirectoryType[] a_3 = null;
            int ca_3 = 0;
            global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a_5 = null;
            int ca_5 = 0;
            global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a_6 = null;
            int ca_6 = 0;
            global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a_7 = null;
            int ca_7 = 0;
            global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a_8 = null;
            int ca_8 = 0;
            bool[] paramsRead = new bool[10];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[9] && ((object) Reader.LocalName == (object)id44_Name && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Name = Reader.Value;
                    paramsRead[9] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Name");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations40 = 0;
            int readerCount40 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id112_AllowStatefulWorkloads && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (Reader.IsEmptyElement) {
                            Reader.Skip();
                        }
                        else {
                            o.@AllowStatefulWorkloads = System.Xml.XmlConvert.ToBoolean(Reader.ReadElementContentAsString());
                        }
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id32_Endpoints && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Endpoints = Read8_FabricEndpointsType(false, true, defaultNamespace);
                        paramsRead[1] = true;
                    }
                    else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id113_KtlLoggerSettings && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@KtlLoggerSettings = Read12_FabricKtlLoggerSettingsType(false, true, defaultNamespace);
                        paramsRead[2] = true;
                    }
                    else if (((object) Reader.LocalName == (object)id114_LogicalDirectories && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.LogicalDirectoryType[] a_3_0 = null;
                            int ca_3_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations41 = 0;
                                int readerCount41 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id115_LogicalDirectory && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_3_0 = (global::System.Fabric.Management.ServiceModel.LogicalDirectoryType[])EnsureArrayIndex(a_3_0, ca_3_0, typeof(global::System.Fabric.Management.ServiceModel.LogicalDirectoryType));a_3_0[ca_3_0++] = Read14_LogicalDirectoryType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:LogicalDirectory");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:LogicalDirectory");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations41, ref readerCount41);
                                }
                            ReadEndElement();
                            }
                            o.@LogicalDirectories = (global::System.Fabric.Management.ServiceModel.LogicalDirectoryType[])ShrinkArray(a_3_0, ca_3_0, typeof(global::System.Fabric.Management.ServiceModel.LogicalDirectoryType), false);
                        }
                    }
                    else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id33_Certificates && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@Certificates = Read17_CertificatesType(false, true, defaultNamespace);
                        paramsRead[4] = true;
                    }
                    else if (((object) Reader.LocalName == (object)id116_PlacementProperties && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a_5_0 = null;
                            int ca_5_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations42 = 0;
                                int readerCount42 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id117_Property && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_5_0 = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])EnsureArrayIndex(a_5_0, ca_5_0, typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType));a_5_0[ca_5_0++] = Read18_KeyValuePairType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Property");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Property");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations42, ref readerCount42);
                                }
                            ReadEndElement();
                            }
                            o.@PlacementProperties = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])ShrinkArray(a_5_0, ca_5_0, typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType), false);
                        }
                    }
                    else if (((object) Reader.LocalName == (object)id118_Capacities && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a_6_0 = null;
                            int ca_6_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations43 = 0;
                                int readerCount43 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id119_Capacity && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_6_0 = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])EnsureArrayIndex(a_6_0, ca_6_0, typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType));a_6_0[ca_6_0++] = Read18_KeyValuePairType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Capacity");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Capacity");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations43, ref readerCount43);
                                }
                            ReadEndElement();
                            }
                            o.@Capacities = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])ShrinkArray(a_6_0, ca_6_0, typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType), false);
                        }
                    }
                    else if (((object) Reader.LocalName == (object)id120_SfssRgPolicies && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a_7_0 = null;
                            int ca_7_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations44 = 0;
                                int readerCount44 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id121_SfssRgPolicy && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_7_0 = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])EnsureArrayIndex(a_7_0, ca_7_0, typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType));a_7_0[ca_7_0++] = Read18_KeyValuePairType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:SfssRgPolicy");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:SfssRgPolicy");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations44, ref readerCount44);
                                }
                            ReadEndElement();
                            }
                            o.@SfssRgPolicies = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])ShrinkArray(a_7_0, ca_7_0, typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType), false);
                        }
                    }
                    else if (((object) Reader.LocalName == (object)id122_UserServiceMetricCapacities && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        if (!ReadNull()) {
                            global::System.Fabric.Management.ServiceModel.KeyValuePairType[] a_8_0 = null;
                            int ca_8_0 = 0;
                            if ((Reader.IsEmptyElement)) {
                                Reader.Skip();
                            }
                            else {
                                Reader.ReadStartElement();
                                Reader.MoveToContent();
                                int whileIterations45 = 0;
                                int readerCount45 = ReaderCount;
                                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                        if (((object) Reader.LocalName == (object)id84_Parameter && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                            a_8_0 = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])EnsureArrayIndex(a_8_0, ca_8_0, typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType));a_8_0[ca_8_0++] = Read18_KeyValuePairType(false, true, defaultNamespace);
                                        }
                                        else {
                                            UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Parameter");
                                        }
                                    }
                                    else {
                                        UnknownNode(null, @"http://schemas.microsoft.com/2011/01/fabric:Parameter");
                                    }
                                    Reader.MoveToContent();
                                    CheckReaderCount(ref whileIterations45, ref readerCount45);
                                }
                            ReadEndElement();
                            }
                            o.@UserServiceMetricCapacities = (global::System.Fabric.Management.ServiceModel.KeyValuePairType[])ShrinkArray(a_8_0, ca_8_0, typeof(global::System.Fabric.Management.ServiceModel.KeyValuePairType), false);
                        }
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:AllowStatefulWorkloads, http://schemas.microsoft.com/2011/01/fabric:Endpoints, http://schemas.microsoft.com/2011/01/fabric:KtlLoggerSettings, http://schemas.microsoft.com/2011/01/fabric:LogicalDirectories, http://schemas.microsoft.com/2011/01/fabric:Certificates, http://schemas.microsoft.com/2011/01/fabric:PlacementProperties, http://schemas.microsoft.com/2011/01/fabric:Capacities, http://schemas.microsoft.com/2011/01/fabric:SfssRgPolicies, http://schemas.microsoft.com/2011/01/fabric:UserServiceMetricCapacities");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:AllowStatefulWorkloads, http://schemas.microsoft.com/2011/01/fabric:Endpoints, http://schemas.microsoft.com/2011/01/fabric:KtlLoggerSettings, http://schemas.microsoft.com/2011/01/fabric:LogicalDirectories, http://schemas.microsoft.com/2011/01/fabric:Certificates, http://schemas.microsoft.com/2011/01/fabric:PlacementProperties, http://schemas.microsoft.com/2011/01/fabric:Capacities, http://schemas.microsoft.com/2011/01/fabric:SfssRgPolicies, http://schemas.microsoft.com/2011/01/fabric:UserServiceMetricCapacities");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations40, ref readerCount40);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.KeyValuePairType Read18_KeyValuePairType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id123_KeyValuePairType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.KeyValuePairType o;
            o = new global::System.Fabric.Management.ServiceModel.KeyValuePairType();
            bool[] paramsRead = new bool[2];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id44_Name && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Name = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id85_Value && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Value = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Name, :Value");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations46 = 0;
            int readerCount46 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations46, ref readerCount46);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.LogicalDirectoryType Read14_LogicalDirectoryType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id124_LogicalDirectoryType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.LogicalDirectoryType o;
            o = new global::System.Fabric.Management.ServiceModel.LogicalDirectoryType();
            bool[] paramsRead = new bool[3];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id125_LogicalDirectoryName && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@LogicalDirectoryName = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id126_MappedTo && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@MappedTo = Reader.Value;
                    paramsRead[1] = true;
                }
                else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id127_Context && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Context = Read13_LogicalDirectoryTypeContext(Reader.Value);
                    paramsRead[2] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":LogicalDirectoryName, :MappedTo, :Context");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations47 = 0;
            int readerCount47 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations47, ref readerCount47);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext Read13_LogicalDirectoryTypeContext(string s) {
            switch (s) {
                case @"application": return global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext.@application;
                case @"node": return global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext.@node;
                default: throw CreateUnknownConstantException(s, typeof(global::System.Fabric.Management.ServiceModel.LogicalDirectoryTypeContext));
            }
        }

        global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsType Read12_FabricKtlLoggerSettingsType(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id128_FabricKtlLoggerSettingsType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsType o;
            o = new global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsType();
            bool[] paramsRead = new bool[3];
            while (Reader.MoveToNextAttribute()) {
                if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o);
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations48 = 0;
            int readerCount48 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id129_SharedLogFilePath && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@SharedLogFilePath = Read9_Item(false, true, defaultNamespace);
                        paramsRead[0] = true;
                    }
                    else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id130_SharedLogFileId && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@SharedLogFileId = Read10_Item(false, true, defaultNamespace);
                        paramsRead[1] = true;
                    }
                    else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id131_SharedLogFileSizeInMB && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                        o.@SharedLogFileSizeInMB = Read11_Item(false, true, defaultNamespace);
                        paramsRead[2] = true;
                    }
                    else {
                        UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:SharedLogFilePath, http://schemas.microsoft.com/2011/01/fabric:SharedLogFileId, http://schemas.microsoft.com/2011/01/fabric:SharedLogFileSizeInMB");
                    }
                }
                else {
                    UnknownNode((object)o, @"http://schemas.microsoft.com/2011/01/fabric:SharedLogFilePath, http://schemas.microsoft.com/2011/01/fabric:SharedLogFileId, http://schemas.microsoft.com/2011/01/fabric:SharedLogFileSizeInMB");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations48, ref readerCount48);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileSizeInMB Read11_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileSizeInMB o;
            o = new global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileSizeInMB();
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id85_Value && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Value = System.Xml.XmlConvert.ToInt32(Reader.Value);
                    paramsRead[0] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Value");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations49 = 0;
            int readerCount49 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations49, ref readerCount49);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileId Read10_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileId o;
            o = new global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFileId();
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id85_Value && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Value = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Value");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations50 = 0;
            int readerCount50 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations50, ref readerCount50);
            }
            ReadEndElement();
            return o;
        }

        global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFilePath Read9_Item(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
            if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
            }
            else {
                throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
            }
            }
            if (isNull) return null;
            global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFilePath o;
            o = new global::System.Fabric.Management.ServiceModel.FabricKtlLoggerSettingsTypeSharedLogFilePath();
            bool[] paramsRead = new bool[1];
            while (Reader.MoveToNextAttribute()) {
                if (!paramsRead[0] && ((object) Reader.LocalName == (object)id85_Value && string.Equals(Reader.NamespaceURI, id10_Item))) {
                    o.@Value = Reader.Value;
                    paramsRead[0] = true;
                }
                else if (!IsXmlnsAttribute(Reader.Name)) {
                    UnknownNode((object)o, @":Value");
                }
            }
            Reader.MoveToElement();
            if (Reader.IsEmptyElement) {
                Reader.Skip();
                return o;
            }
            Reader.ReadStartElement();
            Reader.MoveToContent();
            int whileIterations51 = 0;
            int readerCount51 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)o, @"");
                }
                else {
                    UnknownNode((object)o, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations51, ref readerCount51);
            }
            ReadEndElement();
            return o;
        }

        protected override void InitCallbacks() {
        }

        string id102_Votes;
        string id80_Infrastructure;
        string id124_LogicalDirectoryType;
        string id11_MSILocation;
        string id5_TargetInformationType;
        string id122_UserServiceMetricCapacities;
        string id77_Description;
        string id57_Item;
        string id42_X509FindValue;
        string id8_Item;
        string id30_UpgradeDomain;
        string id76_Version;
        string id17_UpgradeEntryPointExe;
        string id131_SharedLogFileSizeInMB;
        string id51_HttpApplicationGatewayEndpoint;
        string id13_InfrastructureManifestLocation;
        string id120_SfssRgPolicies;
        string id115_LogicalDirectory;
        string id64_Item;
        string id91_PaaS;
        string id107_StaticVotes;
        string id99_AzureRoleType;
        string id108_StaticVote;
        string id21_InfrastructureInformationType;
        string id78_NodeTypes;
        string id101_SeedNodeCount;
        string id79_NodeType;
        string id96_FabricNodeType;
        string id25_IPAddressOrFQDN;
        string id126_MappedTo;
        string id38_UserRoleClientCertificate;
        string id67_ApplicationEndpoints;
        string id123_KeyValuePairType;
        string id32_Endpoints;
        string id37_ClientCertificate;
        string id75_ClusterManifestType;
        string id36_ServerCertificate;
        string id19_UndoUpgradeEntryPointExe;
        string id48_NotificationEndpoint;
        string id61_Item;
        string id16_RemoveNodeState;
        string id31_InfrastructurePlacementID;
        string id35_ClusterCertificate;
        string id10_Item;
        string id65_DefaultReplicatorEndpoint;
        string id60_Item;
        string id20_Item;
        string id56_Item;
        string id4_TargetInformation;
        string id113_KtlLoggerSettings;
        string id110_BlackbirdRoleType;
        string id43_X509FindValueSecondary;
        string id97_Roles;
        string id89_FlexibleDynamicTopology;
        string id81_FabricSettings;
        string id88_Blackbird;
        string id29_FaultDomain;
        string id66_TokenServiceEndpoint;
        string id50_HttpGatewayEndpoint;
        string id74_InternalEndpointType;
        string id28_IsSeedNode;
        string id83_SecretsCertificate;
        string id112_AllowStatefulWorkloads;
        string id92_WindowsAzure;
        string id40_X509StoreName;
        string id59_Item;
        string id33_Certificates;
        string id98_Role;
        string id86_IsEncrypted;
        string id49_ClusterConnectionEndpoint;
        string id9_InstanceId;
        string id129_SharedLogFilePath;
        string id34_CertificatesType;
        string id118_Capacities;
        string id24_InfrastructureNodeType;
        string id44_Name;
        string id22_NodeList;
        string id109_StaticVoteType;
        string id69_StartPort;
        string id128_FabricKtlLoggerSettingsType;
        string id72_Port;
        string id54_Item;
        string id68_EphemeralEndpoints;
        string id23_Node;
        string id62_Item;
        string id84_Parameter;
        string id73_Protocol;
        string id46_ClientConnectionEndpoint;
        string id87_Type;
        string id39_FabricCertificateType;
        string id55_NamingReplicatorEndpoint;
        string id6_CurrentInstallation;
        string id45_FabricEndpointsType;
        string id58_Item;
        string id2_Item;
        string id3_InfrastructureInformation;
        string id70_EndPort;
        string id7_TargetInstallation;
        string id106_RoleNodeCount;
        string id95_IsScaleMin;
        string id111_EnvironmentName;
        string id127_Context;
        string id116_PlacementProperties;
        string id52_ServiceConnectionEndpoint;
        string id26_RoleOrTierName;
        string id14_TargetVersion;
        string id103_Vote;
        string id114_LogicalDirectories;
        string id12_ClusterManifestLocation;
        string id53_Item;
        string id104_PaaSVoteType;
        string id125_LogicalDirectoryName;
        string id117_Property;
        string id121_SfssRgPolicy;
        string id105_PaaSRoleType;
        string id94_WindowsServer;
        string id130_SharedLogFileId;
        string id119_Capacity;
        string id100_RoleName;
        string id41_X509FindType;
        string id47_LeaseDriverEndpoint;
        string id1_ClusterManifest;
        string id27_NodeTypeRef;
        string id18_UpgradeEntryPointExeParameters;
        string id90_Linux;
        string id85_Value;
        string id71_InputEndpointType;
        string id15_NodeName;
        string id93_WindowsAzureStaticTopology;
        string id63_Item;
        string id82_Section;

        protected override void InitIDs() {
            id102_Votes = Reader.NameTable.Add(@"Votes");
            id80_Infrastructure = Reader.NameTable.Add(@"Infrastructure");
            id124_LogicalDirectoryType = Reader.NameTable.Add(@"LogicalDirectoryType");
            id11_MSILocation = Reader.NameTable.Add(@"MSILocation");
            id5_TargetInformationType = Reader.NameTable.Add(@"TargetInformationType");
            id122_UserServiceMetricCapacities = Reader.NameTable.Add(@"UserServiceMetricCapacities");
            id77_Description = Reader.NameTable.Add(@"Description");
            id57_Item = Reader.NameTable.Add(@"ImageStoreServiceReplicatorEndpoint");
            id42_X509FindValue = Reader.NameTable.Add(@"X509FindValue");
            id8_Item = Reader.NameTable.Add(@"WindowsFabricDeploymentInformation");
            id30_UpgradeDomain = Reader.NameTable.Add(@"UpgradeDomain");
            id76_Version = Reader.NameTable.Add(@"Version");
            id17_UpgradeEntryPointExe = Reader.NameTable.Add(@"UpgradeEntryPointExe");
            id131_SharedLogFileSizeInMB = Reader.NameTable.Add(@"SharedLogFileSizeInMB");
            id51_HttpApplicationGatewayEndpoint = Reader.NameTable.Add(@"HttpApplicationGatewayEndpoint");
            id13_InfrastructureManifestLocation = Reader.NameTable.Add(@"InfrastructureManifestLocation");
            id120_SfssRgPolicies = Reader.NameTable.Add(@"SfssRgPolicies");
            id115_LogicalDirectory = Reader.NameTable.Add(@"LogicalDirectory");
            id64_Item = Reader.NameTable.Add(@"GatewayResourceManagerReplicatorEndpoint");
            id91_PaaS = Reader.NameTable.Add(@"PaaS");
            id107_StaticVotes = Reader.NameTable.Add(@"StaticVotes");
            id99_AzureRoleType = Reader.NameTable.Add(@"AzureRoleType");
            id108_StaticVote = Reader.NameTable.Add(@"StaticVote");
            id21_InfrastructureInformationType = Reader.NameTable.Add(@"InfrastructureInformationType");
            id78_NodeTypes = Reader.NameTable.Add(@"NodeTypes");
            id101_SeedNodeCount = Reader.NameTable.Add(@"SeedNodeCount");
            id79_NodeType = Reader.NameTable.Add(@"NodeType");
            id96_FabricNodeType = Reader.NameTable.Add(@"FabricNodeType");
            id25_IPAddressOrFQDN = Reader.NameTable.Add(@"IPAddressOrFQDN");
            id126_MappedTo = Reader.NameTable.Add(@"MappedTo");
            id38_UserRoleClientCertificate = Reader.NameTable.Add(@"UserRoleClientCertificate");
            id67_ApplicationEndpoints = Reader.NameTable.Add(@"ApplicationEndpoints");
            id123_KeyValuePairType = Reader.NameTable.Add(@"KeyValuePairType");
            id32_Endpoints = Reader.NameTable.Add(@"Endpoints");
            id37_ClientCertificate = Reader.NameTable.Add(@"ClientCertificate");
            id75_ClusterManifestType = Reader.NameTable.Add(@"ClusterManifestType");
            id36_ServerCertificate = Reader.NameTable.Add(@"ServerCertificate");
            id19_UndoUpgradeEntryPointExe = Reader.NameTable.Add(@"UndoUpgradeEntryPointExe");
            id48_NotificationEndpoint = Reader.NameTable.Add(@"NotificationEndpoint");
            id61_Item = Reader.NameTable.Add(@"UpgradeOrchestrationServiceReplicatorEndpoint");
            id16_RemoveNodeState = Reader.NameTable.Add(@"RemoveNodeState");
            id31_InfrastructurePlacementID = Reader.NameTable.Add(@"InfrastructurePlacementID");
            id35_ClusterCertificate = Reader.NameTable.Add(@"ClusterCertificate");
            id10_Item = Reader.NameTable.Add(@"");
            id65_DefaultReplicatorEndpoint = Reader.NameTable.Add(@"DefaultReplicatorEndpoint");
            id60_Item = Reader.NameTable.Add(@"BackupRestoreServiceReplicatorEndpoint");
            id20_Item = Reader.NameTable.Add(@"UndoUpgradeEntryPointExeParameters");
            id56_Item = Reader.NameTable.Add(@"FailoverManagerReplicatorEndpoint");
            id4_TargetInformation = Reader.NameTable.Add(@"TargetInformation");
            id113_KtlLoggerSettings = Reader.NameTable.Add(@"KtlLoggerSettings");
            id110_BlackbirdRoleType = Reader.NameTable.Add(@"BlackbirdRoleType");
            id43_X509FindValueSecondary = Reader.NameTable.Add(@"X509FindValueSecondary");
            id97_Roles = Reader.NameTable.Add(@"Roles");
            id89_FlexibleDynamicTopology = Reader.NameTable.Add(@"FlexibleDynamicTopology");
            id81_FabricSettings = Reader.NameTable.Add(@"FabricSettings");
            id88_Blackbird = Reader.NameTable.Add(@"Blackbird");
            id29_FaultDomain = Reader.NameTable.Add(@"FaultDomain");
            id66_TokenServiceEndpoint = Reader.NameTable.Add(@"TokenServiceEndpoint");
            id50_HttpGatewayEndpoint = Reader.NameTable.Add(@"HttpGatewayEndpoint");
            id74_InternalEndpointType = Reader.NameTable.Add(@"InternalEndpointType");
            id28_IsSeedNode = Reader.NameTable.Add(@"IsSeedNode");
            id83_SecretsCertificate = Reader.NameTable.Add(@"SecretsCertificate");
            id112_AllowStatefulWorkloads = Reader.NameTable.Add(@"AllowStatefulWorkloads");
            id92_WindowsAzure = Reader.NameTable.Add(@"WindowsAzure");
            id40_X509StoreName = Reader.NameTable.Add(@"X509StoreName");
            id59_Item = Reader.NameTable.Add(@"FaultAnalysisServiceReplicatorEndpoint");
            id33_Certificates = Reader.NameTable.Add(@"Certificates");
            id98_Role = Reader.NameTable.Add(@"Role");
            id86_IsEncrypted = Reader.NameTable.Add(@"IsEncrypted");
            id49_ClusterConnectionEndpoint = Reader.NameTable.Add(@"ClusterConnectionEndpoint");
            id9_InstanceId = Reader.NameTable.Add(@"InstanceId");
            id129_SharedLogFilePath = Reader.NameTable.Add(@"SharedLogFilePath");
            id34_CertificatesType = Reader.NameTable.Add(@"CertificatesType");
            id118_Capacities = Reader.NameTable.Add(@"Capacities");
            id24_InfrastructureNodeType = Reader.NameTable.Add(@"InfrastructureNodeType");
            id44_Name = Reader.NameTable.Add(@"Name");
            id22_NodeList = Reader.NameTable.Add(@"NodeList");
            id109_StaticVoteType = Reader.NameTable.Add(@"StaticVoteType");
            id69_StartPort = Reader.NameTable.Add(@"StartPort");
            id128_FabricKtlLoggerSettingsType = Reader.NameTable.Add(@"FabricKtlLoggerSettingsType");
            id72_Port = Reader.NameTable.Add(@"Port");
            id54_Item = Reader.NameTable.Add(@"RepairManagerReplicatorEndpoint");
            id68_EphemeralEndpoints = Reader.NameTable.Add(@"EphemeralEndpoints");
            id23_Node = Reader.NameTable.Add(@"Node");
            id62_Item = Reader.NameTable.Add(@"CentralSecretServiceReplicatorEndpoint");
            id84_Parameter = Reader.NameTable.Add(@"Parameter");
            id73_Protocol = Reader.NameTable.Add(@"Protocol");
            id46_ClientConnectionEndpoint = Reader.NameTable.Add(@"ClientConnectionEndpoint");
            id87_Type = Reader.NameTable.Add(@"Type");
            id39_FabricCertificateType = Reader.NameTable.Add(@"FabricCertificateType");
            id55_NamingReplicatorEndpoint = Reader.NameTable.Add(@"NamingReplicatorEndpoint");
            id6_CurrentInstallation = Reader.NameTable.Add(@"CurrentInstallation");
            id45_FabricEndpointsType = Reader.NameTable.Add(@"FabricEndpointsType");
            id58_Item = Reader.NameTable.Add(@"UpgradeServiceReplicatorEndpoint");
            id2_Item = Reader.NameTable.Add(@"http://schemas.microsoft.com/2011/01/fabric");
            id3_InfrastructureInformation = Reader.NameTable.Add(@"InfrastructureInformation");
            id70_EndPort = Reader.NameTable.Add(@"EndPort");
            id7_TargetInstallation = Reader.NameTable.Add(@"TargetInstallation");
            id106_RoleNodeCount = Reader.NameTable.Add(@"RoleNodeCount");
            id95_IsScaleMin = Reader.NameTable.Add(@"IsScaleMin");
            id111_EnvironmentName = Reader.NameTable.Add(@"EnvironmentName");
            id127_Context = Reader.NameTable.Add(@"Context");
            id116_PlacementProperties = Reader.NameTable.Add(@"PlacementProperties");
            id52_ServiceConnectionEndpoint = Reader.NameTable.Add(@"ServiceConnectionEndpoint");
            id26_RoleOrTierName = Reader.NameTable.Add(@"RoleOrTierName");
            id14_TargetVersion = Reader.NameTable.Add(@"TargetVersion");
            id103_Vote = Reader.NameTable.Add(@"Vote");
            id114_LogicalDirectories = Reader.NameTable.Add(@"LogicalDirectories");
            id12_ClusterManifestLocation = Reader.NameTable.Add(@"ClusterManifestLocation");
            id53_Item = Reader.NameTable.Add(@"ClusterManagerReplicatorEndpoint");
            id104_PaaSVoteType = Reader.NameTable.Add(@"PaaSVoteType");
            id125_LogicalDirectoryName = Reader.NameTable.Add(@"LogicalDirectoryName");
            id117_Property = Reader.NameTable.Add(@"Property");
            id121_SfssRgPolicy = Reader.NameTable.Add(@"SfssRgPolicy");
            id105_PaaSRoleType = Reader.NameTable.Add(@"PaaSRoleType");
            id94_WindowsServer = Reader.NameTable.Add(@"WindowsServer");
            id130_SharedLogFileId = Reader.NameTable.Add(@"SharedLogFileId");
            id119_Capacity = Reader.NameTable.Add(@"Capacity");
            id100_RoleName = Reader.NameTable.Add(@"RoleName");
            id41_X509FindType = Reader.NameTable.Add(@"X509FindType");
            id47_LeaseDriverEndpoint = Reader.NameTable.Add(@"LeaseDriverEndpoint");
            id1_ClusterManifest = Reader.NameTable.Add(@"ClusterManifest");
            id27_NodeTypeRef = Reader.NameTable.Add(@"NodeTypeRef");
            id18_UpgradeEntryPointExeParameters = Reader.NameTable.Add(@"UpgradeEntryPointExeParameters");
            id90_Linux = Reader.NameTable.Add(@"Linux");
            id85_Value = Reader.NameTable.Add(@"Value");
            id71_InputEndpointType = Reader.NameTable.Add(@"InputEndpointType");
            id15_NodeName = Reader.NameTable.Add(@"NodeName");
            id93_WindowsAzureStaticTopology = Reader.NameTable.Add(@"WindowsAzureStaticTopology");
            id63_Item = Reader.NameTable.Add(@"EventStoreServiceReplicatorEndpoint");
            id82_Section = Reader.NameTable.Add(@"Section");
        }
    }

    [System.Runtime.CompilerServices.__BlockReflection]
    public abstract class XmlSerializer1 : System.Xml.Serialization.XmlSerializer {
        protected override System.Xml.Serialization.XmlSerializationReader CreateReader() {
            return new XmlSerializationReader1();
        }
        protected override System.Xml.Serialization.XmlSerializationWriter CreateWriter() {
            return new XmlSerializationWriter1();
        }
    }

    [System.Runtime.CompilerServices.__BlockReflection]
    public sealed class ClusterManifestTypeSerializer : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ClusterManifest", this.DefaultNamespace ?? @"http://schemas.microsoft.com/2011/01/fabric");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write44_ClusterManifest(objectToSerialize, this.DefaultNamespace, @"http://schemas.microsoft.com/2011/01/fabric");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read44_ClusterManifest(this.DefaultNamespace);
        }
    }

    [System.Runtime.CompilerServices.__BlockReflection]
    public sealed class InfrastructureInformationTypeSerializer : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InfrastructureInformation", this.DefaultNamespace ?? @"http://schemas.microsoft.com/2011/01/fabric");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write45_InfrastructureInformation(objectToSerialize, this.DefaultNamespace, @"http://schemas.microsoft.com/2011/01/fabric");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read45_InfrastructureInformation(this.DefaultNamespace);
        }
    }

    [System.Runtime.CompilerServices.__BlockReflection]
    public sealed class TargetInformationTypeSerializer : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TargetInformation", this.DefaultNamespace ?? @"http://schemas.microsoft.com/2011/01/fabric");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write46_TargetInformation(objectToSerialize, this.DefaultNamespace, @"http://schemas.microsoft.com/2011/01/fabric");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read46_TargetInformation(this.DefaultNamespace);
        }
    }

    [System.Runtime.CompilerServices.__BlockReflection]
    public class XmlSerializerContract : global::System.Xml.Serialization.XmlSerializerImplementation {
        public override global::System.Xml.Serialization.XmlSerializationReader Reader { get { return new XmlSerializationReader1(); } }
        public override global::System.Xml.Serialization.XmlSerializationWriter Writer { get { return new XmlSerializationWriter1(); } }
        System_Runtime_Extensions::System.Collections.Hashtable readMethods = null;
        public override System_Runtime_Extensions::System.Collections.Hashtable ReadMethods {
            get {
                if (readMethods == null) {
                    System_Runtime_Extensions::System.Collections.Hashtable _tmp = new System_Runtime_Extensions::System.Collections.Hashtable();
                    _tmp[@"System.Fabric.Management.ServiceModel.ClusterManifestType:http://schemas.microsoft.com/2011/01/fabric:ClusterManifest:False:"] = @"Read44_ClusterManifest";
                    _tmp[@"System.Fabric.Management.ServiceModel.InfrastructureInformationType:http://schemas.microsoft.com/2011/01/fabric:InfrastructureInformation:False:"] = @"Read45_InfrastructureInformation";
                    _tmp[@"System.Fabric.Management.ServiceModel.TargetInformationType:http://schemas.microsoft.com/2011/01/fabric:TargetInformation:False:"] = @"Read46_TargetInformation";
                    if (readMethods == null) readMethods = _tmp;
                }
                return readMethods;
            }
        }
        System_Runtime_Extensions::System.Collections.Hashtable writeMethods = null;
        public override System_Runtime_Extensions::System.Collections.Hashtable WriteMethods {
            get {
                if (writeMethods == null) {
                    System_Runtime_Extensions::System.Collections.Hashtable _tmp = new System_Runtime_Extensions::System.Collections.Hashtable();
                    _tmp[@"System.Fabric.Management.ServiceModel.ClusterManifestType:http://schemas.microsoft.com/2011/01/fabric:ClusterManifest:False:"] = @"Write44_ClusterManifest";
                    _tmp[@"System.Fabric.Management.ServiceModel.InfrastructureInformationType:http://schemas.microsoft.com/2011/01/fabric:InfrastructureInformation:False:"] = @"Write45_InfrastructureInformation";
                    _tmp[@"System.Fabric.Management.ServiceModel.TargetInformationType:http://schemas.microsoft.com/2011/01/fabric:TargetInformation:False:"] = @"Write46_TargetInformation";
                    if (writeMethods == null) writeMethods = _tmp;
                }
                return writeMethods;
            }
        }
        System_Runtime_Extensions::System.Collections.Hashtable typedSerializers = null;
        public override System_Runtime_Extensions::System.Collections.Hashtable TypedSerializers {
            get {
                if (typedSerializers == null) {
                    System_Runtime_Extensions::System.Collections.Hashtable _tmp = new System_Runtime_Extensions::System.Collections.Hashtable();
                    _tmp.Add(@"System.Fabric.Management.ServiceModel.InfrastructureInformationType:http://schemas.microsoft.com/2011/01/fabric:InfrastructureInformation:False:", new InfrastructureInformationTypeSerializer());
                    _tmp.Add(@"System.Fabric.Management.ServiceModel.TargetInformationType:http://schemas.microsoft.com/2011/01/fabric:TargetInformation:False:", new TargetInformationTypeSerializer());
                    _tmp.Add(@"System.Fabric.Management.ServiceModel.ClusterManifestType:http://schemas.microsoft.com/2011/01/fabric:ClusterManifest:False:", new ClusterManifestTypeSerializer());
                    if (typedSerializers == null) typedSerializers = _tmp;
                }
                return typedSerializers;
            }
        }
        public override System.Boolean CanSerialize(System.Type type) {
            if (type == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestType)) return true;
            if (type == typeof(global::System.Fabric.Management.ServiceModel.InfrastructureInformationType)) return true;
            if (type == typeof(global::System.Fabric.Management.ServiceModel.TargetInformationType)) return true;
            if (type == typeof(global::System.Reflection.TypeInfo)) return true;
            return false;
        }
        public override System.Xml.Serialization.XmlSerializer GetSerializer(System.Type type) {
            if (type == typeof(global::System.Fabric.Management.ServiceModel.ClusterManifestType)) return new ClusterManifestTypeSerializer();
            if (type == typeof(global::System.Fabric.Management.ServiceModel.InfrastructureInformationType)) return new InfrastructureInformationTypeSerializer();
            if (type == typeof(global::System.Fabric.Management.ServiceModel.TargetInformationType)) return new TargetInformationTypeSerializer();
            return null;
        }
        public static global::System.Xml.Serialization.XmlSerializerImplementation GetXmlSerializerContract() { return new XmlSerializerContract(); }
    }

    [System.Runtime.CompilerServices.__BlockReflection]
    public static class ActivatorHelper {
        public static object CreateInstance(System.Type type) {
            System.Reflection.TypeInfo ti = System.Reflection.IntrospectionExtensions.GetTypeInfo(type);
            foreach (System.Reflection.ConstructorInfo ci in ti.DeclaredConstructors) {
                if (!ci.IsStatic && ci.GetParameters().Length == 0) {
                    return ci.Invoke(null);
                }
            }
            return System.Activator.CreateInstance(type);
        }
    }
}
