using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading;
using System.Xml;

[assembly: global::System.Reflection.AssemblyVersion("4.0.0.0")]



namespace System.Runtime.Serialization.Generated
{
    [global::System.Runtime.CompilerServices.__BlockReflection]
    public static partial class DataContractSerializerHelper
    {
        static void InitDataContracts()
        {
            global::System.Collections.Generic.Dictionary<global::System.Type, global::System.Runtime.Serialization.DataContract> dataContracts = global::System.Runtime.Serialization.DataContract.GetDataContracts();
            PopulateContractDictionary(dataContracts);
            global::System.Collections.Generic.Dictionary<global::System.Runtime.Serialization.DataContract, global::System.Runtime.Serialization.Json.JsonReadWriteDelegates> jsonDelegates = global::System.Runtime.Serialization.Json.JsonReadWriteDelegates.GetJsonDelegates();
            PopulateJsonDelegateDictionary(
                                dataContracts, 
                                jsonDelegates
                            );
        }
        static int[] s_knownContractsLists = new int[] {
              -1, }
        ;
        // Count = 300
        static int[] s_xmlDictionaryStrings = new int[] {
                0, // array length: 0
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2, // array length: 2
                368, // index: 368, string: "Key"
                372, // index: 372, string: "Value"
                2, // array length: 2
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                406, // index: 406, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                2, // array length: 2
                1030, // index: 1030, string: "key"
                1034, // index: 1034, string: "value"
                2, // array length: 2
                406, // index: 406, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                406, // index: 406, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                16, // array length: 16
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                16, // array length: 16
                1040, // index: 1040, string: "CodePackageLinkFolder"
                1062, // index: 1062, string: "CodePackageName"
                1078, // index: 1078, string: "ConfigPackageLinkFolder"
                1102, // index: 1102, string: "ConfigPackageName"
                1120, // index: 1120, string: "ContainerDebugParams"
                1141, // index: 1141, string: "DataPackageLinkFolder"
                1163, // index: 1163, string: "DataPackageName"
                1179, // index: 1179, string: "DebugArguments"
                1194, // index: 1194, string: "DebugExePath"
                1207, // index: 1207, string: "DebugParametersFile"
                1227, // index: 1227, string: "DisableReliableCollectionReplicationMode"
                1268, // index: 1268, string: "EntryPointType"
                1283, // index: 1283, string: "EnvironmentBlock"
                1300, // index: 1300, string: "LockFile"
                1309, // index: 1309, string: "ServiceManifestName"
                1329, // index: 1329, string: "WorkingFolder"
                16, // array length: 16
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                4, // array length: 4
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                4, // array length: 4
                1343, // index: 1343, string: "Entrypoint"
                1354, // index: 1354, string: "EnvVars"
                1362, // index: 1362, string: "Labels"
                1369, // index: 1369, string: "Volumes"
                4, // array length: 4
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                507, // index: 507, string: "http://schemas.datacontract.org/2004/07/System.Fabric"
                7, // array length: 7
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                2, // array length: 2
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                7, // array length: 7
                1377, // index: 1377, string: "JobBlockingPolicy"
                1395, // index: 1395, string: "Mode"
                1400, // index: 1400, string: "InfrastructureTaskId"
                1421, // index: 1421, string: "IsManagementNotificationAvailable"
                1455, // index: 1455, string: "LastKnownJobState"
                1473, // index: 1473, string: "LastKnownTask"
                1487, // index: 1487, string: "ManagementNotification"
                7, // array length: 7
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                2, // array length: 2
                1377, // index: 1377, string: "JobBlockingPolicy"
                1395, // index: 1395, string: "Mode"
                2, // array length: 2
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                2, // array length: 2
                1510, // index: 1510, string: "TaskDescription"
                1526, // index: 1526, string: "TaskInstanceId"
                2, // array length: 2
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                2, // array length: 2
                1541, // index: 1541, string: "NodeName"
                1550, // index: 1550, string: "TaskType"
                2, // array length: 2
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                14, // array length: 14
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                14, // array length: 14
                1559, // index: 1559, string: "ActiveJobContextId"
                1578, // index: 1578, string: "ActiveJobDetailedStatus"
                1602, // index: 1602, string: "ActiveJobId"
                1614, // index: 1614, string: "ActiveJobIncludesTopologyChange"
                1646, // index: 1646, string: "ActiveJobStepId"
                1662, // index: 1662, string: "ActiveJobStepStatus"
                1682, // index: 1682, string: "ActiveJobStepTargetUD"
                1704, // index: 1704, string: "ActiveJobType"
                1718, // index: 1718, string: "Incarnation"
                1730, // index: 1730, string: "JobStepImpact"
                1744, // index: 1744, string: "NotificationDeadline"
                1765, // index: 1765, string: "NotificationId"
                1780, // index: 1780, string: "NotificationStatus"
                1799, // index: 1799, string: "NotificationType"
                14, // array length: 14
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                2, // array length: 2
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                4, // array length: 4
                1377, // index: 1377, string: "JobBlockingPolicy"
                1395, // index: 1395, string: "Mode"
                1816, // index: 1816, string: "JobDocumentIncarnation"
                1839, // index: 1839, string: "Jobs"
                4, // array length: 4
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                6, // array length: 6
                1844, // index: 1844, string: "ContextId"
                1854, // index: 1854, string: "Id"
                1857, // index: 1857, string: "ImpactAction"
                1870, // index: 1870, string: "JobStatus"
                1880, // index: 1880, string: "JobStep"
                1888, // index: 1888, string: "RoleInstancesToBeImpacted"
                6, // array length: 6
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                4, // array length: 4
                1914, // index: 1914, string: "AcknowledgementStatus"
                1936, // index: 1936, string: "ActionStatus"
                1949, // index: 1949, string: "CurrentlyImpactedRoleInstances"
                1980, // index: 1980, string: "ImpactStep"
                4, // array length: 4
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                3, // array length: 3
                286, // index: 286, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                3, // array length: 3
                1991, // index: 1991, string: "ImpactTypes"
                2003, // index: 2003, string: "Name"
                2008, // index: 2008, string: "UD"
                3, // array length: 3
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                1, // array length: 1
                1839, // index: 1839, string: "Jobs"
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                5, // array length: 5
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                5, // array length: 5
                1844, // index: 1844, string: "ContextId"
                2011, // index: 2011, string: "DetailedStatus"
                1854, // index: 1854, string: "Id"
                2026, // index: 2026, string: "Status"
                2033, // index: 2033, string: "Type"
                5, // array length: 5
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654, // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
                654  // index: 654, string: "http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common"
        };
        // Count = 0
        static global::MemberEntry[] s_dataMemberLists = new global::MemberEntry[0];
        static readonly byte[] s_dataContractMap_Hashtable = null;
        // Count=62
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::DataContractMapEntry[] s_dataContractMap = new global::DataContractMapEntry[] {
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 0, // 0x0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]" +
                                ", mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 0, // 0x0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 16, // 0x10
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 32, // 0x20
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 32, // 0x20
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 48, // 0x30
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]" +
                                "], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 48, // 0x30
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 64, // 0x40
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]" +
                                ", mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 64, // 0x40
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 80, // 0x50
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 80, // 0x50
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 96, // 0x60
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 96, // 0x60
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 112, // 0x70
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 112, // 0x70
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 128, // 0x80
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 128, // 0x80
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 144, // 0x90
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 144, // 0x90
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 160, // 0xa0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Private.Xml, Version=4.0.1.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd" +
                                "51")),
                    TableIndex = 176, // 0xb0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 192, // 0xc0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 192, // 0xc0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 208, // 0xd0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 208, // 0xd0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 224, // 0xe0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 240, // 0xf0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]" +
                                "], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 240, // 0xf0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 256, // 0x100
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 256, // 0x100
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 272, // 0x110
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 272, // 0x110
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 288, // 0x120
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 288, // 0x120
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 304, // 0x130
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 304, // 0x130
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.5.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 320, // 0x140
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IDictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 2, // 0x2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 1, // 0x1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 1, // 0x1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 17, // 0x11
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 17, // 0x11
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.CodePackageDebugParameters[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf" +
                                "3856ad364e35")),
                    TableIndex = 18, // 0x12
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.CodePackageDebugParameters, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf38" +
                                "56ad364e35")),
                    TableIndex = 33, // 0x21
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.ContainerDebugParameters, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856" +
                                "ad364e35")),
                    TableIndex = 49, // 0x31
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 34, // 0x22
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateDataSerial, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKe" +
                                "yToken=31bf3856ad364e35")),
                    TableIndex = 65, // 0x41
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken" +
                                "=31bf3856ad364e35")),
                    TableIndex = 81, // 0x51
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.InfrastructureTaskStateData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicK" +
                                "eyToken=31bf3856ad364e35")),
                    TableIndex = 97, // 0x61
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.NodeTaskDescriptionData[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKey" +
                                "Token=31bf3856ad364e35")),
                    TableIndex = 50, // 0x32
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.NodeTaskDescriptionData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyTo" +
                                "ken=31bf3856ad364e35")),
                    TableIndex = 113, // 0x71
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ManagementNotificationData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKe" +
                                "yToken=31bf3856ad364e35")),
                    TableIndex = 129, // 0x81
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateDataParallel, System.Fabric, Version=8.0.0.0, Culture=neutral, Public" +
                                "KeyToken=31bf3856ad364e35")),
                    TableIndex = 145, // 0x91
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobSummary[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf385" +
                                "6ad364e35")),
                    TableIndex = 66, // 0x42
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobSummary, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856a" +
                                "d364e35")),
                    TableIndex = 161, // 0xa1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobStepSummary, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3" +
                                "856ad364e35")),
                    TableIndex = 177, // 0xb1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ImpactedRoleInstance[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyTok" +
                                "en=31bf3856ad364e35")),
                    TableIndex = 82, // 0x52
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ImpactedRoleInstance, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken" +
                                "=31bf3856ad364e35")),
                    TableIndex = 193, // 0xc1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobCollectionData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31" +
                                "bf3856ad364e35")),
                    TableIndex = 209, // 0xd1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobData[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad" +
                                "364e35")),
                    TableIndex = 98, // 0x62
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad36" +
                                "4e35")),
                    TableIndex = 225, // 0xe1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 114, // 0x72
                }
        };
        static readonly byte[] s_dataContracts_Hashtable = null;
        // Count=21
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::DataContractEntry[] s_dataContracts = new global::DataContractEntry[] {
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 0, // boolean
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 0, // boolean
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 0, // boolean
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.BooleanDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 93, // base64Binary
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 93, // base64Binary
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 93, // base64Binary
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.ByteArrayDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 106, // char
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 106, // char
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 106, // char
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.CharDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 111, // dateTime
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 111, // dateTime
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 111, // dateTime
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DateTimeDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 120, // decimal
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 120, // decimal
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 120, // decimal
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DecimalDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 128, // double
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 128, // double
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 128, // double
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DoubleDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 135, // float
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 135, // float
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 135, // float
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.FloatDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 141, // guid
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 141, // guid
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 141, // guid
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.GuidDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 146, // int
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 146, // int
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 146, // int
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.IntDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 150, // long
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 150, // long
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 150, // long
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.LongDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 155, // anyType
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 155, // anyType
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 155, // anyType
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    Kind = global::DataContractKind.ObjectDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 163, // QName
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 163, // QName
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 163, // QName
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Private.Xml, Version=4.0.1.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd" +
                                    "51")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Private.Xml, Version=4.0.1.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd" +
                                    "51")),
                    },
                    Kind = global::DataContractKind.QNameDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 169, // short
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 169, // short
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 169, // short
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.ShortDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 175, // byte
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 175, // byte
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 175, // byte
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.SignedByteDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 180, // string
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 180, // string
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 180, // string
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.StringDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 187, // duration
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 187, // duration
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 187, // duration
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.TimeSpanDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 196, // unsignedByte
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 196, // unsignedByte
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 196, // unsignedByte
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedByteDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 209, // unsignedInt
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 209, // unsignedInt
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 209, // unsignedInt
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedIntDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 221, // unsignedLong
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 221, // unsignedLong
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 221, // unsignedLong
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedLongDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 234, // unsignedShort
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 234, // unsignedShort
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 234, // unsignedShort
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedShortDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 248, // anyURI
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 248, // anyURI
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 248, // anyURI
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.5.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.5.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    Kind = global::DataContractKind.UriDataContract,
                }
        };
        static readonly byte[] s_classDataContracts_Hashtable = null;
        // Count=15
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::ClassDataContractEntry[] s_classDataContracts = new global::ClassDataContractEntry[] {
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 344, // KeyValueOfstringanyType
                        NamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 344, // KeyValueOfstringanyType
                        StableNameNamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 344, // KeyValueOfstringanyType
                        TopLevelElementNamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Runtime.Serialization.KeyValue`2, System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neut" +
                                    "ral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type3.ReadKeyValueOfstringanyTypeFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type4.WriteKeyValueOfstringanyTypeToXml),
                    ChildElementNamespacesListIndex = 1,
                    ContractNamespacesListIndex = 4,
                    MemberNamesListIndex = 6,
                    MemberNamespacesListIndex = 9,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 378, // KeyValuePairOfstringanyType
                        NamespaceIndex = 406, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        StableNameIndex = 378, // KeyValuePairOfstringanyType
                        StableNameNamespaceIndex = 406, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        TopLevelElementNameIndex = 378, // KeyValuePairOfstringanyType
                        TopLevelElementNamespaceIndex = 406, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.KeyValuePair`2, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyTo" +
                                    "ken=b03f5f7f11d50a3a")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type5.ReadKeyValuePairOfstringanyTypeFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type6.WriteKeyValuePairOfstringanyTypeToXml),
                    ChildElementNamespacesListIndex = 12,
                    ContractNamespacesListIndex = 15,
                    MemberNamesListIndex = 17,
                    MemberNamespacesListIndex = 20,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 561, // CodePackageDebugParameters
                        NamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        StableNameIndex = 561, // CodePackageDebugParameters
                        StableNameNamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        TopLevelElementNameIndex = 561, // CodePackageDebugParameters
                        TopLevelElementNamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.CodePackageDebugParameters, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf38" +
                                    "56ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.CodePackageDebugParameters, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf38" +
                                    "56ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 23,
                    ContractNamespacesListIndex = 40,
                    MemberNamesListIndex = 42,
                    MemberNamespacesListIndex = 59,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 588, // ContainerDebugParameters
                        NamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        StableNameIndex = 588, // ContainerDebugParameters
                        StableNameNamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        TopLevelElementNameIndex = 588, // ContainerDebugParameters
                        TopLevelElementNamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.ContainerDebugParameters, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856" +
                                    "ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.ContainerDebugParameters, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856" +
                                    "ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 76,
                    ContractNamespacesListIndex = 81,
                    MemberNamesListIndex = 83,
                    MemberNamespacesListIndex = 88,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 627, // CoordinatorStateDataSerial
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 627, // CoordinatorStateDataSerial
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 627, // CoordinatorStateDataSerial
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateDataSerial, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKe" +
                                    "yToken=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateDataSerial, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKe" +
                                    "yToken=31bf3856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 93,
                    ContractNamespacesListIndex = 101,
                    MemberNamesListIndex = 104,
                    MemberNamespacesListIndex = 112,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 721, // CoordinatorStateData
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 721, // CoordinatorStateData
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 721, // CoordinatorStateData
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken" +
                                    "=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken" +
                                    "=31bf3856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 120,
                    ContractNamespacesListIndex = 123,
                    MemberNamesListIndex = 125,
                    MemberNamespacesListIndex = 128,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 742, // InfrastructureTaskStateData
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 742, // InfrastructureTaskStateData
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 742, // InfrastructureTaskStateData
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.InfrastructureTaskStateData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicK" +
                                    "eyToken=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.InfrastructureTaskStateData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicK" +
                                    "eyToken=31bf3856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 131,
                    ContractNamespacesListIndex = 134,
                    MemberNamesListIndex = 136,
                    MemberNamespacesListIndex = 139,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 801, // NodeTaskDescriptionData
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 801, // NodeTaskDescriptionData
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 801, // NodeTaskDescriptionData
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.NodeTaskDescriptionData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyTo" +
                                    "ken=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.NodeTaskDescriptionData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyTo" +
                                    "ken=31bf3856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 142,
                    ContractNamespacesListIndex = 145,
                    MemberNamesListIndex = 147,
                    MemberNamespacesListIndex = 150,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 825, // ManagementNotificationData
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 825, // ManagementNotificationData
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 825, // ManagementNotificationData
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ManagementNotificationData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKe" +
                                    "yToken=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ManagementNotificationData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKe" +
                                    "yToken=31bf3856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 153,
                    ContractNamespacesListIndex = 168,
                    MemberNamesListIndex = 170,
                    MemberNamespacesListIndex = 185,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 852, // CoordinatorStateDataParallel
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 852, // CoordinatorStateDataParallel
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 852, // CoordinatorStateDataParallel
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateDataParallel, System.Fabric, Version=8.0.0.0, Culture=neutral, Public" +
                                    "KeyToken=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.CoordinatorStateDataParallel, System.Fabric, Version=8.0.0.0, Culture=neutral, Public" +
                                    "KeyToken=31bf3856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 200,
                    ContractNamespacesListIndex = 205,
                    MemberNamesListIndex = 208,
                    MemberNamespacesListIndex = 213,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 899, // JobSummary
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 899, // JobSummary
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 899, // JobSummary
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobSummary, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856a" +
                                    "d364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobSummary, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856a" +
                                    "d364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 218,
                    ContractNamespacesListIndex = 225,
                    MemberNamesListIndex = 227,
                    MemberNamespacesListIndex = 234,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 910, // JobStepSummary
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 910, // JobStepSummary
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 910, // JobStepSummary
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobStepSummary, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3" +
                                    "856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobStepSummary, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3" +
                                    "856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 241,
                    ContractNamespacesListIndex = 246,
                    MemberNamesListIndex = 248,
                    MemberNamespacesListIndex = 253,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 953, // ImpactedRoleInstance
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 953, // ImpactedRoleInstance
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 953, // ImpactedRoleInstance
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ImpactedRoleInstance, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken" +
                                    "=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ImpactedRoleInstance, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken" +
                                    "=31bf3856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 258,
                    ContractNamespacesListIndex = 262,
                    MemberNamesListIndex = 264,
                    MemberNamespacesListIndex = 268,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 974, // JobCollectionData
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 974, // JobCollectionData
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 974, // JobCollectionData
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobCollectionData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31" +
                                    "bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobCollectionData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31" +
                                    "bf3856ad364e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 272,
                    ContractNamespacesListIndex = 274,
                    MemberNamesListIndex = 276,
                    MemberNamespacesListIndex = 278,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1007, // JobData
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 1007, // JobData
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 1007, // JobData
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad36" +
                                    "4e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad36" +
                                    "4e35")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 280,
                    ContractNamespacesListIndex = 286,
                    MemberNamesListIndex = 288,
                    MemberNamespacesListIndex = 294,
                }
        };
        static readonly byte[] s_collectionDataContracts_Hashtable = null;
        // Count=8
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::CollectionDataContractEntry[] s_collectionDataContracts = new global::CollectionDataContractEntry[] {
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        TypeIsCollectionInterface = true,
                        TypeIsInterface = true,
                        NameIndex = 255, // ArrayOfKeyValueOfstringanyType
                        NamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 255, // ArrayOfKeyValueOfstringanyType
                        StableNameNamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 255, // ArrayOfKeyValueOfstringanyType
                        TopLevelElementNamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IDictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IDictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.IDictionary`2, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyTok" +
                                    "en=b03f5f7f11d50a3a")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type0.ReadArrayOfKeyValueOfstringanyTypeFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type1.WriteArrayOfKeyValueOfstringanyTypeToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type2.ReadArrayOfKeyValueOfstringanyTypeFromXmlIsGetOnly),
                    CollectionItemNameIndex = 344, // KeyValueOfstringanyType
                    KeyNameIndex = 368, // Key
                    ItemNameIndex = 344, // KeyValueOfstringanyType
                    ValueNameIndex = 372, // Value
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericDictionary,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 473, // ArrayOfCodePackageDebugParameters
                        NamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        StableNameIndex = 473, // ArrayOfCodePackageDebugParameters
                        StableNameNamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        TopLevelElementNameIndex = 473, // ArrayOfCodePackageDebugParameters
                        TopLevelElementNamespaceIndex = 507, // http://schemas.datacontract.org/2004/07/System.Fabric
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.CodePackageDebugParameters[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf" +
                                    "3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.CodePackageDebugParameters[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf" +
                                    "3856ad364e35")),
                    },
                    CollectionItemNameIndex = 561, // CodePackageDebugParameters
                    KeyNameIndex = -1,
                    ItemNameIndex = 561, // CodePackageDebugParameters
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.CodePackageDebugParameters, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf38" +
                                "56ad364e35")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 613, // ArrayOfstring
                        NamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 613, // ArrayOfstring
                        StableNameNamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 613, // ArrayOfstring
                        TopLevelElementNamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    CollectionItemNameIndex = 180, // string
                    KeyNameIndex = -1,
                    ItemNameIndex = 180, // string
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 770, // ArrayOfNodeTaskDescriptionData
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 770, // ArrayOfNodeTaskDescriptionData
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 770, // ArrayOfNodeTaskDescriptionData
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.NodeTaskDescriptionData[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKey" +
                                    "Token=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.NodeTaskDescriptionData[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKey" +
                                    "Token=31bf3856ad364e35")),
                    },
                    CollectionItemNameIndex = 801, // NodeTaskDescriptionData
                    KeyNameIndex = -1,
                    ItemNameIndex = 801, // NodeTaskDescriptionData
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.NodeTaskDescriptionData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyTo" +
                                "ken=31bf3856ad364e35")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 881, // ArrayOfJobSummary
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 881, // ArrayOfJobSummary
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 881, // ArrayOfJobSummary
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobSummary[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf385" +
                                    "6ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobSummary[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf385" +
                                    "6ad364e35")),
                    },
                    CollectionItemNameIndex = 899, // JobSummary
                    KeyNameIndex = -1,
                    ItemNameIndex = 899, // JobSummary
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobSummary, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856a" +
                                "d364e35")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 925, // ArrayOfImpactedRoleInstance
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 925, // ArrayOfImpactedRoleInstance
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 925, // ArrayOfImpactedRoleInstance
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ImpactedRoleInstance[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyTok" +
                                    "en=31bf3856ad364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ImpactedRoleInstance[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyTok" +
                                    "en=31bf3856ad364e35")),
                    },
                    CollectionItemNameIndex = 953, // ImpactedRoleInstance
                    KeyNameIndex = -1,
                    ItemNameIndex = 953, // ImpactedRoleInstance
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.ImpactedRoleInstance, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken" +
                                "=31bf3856ad364e35")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 992, // ArrayOfJobData
                        NamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        StableNameIndex = 992, // ArrayOfJobData
                        StableNameNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        TopLevelElementNameIndex = 992, // ArrayOfJobData
                        TopLevelElementNamespaceIndex = 654, // http://schemas.datacontract.org/2004/07/System.Fabric.Chaos.Common
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobData[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad" +
                                    "364e35")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobData[], System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad" +
                                    "364e35")),
                    },
                    CollectionItemNameIndex = 1007, // JobData
                    KeyNameIndex = -1,
                    ItemNameIndex = 1007, // JobData
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Fabric.Chaos.Common.JobData, System.Fabric, Version=8.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad36" +
                                "4e35")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1015, // ArrayOfanyType
                        NamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 1015, // ArrayOfanyType
                        StableNameNamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 1015, // ArrayOfanyType
                        TopLevelElementNamespaceIndex = 286, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    CollectionItemNameIndex = 155, // anyType
                    KeyNameIndex = -1,
                    ItemNameIndex = 155, // anyType
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }
        };
        static readonly byte[] s_enumDataContracts_Hashtable = null;
        // Count=0
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::EnumDataContractEntry[] s_enumDataContracts = new global::EnumDataContractEntry[0];
        static readonly byte[] s_xmlDataContracts_Hashtable = null;
        // Count=0
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::XmlDataContractEntry[] s_xmlDataContracts = new global::XmlDataContractEntry[0];
        static readonly byte[] s_jsonDelegatesList_Hashtable = null;
        // Count=20
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::JsonDelegateEntry[] s_jsonDelegatesList = new global::JsonDelegateEntry[] {
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 42,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type11.WriteArrayOfCodePackageDebugParametersToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type10.ReadArrayOfCodePackageDebugParametersFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type12.ReadArrayOfCodePackageDebugParametersFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 43,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type16.WriteCodePackageDebugParametersToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type15.ReadCodePackageDebugParametersFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 44,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type20.WriteContainerDebugParametersToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type19.ReadContainerDebugParametersFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 45,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type25.WriteArrayOfstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type24.ReadArrayOfstringFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type26.ReadArrayOfstringFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 46,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type30.WriteCoordinatorStateDataSerialToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type29.ReadCoordinatorStateDataSerialFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 47,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type34.WriteCoordinatorStateDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type33.ReadCoordinatorStateDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 48,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type38.WriteInfrastructureTaskStateDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type37.ReadInfrastructureTaskStateDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 49,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type43.WriteArrayOfNodeTaskDescriptionDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type42.ReadArrayOfNodeTaskDescriptionDataFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type44.ReadArrayOfNodeTaskDescriptionDataFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 50,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type48.WriteNodeTaskDescriptionDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type47.ReadNodeTaskDescriptionDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 51,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type52.WriteManagementNotificationDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type51.ReadManagementNotificationDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 52,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type56.WriteCoordinatorStateDataParallelToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type55.ReadCoordinatorStateDataParallelFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 53,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type61.WriteArrayOfJobSummaryToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type60.ReadArrayOfJobSummaryFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type62.ReadArrayOfJobSummaryFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 54,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type66.WriteJobSummaryToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type65.ReadJobSummaryFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 55,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type70.WriteJobStepSummaryToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type69.ReadJobStepSummaryFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 56,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type75.WriteArrayOfImpactedRoleInstanceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type74.ReadArrayOfImpactedRoleInstanceFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type76.ReadArrayOfImpactedRoleInstanceFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 57,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type80.WriteImpactedRoleInstanceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type79.ReadImpactedRoleInstanceFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 58,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type84.WriteJobCollectionDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type83.ReadJobCollectionDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 59,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type89.WriteArrayOfJobDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type88.ReadArrayOfJobDataFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type90.ReadArrayOfJobDataFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 60,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type94.WriteJobDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type93.ReadJobDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 61,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type99.WriteArrayOfanyTypeToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type98.ReadArrayOfanyTypeFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type100.ReadArrayOfanyTypeFromJsonIsGetOnly),
                }
        };
        static char[] s_stringPool = new char[] {
            'b','o','o','l','e','a','n','\0','h','t','t','p',':','/','/','w','w','w','.','w','3','.','o','r','g','/','2','0','0','1',
            '/','X','M','L','S','c','h','e','m','a','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','m','i','c','r',
            'o','s','o','f','t','.','c','o','m','/','2','0','0','3','/','1','0','/','S','e','r','i','a','l','i','z','a','t','i','o',
            'n','/','\0','b','a','s','e','6','4','B','i','n','a','r','y','\0','c','h','a','r','\0','d','a','t','e','T','i','m','e','\0',
            'd','e','c','i','m','a','l','\0','d','o','u','b','l','e','\0','f','l','o','a','t','\0','g','u','i','d','\0','i','n','t','\0',
            'l','o','n','g','\0','a','n','y','T','y','p','e','\0','Q','N','a','m','e','\0','s','h','o','r','t','\0','b','y','t','e','\0',
            's','t','r','i','n','g','\0','d','u','r','a','t','i','o','n','\0','u','n','s','i','g','n','e','d','B','y','t','e','\0','u',
            'n','s','i','g','n','e','d','I','n','t','\0','u','n','s','i','g','n','e','d','L','o','n','g','\0','u','n','s','i','g','n',
            'e','d','S','h','o','r','t','\0','a','n','y','U','R','I','\0','A','r','r','a','y','O','f','K','e','y','V','a','l','u','e',
            'O','f','s','t','r','i','n','g','a','n','y','T','y','p','e','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s',
            '.','m','i','c','r','o','s','o','f','t','.','c','o','m','/','2','0','0','3','/','1','0','/','S','e','r','i','a','l','i',
            'z','a','t','i','o','n','/','A','r','r','a','y','s','\0','K','e','y','V','a','l','u','e','O','f','s','t','r','i','n','g',
            'a','n','y','T','y','p','e','\0','K','e','y','\0','V','a','l','u','e','\0','K','e','y','V','a','l','u','e','P','a','i','r',
            'O','f','s','t','r','i','n','g','a','n','y','T','y','p','e','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s',
            '.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0','4','/','0','7','/','S','y','s','t',
            'e','m','.','C','o','l','l','e','c','t','i','o','n','s','.','G','e','n','e','r','i','c','\0','A','r','r','a','y','O','f',
            'C','o','d','e','P','a','c','k','a','g','e','D','e','b','u','g','P','a','r','a','m','e','t','e','r','s','\0','h','t','t',
            'p',':','/','/','s','c','h','e','m','a','s','.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2',
            '0','0','4','/','0','7','/','S','y','s','t','e','m','.','F','a','b','r','i','c','\0','C','o','d','e','P','a','c','k','a',
            'g','e','D','e','b','u','g','P','a','r','a','m','e','t','e','r','s','\0','C','o','n','t','a','i','n','e','r','D','e','b',
            'u','g','P','a','r','a','m','e','t','e','r','s','\0','A','r','r','a','y','O','f','s','t','r','i','n','g','\0','C','o','o',
            'r','d','i','n','a','t','o','r','S','t','a','t','e','D','a','t','a','S','e','r','i','a','l','\0','h','t','t','p',':','/',
            '/','s','c','h','e','m','a','s','.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0','4',
            '/','0','7','/','S','y','s','t','e','m','.','F','a','b','r','i','c','.','C','h','a','o','s','.','C','o','m','m','o','n',
            '\0','C','o','o','r','d','i','n','a','t','o','r','S','t','a','t','e','D','a','t','a','\0','I','n','f','r','a','s','t','r',
            'u','c','t','u','r','e','T','a','s','k','S','t','a','t','e','D','a','t','a','\0','A','r','r','a','y','O','f','N','o','d',
            'e','T','a','s','k','D','e','s','c','r','i','p','t','i','o','n','D','a','t','a','\0','N','o','d','e','T','a','s','k','D',
            'e','s','c','r','i','p','t','i','o','n','D','a','t','a','\0','M','a','n','a','g','e','m','e','n','t','N','o','t','i','f',
            'i','c','a','t','i','o','n','D','a','t','a','\0','C','o','o','r','d','i','n','a','t','o','r','S','t','a','t','e','D','a',
            't','a','P','a','r','a','l','l','e','l','\0','A','r','r','a','y','O','f','J','o','b','S','u','m','m','a','r','y','\0','J',
            'o','b','S','u','m','m','a','r','y','\0','J','o','b','S','t','e','p','S','u','m','m','a','r','y','\0','A','r','r','a','y',
            'O','f','I','m','p','a','c','t','e','d','R','o','l','e','I','n','s','t','a','n','c','e','\0','I','m','p','a','c','t','e',
            'd','R','o','l','e','I','n','s','t','a','n','c','e','\0','J','o','b','C','o','l','l','e','c','t','i','o','n','D','a','t',
            'a','\0','A','r','r','a','y','O','f','J','o','b','D','a','t','a','\0','J','o','b','D','a','t','a','\0','A','r','r','a','y',
            'O','f','a','n','y','T','y','p','e','\0','k','e','y','\0','v','a','l','u','e','\0','C','o','d','e','P','a','c','k','a','g',
            'e','L','i','n','k','F','o','l','d','e','r','\0','C','o','d','e','P','a','c','k','a','g','e','N','a','m','e','\0','C','o',
            'n','f','i','g','P','a','c','k','a','g','e','L','i','n','k','F','o','l','d','e','r','\0','C','o','n','f','i','g','P','a',
            'c','k','a','g','e','N','a','m','e','\0','C','o','n','t','a','i','n','e','r','D','e','b','u','g','P','a','r','a','m','s',
            '\0','D','a','t','a','P','a','c','k','a','g','e','L','i','n','k','F','o','l','d','e','r','\0','D','a','t','a','P','a','c',
            'k','a','g','e','N','a','m','e','\0','D','e','b','u','g','A','r','g','u','m','e','n','t','s','\0','D','e','b','u','g','E',
            'x','e','P','a','t','h','\0','D','e','b','u','g','P','a','r','a','m','e','t','e','r','s','F','i','l','e','\0','D','i','s',
            'a','b','l','e','R','e','l','i','a','b','l','e','C','o','l','l','e','c','t','i','o','n','R','e','p','l','i','c','a','t',
            'i','o','n','M','o','d','e','\0','E','n','t','r','y','P','o','i','n','t','T','y','p','e','\0','E','n','v','i','r','o','n',
            'm','e','n','t','B','l','o','c','k','\0','L','o','c','k','F','i','l','e','\0','S','e','r','v','i','c','e','M','a','n','i',
            'f','e','s','t','N','a','m','e','\0','W','o','r','k','i','n','g','F','o','l','d','e','r','\0','E','n','t','r','y','p','o',
            'i','n','t','\0','E','n','v','V','a','r','s','\0','L','a','b','e','l','s','\0','V','o','l','u','m','e','s','\0','J','o','b',
            'B','l','o','c','k','i','n','g','P','o','l','i','c','y','\0','M','o','d','e','\0','I','n','f','r','a','s','t','r','u','c',
            't','u','r','e','T','a','s','k','I','d','\0','I','s','M','a','n','a','g','e','m','e','n','t','N','o','t','i','f','i','c',
            'a','t','i','o','n','A','v','a','i','l','a','b','l','e','\0','L','a','s','t','K','n','o','w','n','J','o','b','S','t','a',
            't','e','\0','L','a','s','t','K','n','o','w','n','T','a','s','k','\0','M','a','n','a','g','e','m','e','n','t','N','o','t',
            'i','f','i','c','a','t','i','o','n','\0','T','a','s','k','D','e','s','c','r','i','p','t','i','o','n','\0','T','a','s','k',
            'I','n','s','t','a','n','c','e','I','d','\0','N','o','d','e','N','a','m','e','\0','T','a','s','k','T','y','p','e','\0','A',
            'c','t','i','v','e','J','o','b','C','o','n','t','e','x','t','I','d','\0','A','c','t','i','v','e','J','o','b','D','e','t',
            'a','i','l','e','d','S','t','a','t','u','s','\0','A','c','t','i','v','e','J','o','b','I','d','\0','A','c','t','i','v','e',
            'J','o','b','I','n','c','l','u','d','e','s','T','o','p','o','l','o','g','y','C','h','a','n','g','e','\0','A','c','t','i',
            'v','e','J','o','b','S','t','e','p','I','d','\0','A','c','t','i','v','e','J','o','b','S','t','e','p','S','t','a','t','u',
            's','\0','A','c','t','i','v','e','J','o','b','S','t','e','p','T','a','r','g','e','t','U','D','\0','A','c','t','i','v','e',
            'J','o','b','T','y','p','e','\0','I','n','c','a','r','n','a','t','i','o','n','\0','J','o','b','S','t','e','p','I','m','p',
            'a','c','t','\0','N','o','t','i','f','i','c','a','t','i','o','n','D','e','a','d','l','i','n','e','\0','N','o','t','i','f',
            'i','c','a','t','i','o','n','I','d','\0','N','o','t','i','f','i','c','a','t','i','o','n','S','t','a','t','u','s','\0','N',
            'o','t','i','f','i','c','a','t','i','o','n','T','y','p','e','\0','J','o','b','D','o','c','u','m','e','n','t','I','n','c',
            'a','r','n','a','t','i','o','n','\0','J','o','b','s','\0','C','o','n','t','e','x','t','I','d','\0','I','d','\0','I','m','p',
            'a','c','t','A','c','t','i','o','n','\0','J','o','b','S','t','a','t','u','s','\0','J','o','b','S','t','e','p','\0','R','o',
            'l','e','I','n','s','t','a','n','c','e','s','T','o','B','e','I','m','p','a','c','t','e','d','\0','A','c','k','n','o','w',
            'l','e','d','g','e','m','e','n','t','S','t','a','t','u','s','\0','A','c','t','i','o','n','S','t','a','t','u','s','\0','C',
            'u','r','r','e','n','t','l','y','I','m','p','a','c','t','e','d','R','o','l','e','I','n','s','t','a','n','c','e','s','\0',
            'I','m','p','a','c','t','S','t','e','p','\0','I','m','p','a','c','t','T','y','p','e','s','\0','N','a','m','e','\0','U','D',
            '\0','D','e','t','a','i','l','e','d','S','t','a','t','u','s','\0','S','t','a','t','u','s','\0','T','y','p','e','\0'};
    }
}
